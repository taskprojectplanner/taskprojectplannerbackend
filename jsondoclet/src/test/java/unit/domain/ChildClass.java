package unit.domain;

/** Child class */
public class ChildClass extends ParentClass {
  /** Overrided method from parent */
  @Override
  public void parentMethod() {}
}
