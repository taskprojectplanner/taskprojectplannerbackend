package unit.domain;

/** Parent class */
public class ParentClass {

  /** Parent method */
  public void parentMethod() {}

  /** Common method */
  public void commonMethod() {}
}
