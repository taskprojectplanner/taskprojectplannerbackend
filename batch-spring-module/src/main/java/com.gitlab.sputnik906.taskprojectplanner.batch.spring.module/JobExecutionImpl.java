package com.gitlab.sputnik906.taskprojectplanner.batch.spring.module;


import com.gitlab.sputnik906.taskprojectplanner.batch.api.JobExecution;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class JobExecutionImpl implements JobExecution {
  @NonNull private final org.springframework.batch.core.JobExecution jobExecutionWrapper;
}
