package com.gitlab.sputnik906.taskprojectplanner.batch.spring.module;

import com.gitlab.sputnik906.taskprojectplanner.batch.api.ChunkContext;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.Job;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.JobExecution;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.JobLauncher;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.RepeatStatus;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.StepContribution;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.Tasklet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.converter.DefaultJobParametersConverter;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.job.builder.SimpleJobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JobLauncherImpl implements JobLauncher {

  @Autowired
  private org.springframework.batch.core.launch.JobLauncher jobLauncher;

  @Autowired
  private JobExplorer jobExplorer;

  @Autowired
  private JobBuilderFactory jobBuilderFactory;

  @Autowired
  private StepBuilderFactory stepBuilderFactory;

  @Override
  public JobExecution run(
    Job job,
    Map<String, Object> parameters) {

    Properties properties = new Properties();
    parameters.forEach(properties::put);

    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder(
      new DefaultJobParametersConverter().getJobParameters(properties),
      jobExplorer
    );

    List<org.springframework.batch.core.Step> springSteps = job.steps().stream()
      .map(this::converter)
      .collect(Collectors.toList());


    SimpleJobBuilder jobBuilder = jobBuilderFactory
      .get(job.getClass().getSimpleName())
      .start(springSteps.get(0));

    springSteps.stream().skip(1).forEach(jobBuilder::next);

    org.springframework.batch.core.Job springBatchJob = jobBuilder
      .incrementer(new RunIdIncrementer())
      .build();

    jobParametersBuilder = jobParametersBuilder.getNextJobParameters(springBatchJob);

    org.springframework.batch.core.JobExecution jobExecution = null;
    try {
      jobExecution = this.jobLauncher.run(springBatchJob, jobParametersBuilder.toJobParameters());
    } catch (Exception e) {
      throw new IllegalStateException(e);
    }

    return new JobExecutionImpl(jobExecution);
  }

  private org.springframework.batch.core.Step converter(Tasklet step){
    return this.stepBuilderFactory.get(step.getClass().getSimpleName())
      .tasklet((contribution, chunkContext) -> converter(step.execute(converter(contribution),converter(chunkContext))))
      .build();
  }

  private org.springframework.batch.repeat.RepeatStatus converter(RepeatStatus repeatStatus){
    return org.springframework.batch.repeat.RepeatStatus.valueOf(repeatStatus.name());
  }

  private StepContribution converter(org.springframework.batch.core.StepContribution contribution){
    return new StepContribution();
  }

  private ChunkContext converter(org.springframework.batch.core.scope.context.ChunkContext chunkContext){
    return new ChunkContext() {
      @Override
      public void setAttribute(String name, Object value) {
        chunkContext.setAttribute(name,value);
      }

      @Override
      public Object getAttribute(String name) {
        return chunkContext.getAttribute(name);
      }

      @Override
      public Object removeAttribute(String name) {
        return chunkContext.removeAttribute(name);
      }

      @Override
      public boolean hasAttribute(String name) {
        return chunkContext.hasAttribute(name);
      }

      @Override
      public String[] attributeNames() {
        return chunkContext.attributeNames();
      }
    };
  }
}
