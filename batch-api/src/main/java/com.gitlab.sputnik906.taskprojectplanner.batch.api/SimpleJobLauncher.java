package com.gitlab.sputnik906.taskprojectplanner.batch.api;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class SimpleJobLauncher implements JobLauncher{

  @Override
  public JobExecution run(Job job, Map<String, Object> parameters){
    CompletableFuture.runAsync(()->{
      StepContribution stepContribution = new StepContribution();
      ChunkContext chunkContext = new ChunkContextImpl();
      for(Tasklet step:job.steps()){
        try {
          while(true){
            RepeatStatus status = step.execute(stepContribution,chunkContext);
            if (status.equals(RepeatStatus.FINISHED)) break;
          }
        } catch (Exception e) {
          e.printStackTrace();
          return;
        }
      }
    });
    return new JobExecution() { };
  }

  private static class ChunkContextImpl implements ChunkContext{

    private final Map<String, Object> attributes = new LinkedHashMap<>();

    @Override
    public void setAttribute(String name, Object value) {
      Objects.requireNonNull(name, "Name must not be null");
      if (value != null) {
        this.attributes.put(name, value);
      }
      else {
        removeAttribute(name);
      }
    }

    @Override
    public Object getAttribute(String name) {
      Objects.requireNonNull(name, "Name must not be null");
      return this.attributes.get(name);
    }

    @Override
    public Object removeAttribute(String name) {
      Objects.requireNonNull(name, "Name must not be null");
      return this.attributes.remove(name);
    }

    @Override
    public boolean hasAttribute(String name) {
      Objects.requireNonNull(name, "Name must not be null");
      return this.attributes.containsKey(name);
    }

    @Override
    public String[] attributeNames() {
      return this.attributes.keySet().toArray(new String[0]);
    }
  }
}
