package com.gitlab.sputnik906.taskprojectplanner.batch.api;

public interface ChunkContext {

  void setAttribute(String name,Object value);

  Object getAttribute(String name);

  Object removeAttribute(String name);

  boolean hasAttribute(String name);

  String[] attributeNames();

}
