package com.gitlab.sputnik906.taskprojectplanner.batch.api;

import java.util.List;

public interface Job {
  List<Tasklet> steps();
}
