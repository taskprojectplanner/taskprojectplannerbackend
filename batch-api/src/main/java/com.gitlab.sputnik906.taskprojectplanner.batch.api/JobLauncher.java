package com.gitlab.sputnik906.taskprojectplanner.batch.api;

import java.util.Map;

public interface JobLauncher {
  JobExecution run(Job job, Map<String,Object> parameters);
}
