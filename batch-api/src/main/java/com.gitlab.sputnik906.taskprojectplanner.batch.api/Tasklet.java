package com.gitlab.sputnik906.taskprojectplanner.batch.api;

public interface Tasklet {
  RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception;
}
