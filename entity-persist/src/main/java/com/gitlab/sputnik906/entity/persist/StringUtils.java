package com.gitlab.sputnik906.entity.persist;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StringUtils {
  public static boolean matchCaseInsensitive(String str1, String str2){
    return str1.toLowerCase().equals(str2.toLowerCase());
  }
}
