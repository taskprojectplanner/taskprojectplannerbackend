package com.gitlab.sputnik906.entity.persist.repository;

import com.gitlab.sputnik906.entity.persist.repository.transaction.EntityTransactionManager;
import java.lang.reflect.Proxy;
import javax.persistence.EntityManager;
import lombok.experimental.UtilityClass;

@UtilityClass
public class EntityRepositoryFactory {
  public static EntityRepository withoutTransaction(EntityManager em){
    return new EntityRepositoryImpl(em);
  }
  public static EntityRepository withTransactionManager(EntityManager em, EntityTransactionManager transactionManager){
    EntityRepository entityRepository = withoutTransaction(em);
    return (EntityRepository) Proxy.newProxyInstance(EntityRepository.class.getClassLoader(),
      new Class[] { EntityRepository.class },
      new EntityRepositoryTransactionHandler(entityRepository,transactionManager));
  }
}
