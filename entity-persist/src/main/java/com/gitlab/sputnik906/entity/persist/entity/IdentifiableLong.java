package com.gitlab.sputnik906.entity.persist.entity;

import java.util.Objects;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import lombok.Getter;
import lombok.ToString;

@MappedSuperclass
@ToString
@Getter
public abstract class IdentifiableLong {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected Long id;

  @Version
  protected Long version;

  @Override
  public int hashCode() {
    return id != null ? Objects.hashCode(id) + Objects.hashCode(version) : super.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (id == null) return super.equals(obj);
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    IdentifiableLong other = (IdentifiableLong) obj;
    return Objects.equals(id, other.getId()) && Objects.equals(version, other.getVersion());
  }
}
