package com.gitlab.sputnik906.entity.persist.auth;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Authority {
  @NonNull private final String type;
}
