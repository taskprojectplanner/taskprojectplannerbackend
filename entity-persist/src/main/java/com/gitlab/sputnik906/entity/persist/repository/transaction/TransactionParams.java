package com.gitlab.sputnik906.entity.persist.repository.transaction;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class TransactionParams {
  private boolean readOnly = false;
}
