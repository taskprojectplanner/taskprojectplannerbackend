package com.gitlab.sputnik906.entity.persist.repository;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.entity.persist.repository.QueryEntityRepository;
import com.gitlab.sputnik906.entity.persist.repository.transaction.EntityTransactionManager;
import com.gitlab.sputnik906.entity.persist.repository.transaction.TransactionParams;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class EntityRepositoryTransactionHandler implements InvocationHandler {

  @NonNull private final EntityRepository original;
  @NonNull private final EntityTransactionManager transactionManager;

  @Override
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

    TransactionParams transactionParams = new TransactionParams();
    if (method.getDeclaringClass().equals(QueryEntityRepository.class)) transactionParams.setReadOnly(true);
    return transactionManager.withResult(status-> {
      try {
        return method.invoke(original, args);
      } catch (IllegalAccessException | InvocationTargetException e) {
        throw new IllegalStateException(e);
      }
    },transactionParams);
  }
}
