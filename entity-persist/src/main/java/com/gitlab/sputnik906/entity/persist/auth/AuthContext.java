package com.gitlab.sputnik906.entity.persist.auth;

public class AuthContext {
  private static final ThreadLocal<User> threadLocalScope = new ThreadLocal<>();

  public static User getUser() {
    return threadLocalScope.get();
  }

  public static void setUser(User user) {
    threadLocalScope.set(user);
  }
}
