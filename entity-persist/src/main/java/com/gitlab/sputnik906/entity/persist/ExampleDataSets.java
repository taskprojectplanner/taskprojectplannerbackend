package com.gitlab.sputnik906.entity.persist;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.experimental.UtilityClass;

public class ExampleDataSets {

  public static <D> List<D> rangeGenerator(int startNumber, int count,
    IntFunction<? extends D> mapFun){
    return IntStream.range(startNumber, startNumber + count)
      .mapToObj(mapFun)
      .collect(Collectors.toList());
  }

  public static void applyToRepository(EntityRepository repository, Map<Class<?>,List<?>> dataSet) {
    repository.inTransaction(r->dataSet.forEach((k,v)->repository.persistAll(v)));
  }
}
