package com.gitlab.sputnik906.entity.persist;

import java.beans.BeanInfo;
import java.beans.ConstructorProperties;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
public class BeanUtils {

  public static List<Field> listAllFields(Class<?> clazz) {
    List<Field> fieldList = new ArrayList<>();
    Class<?> tmpClass = clazz;
    while (tmpClass != null) {
      fieldList.addAll(Arrays.asList(tmpClass.getDeclaredFields()));
      tmpClass = tmpClass.getSuperclass();
    }
    return fieldList;
  }

  public static void setField(Object bean,String fieldName,Object value){
    for(Field f:listAllFields(bean.getClass())){
      if (f.getName().equals(fieldName)) {
        setField(f,bean,value);
      }
    }
    throw new IllegalStateException("");

  }

  public static void setField(Field field,Object bean,Object value){
    try {
      field.setAccessible(true);
      field.set(bean,value);
    } catch (IllegalAccessException e) {
      throw new IllegalStateException("", e);
    }
  }

  @SneakyThrows
  public static Object getNestedProperty(Object bean, String propPath){
    String[] props = propPath.split("\\.");

    Object value = bean;
    for(int i=0; i<props.length; i++){
      BeanInfo beanInfo = Introspector.getBeanInfo(value.getClass());
      int finalI = i;
      PropertyDescriptor propertyDescriptor = Stream.of(beanInfo.getPropertyDescriptors())
        .filter(p->p.getName().equals(props[finalI]))
        .findFirst()
        .orElseThrow(IllegalArgumentException::new);

      value = propertyDescriptor.getReadMethod().invoke(value);

    }

    return value;

  }

  @SneakyThrows
  public static void setProperty(Object bean, String propPath, Object value) {
    String[] props = propPath.split("\\.");

    Object currentBean = bean;
    for(int i=0; i<props.length; i++){

      BeanInfo beanInfo = Introspector.getBeanInfo(currentBean.getClass());
      int finalI = i;
      PropertyDescriptor propertyDescriptor = Stream.of(beanInfo.getPropertyDescriptors())
        .filter(p->p.getName().equals(props[finalI]))
        .filter(p->finalI==props.length-1
          ?p.getWriteMethod()!=null
          :p.getReadMethod()!=null)
        .findFirst()
        .orElseThrow(IllegalArgumentException::new);

      if (i==props.length-1){
        propertyDescriptor.getWriteMethod().invoke(currentBean,value);
      }else{
        currentBean = propertyDescriptor.getReadMethod().invoke(currentBean);
      }
    }
  }

  public static void setProperties(Object bean, Map<String,Object> patch) {
    Map<String, Object> flatMap = convertToFlatMap(patch);
    for(String propPath:flatMap.keySet()){
      setProperty(bean,propPath,flatMap.get(propPath));
    }
  }

  public static  Map<String, Object> convertToFlatMap(Map<String, Object> map){
    Map<String,Object> result = new HashMap<>();
    for(String key:map.keySet()){
      Object value = map.get(key);
      if (Map.class.isAssignableFrom(value.getClass())){
        convertToFlatMap((Map<String,Object>)value).forEach((k,v)->result.put(key+"."+k,v));
      }else{
        result.put(key,value);
      }
    }
    return result;
  }

  public  static Map<String, Class<?>> convertConstructorBeanToFlatMap(Class<?> beanClass){
    Constructor<?> beanConstructor = findFirstBeanConstructor(beanClass);
    if (beanConstructor==null) return null;

    Map<String, Class<?>> result = new HashMap<>();

    String[] props = beanConstructor.getAnnotation(ConstructorProperties.class).value();
    Class<?>[] paramClasses = beanConstructor.getParameterTypes();

    for (int i = 0; i < props.length; i++) {
      Map<String, Class<?>> nestedMap = convertConstructorBeanToFlatMap(paramClasses[i]);
      if (nestedMap==null){
        result.put(props[i],paramClasses[i]);
      }else{
        int finalI = i;
        nestedMap.forEach((k,v)->{
          result.put(props[finalI]+"."+k,v);
        });
      }


    }

    return result;

  }

  public static <T> Constructor<T> findFirstBeanConstructor(Class<T> sourceClass){
    return Stream.of(sourceClass.getDeclaredConstructors())
      .filter(c-> Modifier.isPublic(c.getModifiers()))
      .filter(c->c.isAnnotationPresent(ConstructorProperties.class))//generate lombook
      .filter(c->c.getParameterCount()>0)
      .findFirst()
      .map(c->getDeclaredConstructor(sourceClass,c))
      .orElse(null);
  }

  @SneakyThrows
  private static <T> Constructor<T> getDeclaredConstructor(Class<T> sourceClass,Constructor<?> c){
    return sourceClass.getDeclaredConstructor(c.getParameterTypes());
  }

  public  static Map<String, Object> convertBeanToMap(Object bean){
    return convertBeanToMap(bean,new HashMap<>());
  }

  @SneakyThrows
  private  static Map<String, Object> convertBeanToMap(Object bean,
    Map<Object,Map<String, Object>> alreadyInspected){

    Class<?> beanClass = bean.getClass();
    Map<String, Object> result = new HashMap<>();

    alreadyInspected.put(bean,result);

    for(PropertyDescriptor propertyDescriptor :
      Introspector.getBeanInfo(beanClass).getPropertyDescriptors()){

      Method readMethod = propertyDescriptor.getReadMethod();

      if (readMethod != null) {
        Class<?> propertyClass = propertyDescriptor.getPropertyType();

        if (propertyClass.equals(Class.class)) continue;

        Object value = readMethod.invoke(bean);

        result.put(
          propertyDescriptor.getName(),
          handleMapValue(value,alreadyInspected)
        );

      }

    }

    return result;
  }

  private static Object handleMapValue(Object value,
    Map<Object,Map<String, Object>> alreadyInspected){

    if (value==null||value instanceof String|| value instanceof Number){
      return value;
    }
    if (alreadyInspected.containsKey(value)){// cycle reference
      return alreadyInspected.get(value);
    }
    if (Map.class.isAssignableFrom(value.getClass())){
      Map<String, Object> nestedMap = new HashMap<>();
      ((Map<?,?>)value).forEach((k,v)->{
        nestedMap.put(k.toString(),handleMapValue(v,alreadyInspected));
      });
      return nestedMap;
    }
    if (Collection.class.isAssignableFrom(value.getClass())){
      return  ((Collection<?>)value).stream()
        .map(e->handleMapValue(e,alreadyInspected))
        .collect(getCollectors(value.getClass()));
    }

    return convertBeanToMap(value,alreadyInspected);
  }

  private static <T> Collector<T, ?, ?> getCollectors(Class<?> collectClass){
    if (Set.class.isAssignableFrom(collectClass)) return Collectors.toSet();
    if (List.class.isAssignableFrom(collectClass)) return Collectors.toList();
    throw new IllegalArgumentException();
  }




}
