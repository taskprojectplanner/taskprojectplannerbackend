package com.gitlab.sputnik906.entity.persist.auth;

import java.util.Collection;

public interface User {

  String getName();

  String getEmail();

  Collection<Authority> getAuthorities();
}
