package com.gitlab.sputnik906.entity.persist;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Validations {
  public static void shouldBeTrue(boolean condition,String errorMessage){
    if (!condition) throw new IllegalArgumentException(errorMessage);
  }
  public static void shouldBeTrue(boolean condition){
    shouldBeTrue(condition,"Incorrect Argument");
  }
}
