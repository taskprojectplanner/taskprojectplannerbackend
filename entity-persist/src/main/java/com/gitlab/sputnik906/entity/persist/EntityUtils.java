package com.gitlab.sputnik906.entity.persist;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.Id;
import lombok.experimental.UtilityClass;

@UtilityClass
public class EntityUtils {
  public static void generateId(Map<Class<?>, List<?>> dataset){
    for(Map.Entry<Class<?>, List<?>> entry: dataset.entrySet()){
      Field idField = getIdFieldOrException(entry.getKey());
      if (idField.getType().equals(Long.class)){
        AtomicLong id = new AtomicLong(1L);
        entry.getValue().forEach(e->BeanUtils.setField(idField,e, id.getAndIncrement()));
      }
      if (idField.getType().equals(Integer.class)){
        AtomicInteger id = new AtomicInteger(1);
        entry.getValue().forEach(e->BeanUtils.setField(idField,e, id.getAndIncrement()));
      }
      if (idField.getType().equals(String.class)){
        entry.getValue().forEach(e->BeanUtils.setField(idField,e, UUID.randomUUID().toString()));
      }
    }

  }

  public static Optional<Field> getIdField(Class<?> entityClass){
    return BeanUtils.listAllFields(entityClass).stream()
      .filter(f->f.isAnnotationPresent(Id.class))
      .findFirst();
  }

  public static Field getIdFieldOrException(Class<?> entityClass){
    return getIdField(entityClass).orElseThrow(IllegalArgumentException::new);
  }
}
