package com.gitlab.sputnik906.entity.persist;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Stream;
import javax.persistence.Entity;
import lombok.Value;
import lombok.experimental.UtilityClass;

@UtilityClass
public class PredictUtils {
  public static boolean predict(Map<String,Object> entity, String where){
    if (where==null) return true;
    String[] operators = new String[]{"=",">","<",">=","<=","!=","<>"," like "," is "," between "," in ", " member of "};//TODO between and 'and'
    String[] conditions = where.split(" and ");
    for(String conditionStr:conditions){
      Condition condition = parseCondition(conditionStr,operators);
      if (condition==null) throw new IllegalArgumentException("Cant parse: "+conditionStr);
      Object entityValue = condition.getOperation().equals("member of")
        ?getEntityValue(condition.rightOperand,entity)
        :getEntityValue(condition.leftOperand,entity);
      if(!checkCondition(condition,entityValue)) return false;
    }
    return true;
  }

  private static boolean checkCondition(Condition condition,Object entityValue){
    if (condition.getOperation().equals("is")){
      return condition.rightOperand.contains("not") == (entityValue != null);
    }

    if (entityValue==null) return false;

    switch (condition.getOperation()) {
      case ">":
        return compare(entityValue,condition.rightOperand)>0;
      case "<":
        return compare(entityValue,condition.rightOperand)<0;
      case ">=":
        return compare(entityValue,condition.rightOperand)>=0;
      case "<=":
        return compare(entityValue,condition.rightOperand)<=0;
      case "=":
        return entityValue.equals(convertValueFromStr(entityValue.getClass(),condition.rightOperand));
      case "!=":
      case "<>":
        return !entityValue.equals(convertValueFromStr(entityValue.getClass(),condition.rightOperand));
      case "like":
        return condition.isNot() != entityValue.toString().contains(condition.rightOperand.replace("%",""));
      case "in":
        return condition.isNot() != Stream.of(
          condition.rightOperand.substring(1,condition.rightOperand.length()-1) // remove (  and )
            .split(",")
        ).anyMatch(v->entityValue.toString().equals(v.trim()));
      case "member of":
        Collection<?> collectionValue = (Collection<?>)entityValue;
        if (collectionValue.isEmpty()) return condition.isNot();
        if (collectionValue.iterator().next().getClass().isAnnotationPresent(Entity.class)){
          return condition.isNot() != collectionValue.stream()
            .anyMatch(e->BeanUtils.getNestedProperty(e,"id").toString().equals(condition.leftOperand.trim()));
        }else{
          return condition.isNot() != collectionValue.stream()
            .anyMatch(e->e.toString().equals(condition.leftOperand.trim()));
        }
      default:
        throw new IllegalArgumentException("Unsupported operation " + condition.getOperation());
    }
  }

  public static Integer getParamIndexOrNull(String str){
    String trimmedStr = str.trim();
    return trimmedStr.startsWith("?")
      ?Integer.valueOf(trimmedStr.substring(1))
      :null;
  }

  private static Object getEntityValue(String expression,Map<String,Object> entity){
    Object value = entity.get(expression);
    if (value!=null) return value;

    for(String op:new String[]{" - "," + "," / "," * "}){
      String[] sp = expression.split(op);
      if (sp.length>1){
        Object v1 = entity.get(sp[0]);
        Object v2 = entity.get(sp[1]);
        if (v1==null || v2==null) return null;

        switch (op) {
          case " - ":
            if (v1 instanceof Integer) return (Integer)v1 - (Integer)v2;
            if (v1 instanceof Long) return (Long)v1 - (Long)v2;
            if (v1 instanceof Float) return (Float)v1 - (Float)v2;
            if (v1 instanceof Double) return (Double)v1 - (Double)v2;
            break;
          case " + ":
            if (v1 instanceof Integer) return (Integer)v1 + (Integer)v2;
            if (v1 instanceof Long) return (Long)v1 + (Long)v2;
            if (v1 instanceof Float) return (Float)v1 + (Float)v2;
            if (v1 instanceof Double) return (Double)v1 + (Double)v2;
            break;
          case " / ":
            if (v1 instanceof Integer) return (Integer)v1 / (Integer)v2;
            if (v1 instanceof Long) return (Long)v1 / (Long)v2;
            if (v1 instanceof Float) return (Float)v1 / (Float)v2;
            if (v1 instanceof Double) return (Double)v1 / (Double)v2;
            break;
          case " * ":
            if (v1 instanceof Integer) return (Integer)v1 * (Integer)v2;
            if (v1 instanceof Long) return (Long)v1 * (Long)v2;
            if (v1 instanceof Float) return (Float)v1 * (Float)v2;
            if (v1 instanceof Double) return (Double)v1 * (Double)v2;
            break;
        }
        throw new IllegalArgumentException("Unsupported operation " + op);
      }
    }

    return null;
  }



  private static int compare(Object o1, String str1){
    return ((Comparable<Object>)o1).compareTo(convertValueFromStr(o1.getClass(),str1));
  }

  private static Condition parseCondition(String condition,String[] operators){
    for(String op:operators){
      String[] sp = condition.split(op);
      if (sp.length>1){
        return new Condition(
          sp[0].replace(" not ","").trim(),
          op.trim(),
          sp[1].trim().replace("'",""),
          sp[0].contains(" not ")
        );
      }
    }
    return null;
  }

  @Value
  private static class Condition{
    private final String leftOperand;
    private final String operation;
    private final String rightOperand;
    private final boolean not;
  }

  private static Object convertValueFromStr(Class<?> fieldClass, String valueStr) {
    if (fieldClass.equals(String.class)) return valueStr;
    else if (fieldClass.equals(int.class)||fieldClass.equals(Integer.class)) return Integer.valueOf(valueStr);
    else if (fieldClass.equals(long.class)||fieldClass.equals(Long.class)) return Long.valueOf(valueStr);
    else if (fieldClass.equals(float.class)||fieldClass.equals(Float.class)) return Float.valueOf(valueStr);
    else if (fieldClass.equals(double.class)||fieldClass.equals(Double.class)) return Double.valueOf(valueStr);
    else {
      try {
        return fieldClass.getMethod("valueOf", String.class).invoke(null, valueStr);
      } catch (RuntimeException e) {
        throw e;
      } catch (Exception e) {
        throw new IllegalArgumentException(
          "Cant deserialize from str "
            + valueStr
            + " to instance of "
            + fieldClass
            + ". Set static method valueOf(String) for this class",
          e);
      }
    }
  }
}
