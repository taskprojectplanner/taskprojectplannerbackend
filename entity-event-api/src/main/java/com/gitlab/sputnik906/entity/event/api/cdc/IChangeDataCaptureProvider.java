package com.gitlab.sputnik906.entity.event.api.cdc;

import com.gitlab.sputnik906.entity.event.api.repo.EntityEventQuery;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public interface IChangeDataCaptureProvider {

  List<SnapshotEntityResponse<?>> getSnapshotEntityList(List<SnapshotEntityRequest> snapshotEntityRequests);

  default SnapshotEntityResponse<?> getSnapshotEntity(SnapshotEntityRequest snapshotEntityRequest){
    return getSnapshotEntityList(Collections.singletonList(snapshotEntityRequest)).get(0);
  }

  ChangesEntities getChanges(List<EntityEventQuery> changesRequests);

  default ChangesEntities getChanges(EntityEventQuery changesRequest){
    return getChanges(Collections.singletonList(changesRequest));
  }

}
