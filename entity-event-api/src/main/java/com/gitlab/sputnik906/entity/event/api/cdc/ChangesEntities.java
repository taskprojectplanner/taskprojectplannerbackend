package com.gitlab.sputnik906.entity.event.api.cdc;

import com.gitlab.sputnik906.entity.event.api.type.AbstractEntityEvent;
import com.gitlab.sputnik906.entity.event.api.repo.EntityEventQuery;
import java.util.List;
import lombok.NonNull;
import lombok.Value;

@Value
public class ChangesEntities {
  @NonNull private final List<AbstractEntityEvent<?>> changes;
  @NonNull private final List<EntityEventQuery> nextChangesRequest;

}
