package com.gitlab.sputnik906.entity.event.api;

public interface AuthorProvider {
    String provide();
}
