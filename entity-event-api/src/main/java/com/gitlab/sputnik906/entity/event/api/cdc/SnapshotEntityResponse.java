package com.gitlab.sputnik906.entity.event.api.cdc;

import com.gitlab.sputnik906.entity.event.api.repo.EntityEventQuery;
import java.util.List;
import lombok.NonNull;
import lombok.Value;

@Value
public class SnapshotEntityResponse<T> {
  @NonNull private final List<T> entities;
  @NonNull private final EntityEventQuery changesRequest;
  private final SnapshotEntityRequest nextSnapshotEntityRequest;
}
