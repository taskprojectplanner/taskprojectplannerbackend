package com.gitlab.sputnik906.entity.event.api.cdc;

import java.io.Serializable;
import java.util.Set;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class SnapshotEntityRequest {
  @NonNull
  private final String entityType;

  private final Integer count;

  private final String where;

  private final Set<String> eventTypes;

  private final Set<String> propertyNames;

  private final Serializable fromEntityId;

  public SnapshotEntityRequest withFromEntityId(Serializable fromEntityId){
    return new SnapshotEntityRequest(
      entityType,
      count,
      where,
      eventTypes,
      propertyNames,
      fromEntityId
    );
  }

}
