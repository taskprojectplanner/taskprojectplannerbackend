package com.gitlab.sputnik906.entity.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;

import java.io.IOException;
import java.util.*;

public class EntityDeserializer extends JsonDeserializer<Object> {

  private final EntityDescription entityDescription;
  private final SettableBeanProperty prop;

  public EntityDeserializer(EntityDescription entityDescription, SettableBeanProperty prop) {
    this.entityDescription = entityDescription;
    this.prop = prop;
  }

  @Override
  public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
    JsonNode node = p.getCodec().readTree(p);
    if (node.isArray()) {
      Class<?> elementType = prop.getType().containedType(0).getRawClass();
      Collection<Object> result = prop.getType().isTypeOrSubTypeOf(List.class) ? new ArrayList<>() : new HashSet<>();
      for (final JsonNode objNode : node) {
        JsonNode idNode = objNode.get(entityDescription.idField);
        if (idNode.isNull()){
          result.add(p.getCodec().treeToValue(objNode,entityDescription.domainClass));
        }else{
          Object id = idNode.isNumber()
            ?idNode.isLong()
            ?idNode.asLong()
            :idNode.asInt()
            :idNode.asText();
          Object entity = BeanUtils.invokeConstructor(elementType);
          BeanUtils.setField(entity,entityDescription.idField,id);
          if (entityDescription.versionField!=null){
            JsonNode versionNode = objNode.get(entityDescription.versionField);
            if (versionNode!=null&&versionNode.isNumber()) BeanUtils.setField(entity,entityDescription.versionField,versionNode.asLong());
          }
          if (entityDescription.labelField!=null){
            JsonNode labelNode = objNode.get(entityDescription.labelField);
            if (labelNode!=null&&labelNode.isTextual()) BeanUtils.setField(entity,entityDescription.labelField,labelNode.asText());
          }
          result.add(entity);
        }


      }
      return result;
    } else {
      JsonNode idNode = node.get(entityDescription.idField);
      if (idNode.isNull()){
        return p.getCodec().treeToValue(node,entityDescription.domainClass);
      }else{
        Object id = idNode.isNumber()
          ?idNode.isLong()
          ?idNode.asLong()
          :idNode.asInt()
          :idNode.asText();
        Object entity = BeanUtils.invokeConstructor(prop.getType().getRawClass());
        BeanUtils.setField(entity,entityDescription.idField,id);
        if (entityDescription.versionField!=null){
          JsonNode versionNode = node.get(entityDescription.versionField);
          if (versionNode!=null&&versionNode.isNumber()) BeanUtils.setField(entity,entityDescription.versionField,versionNode.asLong());
        }
        if (entityDescription.labelField!=null){
          JsonNode labelNode = node.get(entityDescription.labelField);
          if (labelNode!=null&&labelNode.isTextual()) BeanUtils.setField(entity,entityDescription.labelField,labelNode.asText());
        }
        return entity;
      }

    }

  }



}

