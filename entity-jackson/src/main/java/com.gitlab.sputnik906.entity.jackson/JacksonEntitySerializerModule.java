package com.gitlab.sputnik906.entity.jackson;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.BeanDeserializerBuilder;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class JacksonEntitySerializerModule extends Module {

  @Override
  public String getModuleName() {
    return getClass().getSimpleName();
  }

  @Override
  public Version version() {
    return Version.unknownVersion();
  }

  private boolean checkSupportAndPutCache(JavaType type){

    if (knowEntitySerializerCache.get(type)!=null) return true;

    Class<?> fieldClass = Collection.class.isAssignableFrom(type.getRawClass())
      ?type.getContentType().getRawClass()
      :type.getRawClass();

    if (!fieldClass.isAnnotationPresent(Entity.class)) return false;

    String idField =  BeanUtils.listAllNonStaticFields(fieldClass).stream()
      .filter(f->f.isAnnotationPresent(Id.class))
      .map(Field::getName)
      .findFirst()
      .orElse("id");

    String versionField =  BeanUtils.listAllNonStaticFields(fieldClass).stream()
      .filter(f->f.isAnnotationPresent(javax.persistence.Version.class))
      .map(Field::getName)
      .findFirst()
      .orElse(null);

    String labelField = BeanUtils.listAllNonStaticFields(fieldClass).stream()
      .filter(f->f.getType().equals(String.class))
      .map(Field::getName)
      .filter(n->n.contains("label")||n.contains("title"))
      .findFirst()
      .orElse(null);

    return  knowEntitySerializerCache.putIfAbsent(type,new EntitySerializer(new EntityDescription(fieldClass,idField,versionField,labelField)))==null;
  }



  private static final ConcurrentHashMap<JavaType,EntitySerializer> knowEntitySerializerCache = new ConcurrentHashMap<>();

  @Override
  public void setupModule(SetupContext context) {

    context.addBeanSerializerModifier(new BeanSerializerModifier() {
      @Override
      public List<BeanPropertyWriter> changeProperties(SerializationConfig config, BeanDescription beanDesc, List<BeanPropertyWriter> beanProperties) {
        beanProperties.stream()
          .filter(bw ->checkSupportAndPutCache(bw.getType()))
          .forEach(bw -> bw.assignSerializer(knowEntitySerializerCache.get(bw.getType())));
        return beanProperties;
      }
    });

    context.addBeanDeserializerModifier(new BeanDeserializerModifier() {

      @Override
      public BeanDeserializerBuilder updateBuilder(DeserializationConfig config, BeanDescription beanDesc, BeanDeserializerBuilder builder) {

        SettableBeanProperty[] props = builder.getValueInstantiator().getFromObjectArguments(config);
        if (props != null) {
          for (int i = 0; i < props.length; i++) {
            props[i] = changeDeserForPropertyIfRequired(props[i]);
          }
        }

        Iterator<SettableBeanProperty> propertyIterable = builder.getProperties();
        while (propertyIterable.hasNext()) {
          builder.addOrReplaceProperty(changeDeserForPropertyIfRequired(propertyIterable.next()), true);
        }

        return super.updateBuilder(config, beanDesc, builder);
      }

      private SettableBeanProperty changeDeserForPropertyIfRequired(SettableBeanProperty prop) {
        if (checkSupportAndPutCache(prop.getType())) {
          EntityDescription entityDescription = knowEntitySerializerCache.get(prop.getType()).getEntityDescription();
          return prop.withValueDeserializer(new EntityDeserializer(entityDescription,prop));
        }
        return prop;
      }

    });
  }


}

