package com.gitlab.sputnik906.entity.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.lang.reflect.Field;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import lombok.Getter;

import java.io.IOException;
import java.util.Collection;

public class EntitySerializer extends JsonSerializer<Object> {

  @Getter
  private final EntityDescription entityDescription;

  public EntitySerializer(EntityDescription entityDescription) {
    this.entityDescription = entityDescription;
  }

  @Override
  public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
    if (Collection.class.isAssignableFrom(value.getClass())) {
      gen.writeStartArray();
      for (Object v : ((Collection<?>) value)) writeEntity(gen, v, serializers);
      gen.writeEndArray();
    } else {
      writeEntity(gen, value, serializers);
    }
  }

  private void writeEntity(JsonGenerator gen, Object bean, SerializerProvider serializers) throws IOException {
    Object idValue = BeanUtils.getField(bean, entityDescription.idField);

    //serializers.

    if (idValue==null){
      gen.writeStartObject();
      for(Field field:BeanUtils.listAllNonStaticFields(bean.getClass())){
        if (field.isAnnotationPresent(Id.class)) continue;
        if (field.isAnnotationPresent(Version.class)) continue;
        if (field.isAnnotationPresent(ManyToOne.class)) continue;
        String fieldName = field.getName();
        Object fieldValue = BeanUtils.getField(bean,fieldName);
        serializers.defaultSerializeField(fieldName,fieldValue,gen);
      }
      gen.writeEndObject();
      //gen.writeObject(bean);
      return;
    }

    if (entityDescription.writeOnlyId.get()){
      gen.writeObject(idValue);
    }else{
      gen.writeStartObject();

      if (idValue instanceof Number) gen.writeNumberField( entityDescription.idField, (Long)idValue);
      else  gen.writeStringField(entityDescription.idField, idValue.toString());

      if (entityDescription.versionField!=null){
        Long version = (Long) BeanUtils.getField(bean, entityDescription.versionField);
        gen.writeNumberField(entityDescription.versionField, version);
      }
      if (entityDescription.labelField!=null){
        String label = (String) BeanUtils.getField(bean, entityDescription.labelField);
        gen.writeStringField(entityDescription.labelField, label);
      }

      gen.writeEndObject();
    }

  }




}

