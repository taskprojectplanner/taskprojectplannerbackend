package com.gitlab.sputnik906.entity.jackson;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import org.apache.commons.beanutils.ConstructorUtils;
import org.apache.commons.beanutils.PropertyUtils;

@UtilityClass
public class BeanUtils {
  public static List<Field> listAllNonStaticFields(Class<?> clazz) {
    List<Field> fieldList = new ArrayList<>();
    Class<?> tmpClass = clazz;
    while (tmpClass != null) {
      fieldList.addAll(Arrays.stream(tmpClass.getDeclaredFields())
        .filter(f-> !Modifier.isStatic(f.getModifiers()))
        .collect(Collectors.toList()));
      tmpClass = tmpClass.getSuperclass();
    }
    return fieldList;
  }
  public static Object invokeConstructor(Class<?> clazz){
    try {
      return  ConstructorUtils.invokeConstructor(clazz,new Object[0]);
    } catch (Exception e) {
      throw new IllegalStateException("", e);
    }

  }

  public static void setField(Object bean,String fieldName,Object value){
    for(Field f:listAllNonStaticFields(bean.getClass())){
      if (f.getName().equals(fieldName)) {
        setField(f,bean,value);
      }
    }
    throw new IllegalStateException("");

  }

  public static void setField(Field field,Object bean,Object value){
    try {
      field.setAccessible(true);
      field.set(bean,value);
    } catch (IllegalAccessException e) {
      throw new IllegalStateException("", e);
    }
  }

  public static Object getField(Object bean,String fieldName){
    try {
      return PropertyUtils.getProperty(bean, fieldName);
    } catch (Exception e) {
      throw new IllegalStateException("", e);
    }

  }
}
