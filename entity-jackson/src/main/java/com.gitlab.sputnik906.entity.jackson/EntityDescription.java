package com.gitlab.sputnik906.entity.jackson;

import java.util.concurrent.atomic.AtomicBoolean;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EntityDescription {
  public final Class<?> domainClass;
  public final String idField;
  public final String versionField;
  public final String labelField;
  public final AtomicBoolean writeOnlyId = new AtomicBoolean(true);
}
