package com.gitlab.sputnik906.tpp.spring.app.aggregation.handler;

import com.gitlab.sputnik906.tpp.spring.app.aggregation.IAggregationEntityHandler;

public class MaxAggregationEntityHandler implements IAggregationEntityHandler {

  @Override
  public Object handlePlusEntity(String fieldName, Object fieldValue,
    Object oldAggregationValue) {
    if (fieldValue instanceof Double) return Math.max((Double) fieldValue,(Double)oldAggregationValue);
    if (fieldValue instanceof Long) return Math.max((Long) fieldValue,(Long)oldAggregationValue);
    if (fieldValue instanceof Integer) return Math.max((Integer) fieldValue,(Integer)oldAggregationValue);
    if (fieldValue instanceof Float) return Math.max((Float) fieldValue,(Float)oldAggregationValue);
    throw new IllegalArgumentException();
  }

  @Override
  public Object handleMinusEntity(String fieldName, Object fieldValue,
    Object oldAggregationValue) {
    return fieldValue.equals(oldAggregationValue)
      ?null
      :oldAggregationValue;
  }

  @Override
  public String aggregateFunName() {
    return "max";
  }
}
