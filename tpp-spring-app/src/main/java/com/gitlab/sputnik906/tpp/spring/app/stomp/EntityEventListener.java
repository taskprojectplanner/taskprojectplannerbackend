package com.gitlab.sputnik906.tpp.spring.app.stomp;

import com.gitlab.sputnik906.entity.event.api.TransactionEvents;
import com.gitlab.sputnik906.entity.event.api.type.AddToCollectionEvent;
import com.gitlab.sputnik906.entity.event.api.type.AddToMapEvent;
import com.gitlab.sputnik906.entity.event.api.type.EntityDeleteEvent;
import com.gitlab.sputnik906.entity.event.api.type.EntityPersistEvent;
import com.gitlab.sputnik906.entity.event.api.type.EntityUpdateEvent;
import com.gitlab.sputnik906.entity.event.api.type.RemoveFromCollectionEvent;
import com.gitlab.sputnik906.entity.event.api.type.RemoveFromMapEvent;
import com.gitlab.sputnik906.tpp.spring.app.aggregation.EntityAggregationRegistrator;
import com.gitlab.sputnik906.tpp.spring.app.aggregation.EntityAggregationRegistrator.AggregationSubscription;
import com.gitlab.sputnik906.tpp.spring.app.aggregation.EntityAggregationRegistrator.ChangeAggregationEvent;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

@Component
@Lazy(false)
public class EntityEventListener {

  @Autowired private SimpMessageSendingOperations messagingTemplate;

  @Autowired private EntityAggregationRegistrator aggregationRegistrator;

  private final Map<String, Map<String,Consumer<ChangeAggregationEvent>>> sessionAggSubsMap =
    Collections.synchronizedMap(new HashMap<>());

  @TransactionalEventListener
  public void handler(TransactionEvents transactionEvents) {

    CompletableFuture.runAsync(()->{
      transactionEvents
        .getEvents()
        .forEach(
          e -> {
            String[] entityTypeSplit = e.getEntityType().split("\\.");

            String topic =
              String.format(
                "/topic/%s/%s/%s",
                e.getEventType(),
                entityTypeSplit[entityTypeSplit.length - 1],
                e.getEntityId());

            Map<String, Object> headers =
              of(
                "transactionId", e.getMetadata().getTransactionId(),
                "timestamp", e.getMetadata().getTimestamp(),
                "author", e.getMetadata().getAuthor());

            if (e instanceof EntityPersistEvent) {
              EntityPersistEvent<?> c = (EntityPersistEvent<?>) e;
              messagingTemplate.convertAndSend(
                topic, c.getStates(), headers);
              return;
            }

            if (e instanceof EntityDeleteEvent) {
              EntityDeleteEvent<?> d = (EntityDeleteEvent<?>) e;
              messagingTemplate.convertAndSend(
                topic, new HashMap<>(), headers);

              return;
            }

            if (e instanceof EntityUpdateEvent) {
              EntityUpdateEvent<?> u = (EntityUpdateEvent<?>) e;

              messagingTemplate.convertAndSend(
                topic, new UpdateEntityMessage(u.getOldStates(), u.getNewStates()), headers);

              for (String propName : u.getOldStates().keySet()) {
                messagingTemplate.convertAndSend(
                  topic + "/" + propName,
                  new UpdateEntityMessage(
                    of(propName, u.getOldStates().get(propName)),
                    of(propName, u.getNewStates().get(propName))),
                  headers);
              }

              return;
            }

            if (e instanceof AddToCollectionEvent) {
              AddToCollectionEvent<?> ac = (AddToCollectionEvent<?>) e;
              messagingTemplate.convertAndSend(
                topic + "/" + ac.getPropertyName(), ac.getAddedValues(), headers);
              messagingTemplate.convertAndSend(
                topic, of(ac.getPropertyName(), ac.getAddedValues()), headers);
              return;
            }

            if (e instanceof AddToMapEvent) {
              AddToMapEvent<?> am = (AddToMapEvent<?>) e;
              messagingTemplate.convertAndSend(
                topic + "/" + am.getPropertyName(), am.getAddedEntries(), headers);
              messagingTemplate.convertAndSend(
                topic, of(am.getPropertyName(), am.getAddedEntries()), headers);
              return;
            }

            if (e instanceof RemoveFromCollectionEvent) {
              RemoveFromCollectionEvent<?> rc = (RemoveFromCollectionEvent<?>) e;
              messagingTemplate.convertAndSend(
                topic + "/" + rc.getPropertyName(), rc.getRemovedValues(), headers);

              messagingTemplate.convertAndSend(
                topic, of(rc.getPropertyName(), rc.getRemovedValues()), headers);
              return;
            }

            if (e instanceof RemoveFromMapEvent) {
              RemoveFromMapEvent<?> rm = (RemoveFromMapEvent<?>) e;
              messagingTemplate.convertAndSend(
                topic + "/" + rm.getPropertyName(), rm.getRemovedEntries(), headers);
              messagingTemplate.convertAndSend(
                topic, of(rm.getPropertyName(), rm.getRemovedEntries()), headers);

            }
          });
    });

  }


  @EventListener
  public void handleSessionSubscribeEvent(SessionSubscribeEvent subscribeEvent) {
    String topic = (String)subscribeEvent.getMessage().getHeaders().get("simpDestination");
    String sessionId = (String)subscribeEvent.getMessage().getHeaders().get("simpSessionId");
    String subId = (String)subscribeEvent.getMessage().getHeaders().get("simpSubscriptionId");
    Map<String, List<?>> nativeHeaders = (Map<String, List<?>>)subscribeEvent.getMessage().getHeaders().get("nativeHeaders");
    String where = nativeHeaders.get("where")!=null?(String)nativeHeaders.get("where").get(0):null;

    if (AggSubscription.is(topic)){

      Consumer<ChangeAggregationEvent> consumer = (e)-> messagingTemplate.convertAndSend(
        topic,
        e.getSub().getCurrentValue(),
        e.getMetadata()
      );

      String[] splittedTopic = topic.split("/");
      String entityName = splittedTopic[3];
      String aggregateFunName = splittedTopic[4];
      String field = splittedTopic.length>5?splittedTopic[5]:null;

      sessionAggSubsMap.computeIfAbsent(sessionId,(k)->Collections.synchronizedMap(new HashMap<>()))
        .put(subId,consumer);

      aggregationRegistrator.register(
        entityName,
        aggregateFunName,
        field,
        where,
        consumer,
        true
      );
    }

  }

  @EventListener
  public void handleSessionUnSubscribeEvent(SessionUnsubscribeEvent unsubscribeEvent) {
    String sessionId = (String)unsubscribeEvent.getMessage().getHeaders().get("simpSessionId");
    String subId = (String)unsubscribeEvent.getMessage().getHeaders().get("simpSubscriptionId");
    Consumer<ChangeAggregationEvent> consumer = sessionAggSubsMap.get(sessionId).remove(subId);
    aggregationRegistrator.unRegister(consumer);

  }

  @EventListener
  public void handleSessionDisconnectEvent(SessionDisconnectEvent disconnectEvent) {
    String sessionId = (String)disconnectEvent.getMessage().getHeaders().get("simpSessionId");

    Map<String,Consumer<ChangeAggregationEvent>> subs = sessionAggSubsMap.remove(sessionId);

    if (subs!=null) subs.values().forEach(v-> aggregationRegistrator.unRegister(v));

  }

  @Value
  private static class UpdateEntityMessage{
    private final Map<String,Object> oldStates;
    private final Map<String,Object> newStates;
  }

  @Value
  private static class AggSubscription{
    private final String topic;
    private final String subId;
    private final Consumer<AggregationSubscription> consumer;

    public static boolean is(String topic){
      return topic.startsWith("/topic/a/");
    }
  }


  private static Map<String, Object> of(Object... objs) {
    Map<String, Object> map = new HashMap<>();
    for (int i = 0; i < objs.length; i += 2) map.put((String) objs[i], objs[i + 1]);
    return map;
  }
}
