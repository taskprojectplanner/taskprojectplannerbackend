package com.gitlab.sputnik906.tpp.spring.app;

import com.gitlab.sputnik906.tpp.domain.PlanService;
import com.gitlab.sputnik906.tpp.domain.TppDataSets;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class LoadDataSetForDevProfile implements ApplicationRunner {

  @Override
  //@Transactional
  public void run(ApplicationArguments args) {
    TppDataSets.applyToRepository(
      PlanService.instance.getRepository(),
      TppDataSets.devDataSet()
    );
    TppDataSets.applyDevEvents(PlanService.instance.getRepository());
  }
}
