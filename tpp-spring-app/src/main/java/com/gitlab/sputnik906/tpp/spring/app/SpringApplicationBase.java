package com.gitlab.sputnik906.tpp.spring.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringApplicationBase {
  public static void main(String[] args) {
    SpringApplication.run(SpringApplicationBase.class, args);
  }

}
