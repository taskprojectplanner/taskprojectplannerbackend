package com.gitlab.sputnik906.tpp.spring.app.aggregation.handler;

import com.gitlab.sputnik906.tpp.spring.app.aggregation.IAggregationEntityHandler;

public class CountAggregationEntityHandler implements IAggregationEntityHandler {

  @Override
  public Object handlePlusEntity(String fieldName,Object fieldValue, Object oldAggregationValue) {
    return fieldName==null||fieldValue!=null
      ?(Long)oldAggregationValue+1
      :oldAggregationValue;
  }

  @Override
  public Object handleMinusEntity(String fieldName,Object fieldValue, Object oldAggregationValue) {
    return fieldName==null||fieldValue!=null
      ?(Long)oldAggregationValue-1
      :oldAggregationValue;
  }

  @Override
  public String aggregateFunName() {
    return "count";
  }
}
