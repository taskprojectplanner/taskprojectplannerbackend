package com.gitlab.sputnik906.tpp.spring.app.batch;

import java.util.Map;
import java.util.Properties;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.converter.DefaultJobParametersConverter;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JobLaunchingController {

  @Autowired
  private JobLauncher jobLauncher;

  @Autowired
  private JobExplorer jobExplorer;

  @Autowired
  private ApplicationContext context;

  @GetMapping(path = "/api/run-job/{jobName}")
  public ExitStatus runJob(
    @PathVariable(value = "jobName") String jobName,
    @RequestParam Map<String,String> allRequestParams
  ) throws Exception {
    Properties properties = new Properties();
    allRequestParams.forEach(properties::put);

    JobParametersBuilder jobParametersBuilder = new JobParametersBuilder(
      new DefaultJobParametersConverter().getJobParameters(properties),
      jobExplorer
    );

    Job job = this.context.getBean(jobName, Job.class);

    jobParametersBuilder = jobParametersBuilder.getNextJobParameters(job);

    return this.jobLauncher.run(job, jobParametersBuilder.toJobParameters()).getExitStatus();
  }
}
