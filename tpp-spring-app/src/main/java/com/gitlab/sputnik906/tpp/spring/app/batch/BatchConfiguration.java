package com.gitlab.sputnik906.tpp.spring.app.batch;

import com.gitlab.sputnik906.tpp.domain.entity.Project;
import com.gitlab.sputnik906.tpp.domain.entity.planning.ProjectTask;
import java.util.Collections;
import javax.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.database.builder.JpaPagingItemReaderBuilder;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

  private final static String MAX_HANDLED_ID_PARAM_NAME = "maxHandledId";

  @Autowired
  private JobBuilderFactory jobBuilderFactory;

  @Autowired
  private StepBuilderFactory stepBuilderFactory;

  @Autowired
  private EntityManagerFactory entityManagerFactory;

  @Autowired
  private JobExplorer jobExplorer;

  @Bean
  public Step createProjectTaskStep() {
    PersistMaxHandledId persistMaxHandledId = new PersistMaxHandledId();
    return this.stepBuilderFactory.get("createProjectTaskStep")
      .<Project, ProjectTask>chunk(10)
      .reader(projectItemReader(null,null))
      .writer(projectTaskItemWriter())
      .processor(projectItemProcessor())
      .readerIsTransactionalQueue()
      .listener((ItemProcessListener<Project,ProjectTask>)persistMaxHandledId)
      .listener((StepExecutionListener)persistMaxHandledId)
      .build();
  }

  @Bean
  @StepScope
  public JpaPagingItemReader<Project> projectItemReader(
    @Value("#{jobExecutionContext['"+MAX_HANDLED_ID_PARAM_NAME+"']}") Long maxHandledId,
    @Value("#{jobParameters['maxItemCount']}") Integer maxItemCount
    ){
    if (maxItemCount==null) maxItemCount=5;
    return new JpaPagingItemReaderBuilder<Project>()
      .name("projectItemReader")
      .entityManagerFactory(entityManagerFactory)
      .queryString("select c from Project c where c.id>:maxHandledId")
      .parameterValues(Collections.singletonMap("maxHandledId",maxHandledId))
      .maxItemCount(maxItemCount)
      .build();
  }

  @Bean
  @StepScope
  public ItemProcessor<Project,ProjectTask> projectItemProcessor(){
    return ProjectTask::new;
  }

  @Bean
  @StepScope
  public JpaItemWriter<ProjectTask> projectTaskItemWriter(){
    JpaItemWriter<ProjectTask> jpaItemWriter = new JpaItemWriter<>();
    jpaItemWriter.setEntityManagerFactory(entityManagerFactory);
    return jpaItemWriter;
  }

  @Bean
  public Tasklet retrieveLastMaxHandledIdTasklet() {
    return new RetrieveLastMaxHandledId(this.jobExplorer);
  }

  @Bean
  public Step retrieveLastMaxHandledIdStep() {
    return this.stepBuilderFactory.get("retrieveLastMaxHandledIdStep")
      .tasklet(retrieveLastMaxHandledIdTasklet())
      .build();
  }


  @Bean
  public Job createProjectTask() {
    return this.jobBuilderFactory.get("createProjectTask")
      .start(retrieveLastMaxHandledIdStep())
      .next(createProjectTaskStep())
      .incrementer(new RunIdIncrementer())
      .build();
  }



  @RequiredArgsConstructor
  private static class RetrieveLastMaxHandledId implements Tasklet {

    private final JobExplorer jobExplorer;

    @Override
    public RepeatStatus execute(
      StepContribution contribution,
      ChunkContext chunkContext) throws Exception {

      String jobName = chunkContext.getStepContext().getJobName();

      JobInstance lastJobInstance = jobExplorer.getLastJobInstance(jobName);

      JobExecution lastJobExecution = jobExplorer.getLastJobExecution(lastJobInstance);//current execution

      long lastJobExecutionId =  lastJobExecution.getId()-1;

      long maxHandledId = -1L;

      if (lastJobExecutionId>0){ //id start from 1
        JobExecution lastFinishedJobExecution = jobExplorer.getJobExecution(lastJobExecutionId);
        maxHandledId = lastFinishedJobExecution.getExecutionContext().getLong(MAX_HANDLED_ID_PARAM_NAME);
      }


      chunkContext.getStepContext()
        .getStepExecution()
        .getJobExecution()
        .getExecutionContext()
        .put(MAX_HANDLED_ID_PARAM_NAME,maxHandledId);



      return  RepeatStatus.FINISHED;
    }
  }

  private static class PersistMaxHandledId implements ItemProcessListener<Project,ProjectTask>,
    StepExecutionListener {

    private Long maxHandledId = -1L;

    @Override
    public void beforeProcess(Project item) {

    }

    @Override
    public void afterProcess(Project item, ProjectTask result) {
      maxHandledId = Math.max(item.getId(),maxHandledId);
    }

    @Override
    public void onProcessError(Project item, Exception e) {

    }

    @Override
    public void beforeStep(StepExecution stepExecution) {

    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
      stepExecution.getJobExecution().getExecutionContext().putLong(MAX_HANDLED_ID_PARAM_NAME,maxHandledId);
      return stepExecution.getExitStatus();
    }
  }

}
