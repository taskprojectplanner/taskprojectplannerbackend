package com.gitlab.sputnik906.tpp.spring.app.aggregation;

import com.gitlab.sputnik906.entity.event.api.TransactionEvents;
import com.gitlab.sputnik906.entity.event.api.type.EntityDeleteEvent;
import com.gitlab.sputnik906.entity.event.api.type.EntityPersistEvent;
import com.gitlab.sputnik906.entity.event.api.type.EntityUpdateEvent;
import com.gitlab.sputnik906.entity.persist.BeanUtils;
import com.gitlab.sputnik906.entity.persist.PredictUtils;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.tpp.spring.app.aggregation.handler.AvgAggregationEntityHandler;
import com.gitlab.sputnik906.tpp.spring.app.aggregation.handler.CountAggregationEntityHandler;
import com.gitlab.sputnik906.tpp.spring.app.aggregation.handler.MaxAggregationEntityHandler;
import com.gitlab.sputnik906.tpp.spring.app.aggregation.handler.MinAggregationEntityHandler;
import com.gitlab.sputnik906.tpp.spring.app.aggregation.handler.SumAggregationEntityHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import javax.annotation.PostConstruct;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class EntityAggregationRegistrator {

  private final Map<String,AggregationSubscription> subscriptionsSync =
    Collections.synchronizedMap(new HashMap<>());

  private final Map<String,IAggregationEntityHandler> handlersSync =
    Collections.synchronizedMap(new HashMap<>());

  @Autowired
  private EntityRepository repository;

  @PostConstruct
  protected void init(){
    registerDefaultHandlers();
  }

  public void register(String entityName,String aggregateFunName,
    String field,String where,
    Consumer<ChangeAggregationEvent> consumer,
    boolean invokeImmediately){

    String key = generateKey(entityName,aggregateFunName,field,where);

    subscriptionsSync.compute(key,(k,v)->{
      AggregationSubscription sub = v!=null
        ?v
        :new AggregationSubscription(entityName,aggregateFunName,field,where);
      if (v==null){
        Optional.ofNullable(
          handlersSync.get(sub.getAggregateFunName())
        ).ifPresent(handler->sub.setAggregateObject(
          handler.initValue(repository,entityName,aggregateFunName,field,where)
        ));
      }

      sub.getConsumers().add(consumer);

      //TODO как можно получить transactionId, author и корректный timestamp когда было сделано изменнеие
      Map<String, Object> metadata = of("timestamp",System.currentTimeMillis());

      if (invokeImmediately&&sub.getAggregateObject()!=null)  consumer.accept(
        new ChangeAggregationEvent(sub,new HashMap<>())
      );

      return sub;
    });
  }

  public boolean unRegister(Consumer<ChangeAggregationEvent> consumer){
    return subscriptionsSync.entrySet().removeIf(entry->{
      entry.getValue().getConsumers().remove(consumer);
      return entry.getValue().getConsumers().size()==0;
    });
  }

  public Object getAggregateValue(String entityName,String aggregateFunName,
    String field,String where){

    AggregationSubscription sub = subscriptionsSync
      .get(generateKey(entityName,aggregateFunName,field,where));

    return sub!=null?sub.getCurrentValue():null;

  }

  public IAggregationEntityHandler registerAggregateHandler(String aggregateFunName, IAggregationEntityHandler handler){
    return handlersSync.put(aggregateFunName,handler);
  }

  public void registerDefaultHandlers(){
    IAggregationEntityHandler handler = new CountAggregationEntityHandler();
    registerAggregateHandler(handler.aggregateFunName(),handler);

    handler = new AvgAggregationEntityHandler();
    registerAggregateHandler(handler.aggregateFunName(),handler);

    handler = new MaxAggregationEntityHandler();
    registerAggregateHandler(handler.aggregateFunName(),handler);

    handler = new MinAggregationEntityHandler();
    registerAggregateHandler(handler.aggregateFunName(),handler);

    handler = new SumAggregationEntityHandler();
    registerAggregateHandler(handler.aggregateFunName(),handler);
  }

  public IAggregationEntityHandler unRegisterAggregateHandler(String aggregateFunName){
    return handlersSync.remove(aggregateFunName);
  }

  @TransactionalEventListener
  protected void handler(TransactionEvents transactionEvents) {
    CompletableFuture.runAsync(
        () -> {
          transactionEvents
              .getEvents()
              .forEach(
                  e -> {
                    Map<String, Object> metadata =
                      of(
                        "transactionId", e.getMetadata().getTransactionId(),
                        "timestamp", e.getMetadata().getTimestamp(),
                        "author", e.getMetadata().getAuthor());

                    String[] entityTypeSplit = e.getEntityType().split("\\.");
                    String entityName = entityTypeSplit[entityTypeSplit.length - 1];

                    if (e instanceof EntityPersistEvent) {
                      EntityPersistEvent<?> c = (EntityPersistEvent<?>) e;

                      subscriptionsSync.values().stream()
                        .filter(sub->sub.getEntityName().equals(entityName))
                        .filter(sub->sub.getField()==null||c.changedProperties().contains(sub.getField()))
                        .filter(sub-> PredictUtils.predict(c.getStates(),sub.getWhere()))
                        .forEach(sub->{
                          handler(sub,c.getStates(),true,metadata);
                        });

                      return;

                    }

                    if (e instanceof EntityDeleteEvent) {
                      EntityDeleteEvent<?> d = (EntityDeleteEvent<?>) e;

                      subscriptionsSync.values().stream()
                        .filter(sub->sub.getEntityName().equals(entityName))
                        .filter(sub->sub.getField()==null||d.changedProperties().contains(sub.getField()))
                        .filter(sub-> PredictUtils.predict(d.getStates(),sub.getWhere()))
                        .forEach(sub->{
                          handler(sub,d.getStates(),false,metadata);

                        });

                      return;
                    }

                    if (e instanceof EntityUpdateEvent) {
                      EntityUpdateEvent<?> u = (EntityUpdateEvent<?>) e;

                      subscriptionsSync.values().stream()
                        .filter(sub->sub.getEntityName().equals(entityName))
                        .filter(sub->sub.getField()==null||u.changedProperties().contains(sub.getField()))
                        .forEach(sub->{

                          boolean oldStatePredict = PredictUtils.predict(u.getOldStates(),sub.getWhere());
                          boolean newStatePredict = PredictUtils.predict(u.getNewStates(),sub.getWhere());

                          if (!oldStatePredict&&newStatePredict) handler(sub,u.getNewStates(),true,metadata);

                          if (oldStatePredict&&!newStatePredict) handler(sub,u.getOldStates(),false,metadata);

                         });

                    }

                  });
        });
  }

  private void handler(AggregationSubscription sub,Map<String,Object> states,boolean isPlus,Map<String, Object> metadata){
    IAggregationEntityHandler handler = handlersSync.get(sub.getAggregateFunName());
    if (handler==null) return;
    Object newValue = null;
    if (sub.getAggregateObject()!=null){
      newValue = isPlus
        ?handler.handlePlusEntity(
          sub.getField(),
          states.get(sub.getField()),
          sub.getAggregateObject()
        )
        :handler.handleMinusEntity(
          sub.getField(),
          states.get(sub.getField()),
          sub.getAggregateObject()
        );
    }
    if (newValue==null){
      newValue = handler.initValue(
        repository,
        sub.getEntityName(),
        sub.getAggregateFunName(),
        sub.getField(),
        sub.getWhere()
      );
    }
    if(newValue!=null&&!newValue.equals(sub.getAggregateObject())){
      sub.setAggregateObject(newValue);
      sub.getConsumers().forEach(consumer->consumer.accept(new ChangeAggregationEvent(sub,metadata)));
    }
  }

  @Value
  public static class ChangeAggregationEvent{
    private AggregationSubscription sub;
    private Map<String, Object> metadata;
  }

  @RequiredArgsConstructor
  @Getter
  public static class AggregationSubscription{
    @NonNull
    private final String entityName;
    @NonNull
    private final String aggregateFunName;
    private final String field;
    private final String where;

    @Setter(AccessLevel.PRIVATE)
    @Getter(AccessLevel.PRIVATE)
    private volatile Object aggregateObject;

    public Object getCurrentValue(){
      return aggregateObject instanceof IComplexValue
        ?((IComplexValue)aggregateObject).getValue()
        :aggregateObject;
    }

    @Getter(AccessLevel.PRIVATE)
    private final Set<Consumer<ChangeAggregationEvent>> consumers = Collections.synchronizedSet(new HashSet<>());

  }

  private static String generateKey(String entityName,String aggregateFunName,
    String field,String where){
    Objects.requireNonNull(entityName);
    Objects.requireNonNull(aggregateFunName);
    return entityName+" "+aggregateFunName+"("+(field==null?"":field)+") where "+(where==null?"":where);
  }

  private static Map<String, Object> of(Object... objs) {
    Map<String, Object> map = new HashMap<>();
    for (int i = 0; i < objs.length; i += 2) map.put((String) objs[i], objs[i + 1]);
    return map;
  }



}
