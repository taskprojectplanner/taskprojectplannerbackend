package com.gitlab.sputnik906.tpp.spring.app.aggregation.handler;

import com.gitlab.sputnik906.tpp.spring.app.aggregation.IAggregationEntityHandler;

public class SumAggregationEntityHandler implements IAggregationEntityHandler {
  @Override
  public Object handlePlusEntity(String fieldName, Object fieldValue,
    Object oldAggregationValue) {
    if (fieldValue instanceof Double) return (Double) fieldValue+(Double) oldAggregationValue;
    if (fieldValue instanceof Long) return(Long) fieldValue+(Long)oldAggregationValue;
    if (fieldValue instanceof Integer) return (Integer) fieldValue+(Integer)oldAggregationValue;
    if (fieldValue instanceof Float) return (Float) fieldValue+(Float)oldAggregationValue;
    throw new IllegalArgumentException();
  }

  @Override
  public Object handleMinusEntity(String fieldName, Object fieldValue,
    Object oldAggregationValue) {
    if (fieldValue instanceof Double) return (Double) oldAggregationValue-(Double) fieldValue;
    if (fieldValue instanceof Long) return(Long) oldAggregationValue-(Long)fieldValue;
    if (fieldValue instanceof Integer) return (Integer) oldAggregationValue-(Integer)fieldValue;
    if (fieldValue instanceof Float) return (Float) oldAggregationValue-(Float)fieldValue;
    throw new IllegalArgumentException();
  }

  @Override
  public String aggregateFunName() {
    return "sum";
  }
}
