package com.gitlab.sputnik906.tpp.spring.app.aggregation;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;

public interface IAggregationEntityHandler {
  Object handlePlusEntity(String fieldName,Object fieldValue, Object oldAggregationValue);
  Object handleMinusEntity(String fieldName,Object fieldValue,Object oldAggregationValue);

  default Object initValue(EntityRepository repository,String entityName,String aggregateFunName,
    String fieldName,String where){
    return repository.aggregateFun(
      aggregateFunName,
      fieldName,
      where,
      repository.getEntityClassByName(entityName)
    );

  }

  String aggregateFunName();
}
