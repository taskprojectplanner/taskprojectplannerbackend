package com.gitlab.sputnik906.tpp.spring.app.aggregation.handler;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.tpp.spring.app.aggregation.IAggregationEntityHandler;
import com.gitlab.sputnik906.tpp.spring.app.aggregation.IComplexValue;
import lombok.EqualsAndHashCode;
import lombok.Value;

public class AvgAggregationEntityHandler implements IAggregationEntityHandler {

  private final CountAggregationEntityHandler countHandler = new CountAggregationEntityHandler();
  private final SumAggregationEntityHandler sumHandler = new SumAggregationEntityHandler();


  @Override
  public Object handlePlusEntity(String fieldName, Object fieldValue,
    Object oldAggregationValue) {
    return new CountAndSum(
      (Long)countHandler.handlePlusEntity(fieldName,fieldValue,((CountAndSum)oldAggregationValue).getCount()),
      sumHandler.handlePlusEntity(fieldName,fieldValue,((CountAndSum)oldAggregationValue).getSum())
    );
  }

  @Override
  public Object handleMinusEntity(String fieldName, Object fieldValue,
    Object oldAggregationValue) {
    return new CountAndSum(
      (Long)countHandler.handleMinusEntity(fieldName,fieldValue,((CountAndSum)oldAggregationValue).getCount()),
      sumHandler.handleMinusEntity(fieldName,fieldValue,((CountAndSum)oldAggregationValue).getSum())
    );
  }

  @Override
  public Object initValue(EntityRepository repository,
    String entityName, String aggregateFunName, String fieldName, String where) {
    return new CountAndSum(
      (Long)countHandler.initValue(repository,entityName,aggregateFunName,fieldName,where),
      sumHandler.initValue(repository,entityName,aggregateFunName,fieldName,where)
    );
  }

  @Override
  public String aggregateFunName() {
    return "avg";
  }

  @Value
  @EqualsAndHashCode
  private static class CountAndSum implements IComplexValue {
    private Long count;
    private Object sum;

    public Object getValue(){
      if (count==0) return 0;
      return (Double)sum/count;
    }
  }
}
