package component;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.tpp.domain.TppDataSets;
import com.gitlab.sputnik906.tpp.domain.entity.Employee;
import com.gitlab.sputnik906.tpp.domain.entity.Skill;
import com.gitlab.sputnik906.tpp.domain.entity.Task.Fields;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;

public class RestDocEmployeeTests extends AbstractRestDocTests {

  @Autowired private EntityRepository repository;

  @Test
  public void create() throws Exception {
    this.mockMvc
        .perform(
            post(getPathByDomain(Employee.class))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(TppDataSets.exampleEmployee())))
        .andExpect(status().isOk());
  }

  @Test
  public void createArray() throws Exception {
    this.mockMvc
        .perform(
            post(getPathByDomain(Employee.class) + "/array")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(Arrays.asList(
                  TppDataSets.exampleEmployee(),
                  TppDataSets.exampleEmployee()
                ))))
        .andExpect(status().isOk());
  }

  @Test
  public void findAll() throws Exception {
    this.mockMvc
        .perform(get(getPathByDomain(Employee.class)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void findById() throws Exception {

    Employee createdEmployee =
        repository.persist(TppDataSets.exampleEmployee());

    this.mockMvc
        .perform(
            get(getPathByDomain(Employee.class) + "/{id}", createdEmployee.getId())
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void patch() throws Exception {

    Employee createdEmployee =
      repository.persist(TppDataSets.exampleEmployee());

    this.mockMvc
        .perform(
            RestDocumentationRequestBuilders.patch(getPathByDomain(Employee.class))
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsString(
                        of("id", createdEmployee.getId(), Fields.label, "Patch Employee 1"))))
        .andExpect(status().isOk());
  }

  @Test
  public void delete() throws Exception {

    Employee createdEmployee =
      repository.persist(TppDataSets.exampleEmployee());

    this.mockMvc
        .perform(
            RestDocumentationRequestBuilders.delete(
              getPathByDomain(Employee.class) + "/{id}", createdEmployee.getId())
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void addSkills() throws Exception {

    Employee createdEmployee =
      repository.persist(TppDataSets.exampleEmployee());

    Skill canProgrammingJava =
      repository.persist(TppDataSets.canProgrammingJava());

    Skill canTestingJava =
      repository.persist(TppDataSets.canTestingJava());

    this.mockMvc
        .perform(
            post(
              getPathByDomain(Employee.class) + "/{employeeId}/" + Employee.Fields.skills,
                    createdEmployee.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsString(
                        Arrays.asList(
                          canProgrammingJava.getId(),
                          canTestingJava.getId()
                        ))))
        .andExpect(status().isOk());
  }

  @Test
  public void removeSkills() throws Exception {

    Skill canProgrammingJava =
      repository.persist(TppDataSets.canProgrammingJava());

    Skill canTestingJava =
      repository.persist(TppDataSets.canTestingJava());

    Employee createdEmployee =
      repository.persist(TppDataSets.exampleEmployee(
        canProgrammingJava,
        canTestingJava
      ));

    this.mockMvc
        .perform(
            RestDocumentationRequestBuilders.delete(
              getPathByDomain(Employee.class) + "/{employeeId}/" + Employee.Fields.skills,
                    createdEmployee.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsString(Collections.singletonList(canTestingJava))))
        .andExpect(status().isOk());
  }
}
