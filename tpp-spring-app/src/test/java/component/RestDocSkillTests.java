package component;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.gitlab.sputnik906.tpp.domain.TppDataSets;
import com.gitlab.sputnik906.tpp.domain.entity.Skill;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;

public class RestDocSkillTests extends AbstractRestDocTests {

  @Test
  public void create() throws Exception {

    this.mockMvc
        .perform(
            RestDocumentationRequestBuilders.post(getPathByDomain(Skill.class))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(TppDataSets.exampleSkill())))
        .andExpect(status().isOk());
  }

  @Test
  public void findAll() throws Exception {
    this.mockMvc
        .perform(
            RestDocumentationRequestBuilders.get(getPathByDomain(Skill.class))
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }
}
