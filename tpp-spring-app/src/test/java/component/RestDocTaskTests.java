package component;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.tpp.domain.PlanService.PlanTaskCommand;
import com.gitlab.sputnik906.tpp.domain.TppDataSets;
import com.gitlab.sputnik906.tpp.domain.entity.Employee;
import com.gitlab.sputnik906.tpp.domain.entity.Project;
import com.gitlab.sputnik906.tpp.domain.entity.Task;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

public class RestDocTaskTests extends AbstractRestDocTests {

  @Autowired private EntityRepository repository;

  @Test
  public void findAll() throws Exception {
    createProjectWithOneTaskAndGetIt();//Для генерации примера ответа

    this.mockMvc
        .perform(get(getPathByDomain(Task.class)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

//  @Test
//  public void findById() throws Exception {
//
//    Task createdTask = createProjectWithOneTaskAndGetIt();
//
//    this.mockMvc
//        .perform(
//            get(getPathByDomain(Task.class) + "/{id}", createdTask.getId())
//                .accept(MediaType.APPLICATION_JSON))
//        .andExpect(status().isOk());
//  }
//
//  @Test
//  public void patch() throws Exception {
//
//    Task createdTask = createProjectWithOneTaskAndGetIt();
//
//    this.mockMvc
//        .perform(
//            RestDocumentationRequestBuilders.patch(getPathByDomain(Task.class))
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(
//                    objectMapper.writeValueAsString(
//                        of("id", createdTask.getId(), Fields.label, "Patch Задача 1"))))
//        .andExpect(status().isOk());
//  }
//
//  @Test
//  public void delete() throws Exception {
//
//    Task createdTask = createProjectWithOneTaskAndGetIt();
//
//    this.mockMvc
//        .perform(
//            RestDocumentationRequestBuilders.delete(
//                    getPathByDomain(Task.class) + "/{id}", createdTask.getId())
//                .accept(MediaType.APPLICATION_JSON))
//        .andExpect(status().isOk());
//  }

  @Test
  public void plan() throws Exception {

    Task createdTask = createProjectWithOneTaskAndGetIt();

    Employee createdEmployee =
      repository.persist(TppDataSets.exampleEmployee());

    PlanTaskCommand planTaskCommand =
        new PlanTaskCommand(
            createdTask.getId(), createdEmployee.getId(), Instant.now().getEpochSecond());

    this.mockMvc
        .perform(
            post(getPathByDomain(Task.class) + "/method/plan")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsString(Collections.singletonList(planTaskCommand))))
        .andExpect(status().isOk());
  }

  private Task createProjectWithOneTaskAndGetIt() {

    List<Task> tasks = rangeGenerator(1,2,
      i->new Task("Task "+i,new HashSet<>(), Duration.ofHours(1).toMillis()));

    Project project = new Project("Project",
      TppDataSets.BASE_TIME.toEpochMilli(),
      TppDataSets.BASE_TIME.plus(1, ChronoUnit.DAYS).toEpochMilli(),
      1.0
      );

    project.addTasks(tasks);

    Project createdProject = repository.persist(project);

    return createdProject.getTasks().stream().findFirst().get();
  }
}
