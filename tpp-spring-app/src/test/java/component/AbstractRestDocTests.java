package component;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.sputnik906.tpp.spring.app.SpringApplicationBase;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.MOCK,
    classes = SpringApplicationBase.class)
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
public abstract class AbstractRestDocTests {
  protected MockMvc mockMvc;

  protected ObjectMapper objectMapper = new ObjectMapper();

  public static String getPath(Class<?> controller) {
    return controller.getAnnotationsByType(RequestMapping.class)[0].value()[0];
  }

  public static String getPathByDomain(Class<?> domainClass){
    return "/api/"+domainClass.getSimpleName().toLowerCase()+"s";
  }

  public static Map<String, Object> of(Object... objs) {
    Map<String, Object> map = new HashMap<>();
    for (int i = 0; i < objs.length; i += 2) map.put((String) objs[i], objs[i + 1]);
    return map;
  }

  public static <D> List<D> rangeGenerator(int startNumber, int count,
    IntFunction<? extends D> mapFun){
    return IntStream.range(startNumber, startNumber + count)
      .mapToObj(mapFun)
      .collect(Collectors.toList());
  }

  @BeforeEach
  public void setUp(
      WebApplicationContext webApplicationContext,
      RestDocumentationContextProvider restDocumentation) {
    this.mockMvc =
        MockMvcBuilders.webAppContextSetup(webApplicationContext)
            // START Auto rest doc
            .alwaysDo(JacksonResultHandlers.prepareJackson(objectMapper))
            .alwaysDo(
                MockMvcRestDocumentation.document(
                    "{class-name}-{method-name}",
                    Preprocessors.preprocessRequest(),
                    Preprocessors.preprocessResponse(
                        ResponseModifyingPreprocessors.replaceBinaryContent(),
                        ResponseModifyingPreprocessors.limitJsonArrayLength(objectMapper),
                        Preprocessors.prettyPrint())))
            .apply(
                MockMvcRestDocumentation.documentationConfiguration(restDocumentation)
                    .snippets()
                    .withDefaults(
                        CliDocumentation.curlRequest(),
                        HttpDocumentation.httpRequest(),
                        HttpDocumentation.httpResponse(),
                        AutoDocumentation.requestFields(),
                        AutoDocumentation.responseFields(),
                        AutoDocumentation.pathParameters(),
                        AutoDocumentation.requestParameters(),
                        AutoDocumentation.description(),
                        AutoDocumentation.methodAndPath(),
                        AutoDocumentation.section()))
            // END auto rest doc
            // .apply(documentationConfiguration(restDocumentation)) // default way
            .build();
  }
}
