package component;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.tpp.domain.TppDataSets;
import com.gitlab.sputnik906.tpp.domain.entity.Project;
import com.gitlab.sputnik906.tpp.domain.entity.Task;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;

public class RestDocProjectTests extends AbstractRestDocTests {

  @Autowired private EntityRepository repository;


  @Test
  public void create() throws Exception {
    this.mockMvc
        .perform(
            post(getPathByDomain(Project.class))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(TppDataSets.exampleProject())))
        .andExpect(status().isOk());
  }

  @Test
  public void createArray() throws Exception {
    this.mockMvc
        .perform(
            post(getPathByDomain(Project.class) + "/array")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(Arrays.asList(
                  TppDataSets.exampleProject(),
                  TppDataSets.exampleProject())
                )))
        .andExpect(status().isOk());
  }

  @Test
  public void findAll() throws Exception {
    this.mockMvc
        .perform(get(getPathByDomain(Project.class)).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void findById() throws Exception {
    Project createdProject = repository.persist(TppDataSets.exampleProject());

    this.mockMvc
        .perform(
            get(getPathByDomain(Project.class) + "/{id}", createdProject.getId())
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void delete() throws Exception {
    Project createdProject = repository.persist(TppDataSets.exampleProject());

    this.mockMvc
        .perform(
            RestDocumentationRequestBuilders.delete(
              getPathByDomain(Project.class) + "/{id}", createdProject.getId()))
        .andExpect(status().isOk());
  }

  @Test
  public void addTasks() throws Exception {
    Project createdProject = repository.persist(TppDataSets.exampleProject());

    List<Task> tasks = TppDataSets.createTasks(1,2,1);


    this.mockMvc
        .perform(
            post(
              getPathByDomain(Project.class) + "/{projectId}/" + Project.Fields.tasks,
                    createdProject.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsString(tasks)))
        .andExpect(status().isOk());
  }
}
