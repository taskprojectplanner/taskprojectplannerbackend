package manualtest.basic;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.introspect.ObjectIdInfo;
import com.gitlab.sputnik906.entity.spring.controller.UrlEntity;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import java.io.Serializable;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import javax.persistence.Entity;
import lombok.RequiredArgsConstructor;
import sun.misc.SharedSecrets;
import sun.reflect.ConstantPool;

@RequiredArgsConstructor
public class RestAssuredEntityApiClient {

  static {
    RestAssured.config = RestAssuredConfig.config().objectMapperConfig(
      new ObjectMapperConfig().jackson2ObjectMapperFactory(
        (type, s) -> {

          ObjectMapper mapper = new ObjectMapper();

          mapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector() {
            @Override
            public ObjectIdInfo findObjectIdInfo(final Annotated ann) {
              if (ann.getRawType().isAnnotationPresent(Entity.class)) {
                return new ObjectIdInfo(
                  PropertyName.construct(idFieldNameOf(ann.getRawType()), null),
                  ann.getRawType(),
                  Number.class.isAssignableFrom(getIdType(ann.getRawType()))
                    ?ObjectIdGenerators.IntSequenceGenerator.class
                    :ObjectIdGenerators.StringIdGenerator.class,
                  null);
              }
              return super.findObjectIdInfo(ann);
            }

          });

          return mapper;
        }
      ));

  }

  private final String baseUrl;

  public <T> T create(Class<T> domain,Map<String,Object> object){
    return RestAssured.given()
      .contentType(ContentType.JSON)
      .body(object)
      .when()
      .post(getPathByDomain(domain))
      .then()
      .body(idFieldNameOf(domain), notNullValue())
      .body(versionFieldNameOf(domain), equalTo(0))
      .statusCode(200)
      .log()
      .all()
      .extract()
      .as(domain);
  }

  public <T> T create(T object){
    Class<T> domain = getClass(object);
    return RestAssured.given()
      .contentType(ContentType.JSON)
      .body(object)
      .when()
      .post(getPathByDomain(domain))
      .then()
      .body(idFieldNameOf(domain), notNullValue())
      .body(versionFieldNameOf(domain), equalTo(0))
      .statusCode(200)
      .log()
      .all()
      .extract()
      .as(domain);
  }

  public <T> List<T> createBatch(Class<T> domain, Collection<T> batch){
    return RestAssured.given()
      .contentType(ContentType.JSON)
      .body(batch)
      .when()
      .post(getPathByDomain(domain)+"/batch")
      .then()
      .statusCode(200)
      .log()
      .all()
      .extract()
      .body()
      .jsonPath()
      .getList(".", domain);
  }

  public <T> T findById(Class<T> domain, Serializable id){
    return RestAssured.given()
      .contentType(ContentType.JSON)
      .when()
      .get(getPathByDomain(domain)+"/"+id)
      .then()
      .statusCode(200)
      .log()
      .all()
      .extract()
      .as(domain);
  }

  public <T> List<T> findAllById(Class<T> domain, Serializable... ids){
    return RestAssured.given()
      .contentType(ContentType.JSON)
      .param("ids",ids)
      .when()
      .get(getPathByDomain(domain))
      .then()
      .statusCode(200)
      .log()
      .all()
      .extract()
      .body()
      .jsonPath()
      .getList(".", domain);
  }

  public <T> T patch(Class<T> domain,Serializable id, Long version, Map<String, Object> patch){
    patch.put(idFieldNameOf(domain),id);
    patch.put(versionFieldNameOf(domain),version);
    return RestAssured.given()
      .contentType(ContentType.JSON)
      .body(patch)
      .when()
      .patch(getPathByDomain(domain))
      .then()
      .statusCode(200)
      .log()
      .all()
      .extract()
      .as(domain);
  }

  public void delete(Class<?> domain, Serializable id){
    RestAssured.given()
      .when()
      .delete(getPathByDomain(domain)+"/"+id)
      .then()
      .statusCode(200)
      .log()
      .all();
  }

  public boolean addToCollection(Class<?> domain,Serializable id, String propName, Object ... objects){
    return RestAssured.given()
      .when()
      .body(objects)
      .post(getPathByDomain(domain)+"/"+id+"/"+propName+"/add")
      .then()
      .statusCode(200)
      .log()
      .all()
      .extract()
      .as(Boolean.class);
  }

  public boolean removeFromCollection(Class<?> domain,Serializable id, String propName, Serializable ... ids){
    return RestAssured.given()
      .when()
      .body(ids)
      .delete(getPathByDomain(domain)+"/"+id+"/"+propName+"/remove")
      .then()
      .statusCode(200)
      .log()
      .all()
      .extract()
      .as(Boolean.class);
  }

  public boolean clearCollection(Class<?> domain,Serializable id, String propName){
    return RestAssured.given()
      .when()
      .delete(getPathByDomain(domain)+"/"+id+"/"+propName+"/clear")
      .then()
      .statusCode(200)
      .log()
      .all()
      .extract()
      .as(Boolean.class);
  }

  public <R> R invokeMethod(Class<?> domain,Serializable id,String methodName,Class<R> resultType,
    Object ... params){

    ValidatableResponse response = RestAssured.given()
      .contentType(ContentType.JSON)
      .body(params)
      .when()
      .post(getPathByDomain(domain)+(id!=null?"/"+id:"")+"/m/"+methodName)
      .then()
      .statusCode(200)
      .log()
      .all();

    return response.extract()
        .as(resultType);
  }

  public <R> List<R> invokeMethodList(Class<?> domain,Serializable id,String methodName,Class<R> genericType,
    Object ... params){

    return RestAssured.given()
      .contentType(ContentType.JSON)
      .body(params)
      .when()
      .post(getPathByDomain(domain)+(id!=null?"/"+id:"")+"/m/"+methodName)
      .then()
      .statusCode(200)
      .log()
      .all()
      .extract()
      .jsonPath()
      .getList(".",genericType);
  }

  //static 1 argument
  public <A,R> R invokeMethod(Serializable id, Function<A,R> fun, A a){
    Method method = unreference(fun);
    return invokeMethod(method.getDeclaringClass(),id,method.getName(),(Class<R>)method.getReturnType(),a);
  }

  //instance 0 argument
  public <T,R> R invokeMethod(Serializable id, Function<T,R> fun){
    Method method = unreference(fun);
    return invokeMethod(method.getDeclaringClass(),id,method.getName(),(Class<R>)method.getReturnType());
  }

  //instance 1 argument
  public <T,A,R> R invokeMethod(Serializable id, BiFunction<T,A,R> fun, A a){
    Method method = unreference(fun);
    return invokeMethod(method.getDeclaringClass(),id,method.getName(),(Class<R>)method.getReturnType(),a);
  }

  //static 2 argument
  public <A,B,R> R invokeMethod(Serializable id, BiFunction<A,B,R> fun, A a, B b){
    Method method = unreference(fun);
    return invokeMethod(method.getDeclaringClass(),id,method.getName(),(Class<R>)method.getReturnType(),a,b);
  }

  //instance 2 argument
  public <T,A,B,R> R invokeMethod(Serializable id, ThirdFunction<T,A,B,R> fun, A a, B b){
    Method method = unreference(fun);
    return invokeMethod(method.getDeclaringClass(),id,method.getName(),(Class<R>)method.getReturnType(),a,b);
  }

  //static 3 argument
  public <A,B,C,R> R invokeMethod(Serializable id, ThirdFunction<A,B,C,R> fun, A a, B b, C c){
    Method method = unreference(fun);
    return invokeMethod(method.getDeclaringClass(),id,method.getName(),(Class<R>)method.getReturnType(),a,b,c);
  }

  //instance 3 argument
  public <T,A,B,C,R> R invokeMethod(Serializable id, FourFunction<T,A,B,C,R> fun, A a, B b, C c){
    Method method = unreference(fun);
    return invokeMethod(method.getDeclaringClass(),id,method.getName(),(Class<R>)method.getReturnType(),a,b,c);
  }

  @FunctionalInterface
  public interface ThirdFunction<A, B, C, R> {
    R apply(A a, B b, C c);
  }

  @FunctionalInterface
  public interface FourFunction<A, B, C, D, R> {
    R apply(A a, B b, C c, D d);
  }


  //https://habr.com/ru/post/311788/
  private static Method unreference(Object ref) {
    ConstantPool pool = SharedSecrets.getJavaLangAccess().getConstantPool(ref.getClass());
    int size = pool.getSize();
    for (int i = 1; i < size; i++) {
      try {
        Member member = pool.getMethodAt(i);
        if (member instanceof Method) {
          return (Method) member;
        }
      } catch (IllegalArgumentException e) {
        // skip non-method entry
      }
    }
    throw new IllegalArgumentException("Not a method reference");
  }




  private  String getPathByDomain(Class<?> domainClass){
    return baseUrl + UrlEntity.root(domainClass.getSimpleName());
  }

  private static String versionFieldNameOf(Class<?> entityClass){
    return "version";
  }

  private static String idFieldNameOf(Class<?> entityClass){
    return "id";
  }

  private static Class<?> getIdType(Class<?> entityClass){return Long.class;}

  private static <T> Class<T> getClass(T object){
    return (Class<T>) object.getClass();
  }

}
