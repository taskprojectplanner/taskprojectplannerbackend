package manualtest.basic;

import com.gitlab.sputnik906.entity.persist.Maps;
import com.gitlab.sputnik906.tpp.domain.PlanService.PlanTaskCommand;
import com.gitlab.sputnik906.tpp.domain.PlanService.PlanTaskCommandResult;
import com.gitlab.sputnik906.tpp.domain.TppDataSets;
import com.gitlab.sputnik906.tpp.domain.entity.Assignment;
import com.gitlab.sputnik906.tpp.domain.entity.Employee;
import com.gitlab.sputnik906.tpp.domain.entity.Project;
import com.gitlab.sputnik906.tpp.domain.entity.Task;
import java.time.Instant;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled
public class ManualTests {

  private static final RestAssuredEntityApiClient client =
    new RestAssuredEntityApiClient("http://localhost:8080");

  @Test
  public void createNewEmployeeTest() {
    client.create(TppDataSets.exampleEmployee());
  }

  @Test
  public void createNewProjectTest() {

    client.create(
      TppDataSets.exampleProject(
        TppDataSets.exampleProjectTasks()
      )
    );

  }

  @Test
  public void addEmployee() {

    client.create(TppDataSets.exampleEmployee());

  }


  @Test
  public void changeLabelEmployee() {

    client.patch(
      Employee.class,
      1L,
      0L,
      Maps.of(Employee.Fields.label, "Employee 111111111")
    );

  }



  @Test
  public void deleteProject() {
    client.delete(
      Project.class,
      1L
    );
  }

  @Test
  public void addSkillToTask() {

    client.addToCollection(
      Task.class,
      1L,
      Task.Fields.requiredSkills,
      3L
    );


  }

  @Test
  public void removeSkillFromTask() {

    client.removeFromCollection(
      Task.class,
      1L,
      Task.Fields.requiredSkills,
      3L
    );

  }

  @Test
  public void clearSkillFromTask() {

    client.clearCollection(
      Task.class,
      1L,
      Task.Fields.requiredSkills
    );

  }

  @Test
  public void planTask() {

    PlanTaskCommand planTaskCommand = new PlanTaskCommand(
      1L,
      1L,
      Instant.now().toEpochMilli()
    );

    PlanTaskCommandResult[] result = client.invokeMethod(
      null,
      Task::plan,
      new PlanTaskCommand[]{planTaskCommand}
    );

  }

  @Test
  public void overlapsAssingment() {

    List<Assignment> overlaps = client.invokeMethod(
      null,
      Assignment::overlaps,
      1L,
      Instant.now().toEpochMilli(),
      Instant.now().toEpochMilli()+3600*1000
    );

  }

  @Test
  public void unPlanTask() {

    Boolean result = client.invokeMethod(
      1L,
      Task::unPlan
    );

  }

}
