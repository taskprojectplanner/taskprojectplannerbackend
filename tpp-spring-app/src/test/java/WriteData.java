import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator.Feature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gitlab.sputnik906.entity.jackson.JacksonEntitySerializerModule;
import com.gitlab.sputnik906.entity.persist.EntityUtils;
import com.gitlab.sputnik906.tpp.domain.TppDataSets;
import com.gitlab.sputnik906.tpp.domain.entity.Task;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.Test;

public class WriteData {
  @Test
  public void writeDataToYaml() throws IOException {
    File baseDir = new File("build/devDataSet");
    baseDir.mkdirs();

    Map<Class<?>, List<?>> initData = TppDataSets.devDataSet();
    ObjectMapper yamlObjectMapper = createYamlObjectMapper(initData.keySet());
    EntityUtils.generateId(initData);

    for(Map.Entry<Class<?>, List<?>> entry: initData.entrySet()){
      yamlObjectMapper.writerWithDefaultPrettyPrinter()
        .writeValue(new File(baseDir, entry.getKey().getSimpleName()+".yml"),entry.getValue());
    }

  }

  private ObjectMapper createYamlObjectMapper(Set<Class<?>> entityClasses){
    YAMLFactory yamlFactory = new YAMLFactory();
    ObjectMapper objectMapper = new ObjectMapper(yamlFactory);
    //ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JacksonEntitySerializerModule());
    entityClasses.forEach(entityClass->objectMapper.addMixIn(entityClass,EntityMixin.class));
//    objectMapper.addMixIn(Task.class,EntityMixin.class);
    objectMapper.registerModule(new JavaTimeModule());
    objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    return objectMapper;
  }

  @JsonIgnoreProperties({"version"})
  public static class EntityMixin{ }

//  @JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class,
//    property = "id")
//  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "version", "id"}) // id - оно автогенериться
//  public static class EntityMixin{ }
}
