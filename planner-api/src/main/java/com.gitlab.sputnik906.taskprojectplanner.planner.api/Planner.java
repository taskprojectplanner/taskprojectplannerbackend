package com.gitlab.sputnik906.taskprojectplanner.planner.api;

import com.gitlab.sputnik906.taskprojectplanner.planner.api.domain.Order;
import com.gitlab.sputnik906.taskprojectplanner.planner.api.repository.IOrderRepository;
import com.gitlab.sputnik906.taskprojectplanner.planner.api.repository.IPlanVariantRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Planner {

  @NonNull
  private final IOrderRepository orderRepository;

  @NonNull
  private final IPlanVariantRepository planVariantRepository;

  @NonNull
  private final IGeneratePlanVariantStrategy generatePlanVariantStrategy;

  public Order addOrder(Order newOrder){
    Order order = orderRepository.persist(newOrder);

    return order;
  }




}
