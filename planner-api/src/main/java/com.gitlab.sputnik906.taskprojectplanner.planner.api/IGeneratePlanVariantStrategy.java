package com.gitlab.sputnik906.taskprojectplanner.planner.api;

import com.gitlab.sputnik906.taskprojectplanner.planner.api.domain.Order;
import java.util.Map;

public interface IGeneratePlanVariantStrategy {
  GeneratePlanVariantResult nextPlanVariants(Order order,int desiredAmount, Map<String,Object> prevContext);
}
