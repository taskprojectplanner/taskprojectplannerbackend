package com.gitlab.sputnik906.taskprojectplanner.planner.api;

import com.gitlab.sputnik906.taskprojectplanner.planner.api.domain.PlanVariant;
import java.util.List;
import java.util.Map;
import lombok.Value;

@Value
public class GeneratePlanVariantResult {
  List<PlanVariant> result;
  boolean isFinished;
  Map<String,Object> serializedContext;
  boolean isFindTheBetterPlanVariant;
}
