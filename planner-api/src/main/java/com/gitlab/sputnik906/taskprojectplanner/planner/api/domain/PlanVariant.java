package com.gitlab.sputnik906.taskprojectplanner.planner.api.domain;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PlanVariant {
  private Long id;
  private Order order;
  private List<OperationVariant> operationVariants;
  private double objectFunValue;
}
