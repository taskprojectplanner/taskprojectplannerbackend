package com.gitlab.sputnik906.taskprojectplanner.planner.api.domain;

import lombok.Value;

/**
 * Место (интервал времени) на ресурсе в рамках которого целевая функция не изменяется
 */
@Value
public class ResourceTimePlace {
  private Resource resource;

  private Long startTime;

  private Long endTime;
}
