package com.gitlab.sputnik906.taskprojectplanner.planner.api.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Resource {
  private Long id;
}
