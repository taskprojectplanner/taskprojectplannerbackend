package com.gitlab.sputnik906.taskprojectplanner.planner.api.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Order {
  private Long id;

  private Long startTime;

  private Long endTime;
}
