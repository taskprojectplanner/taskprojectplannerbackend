package com.gitlab.sputnik906.taskprojectplanner.planner.api.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class OperationVariant {
  private Long id;
  private Order order;
  private Set<ResourceTimePlace> resourceTimePlaceSet;
  private final Map<String,Double> params = new HashMap<>();
}
