package com.gitlab.sputnik906.taskprojectplanner.planner.api.untyped;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.Accessors;

@RequiredArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class DataSourceQuery {
  private final String type;
  private Boolean distinct;
  private Filters filters;
  private OrderBy orderBy;
  private Integer skip;
  private Integer size;


  public static class Filters{

    private List<Condition> and = new ArrayList<>();
    private List<Filters> or = new ArrayList<>();

    public Filters(Condition condition){
      and(condition);
    }

    public Filters and(Condition condition){
      and.add(condition);
      return this;
    }

    public Filters or(Filters filters){
      or.add(filters);
      return this;
    }

  }

  @Value
  public static class Condition{
    String propPath;
    String operation;
    Object value;
    boolean not;
  }


  @Value
  public static class OrderBy{
    String propPath;
    boolean desc;
  }
}
