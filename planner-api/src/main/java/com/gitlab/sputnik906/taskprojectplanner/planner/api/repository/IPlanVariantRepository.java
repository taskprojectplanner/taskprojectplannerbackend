package com.gitlab.sputnik906.taskprojectplanner.planner.api.repository;

import com.gitlab.sputnik906.taskprojectplanner.planner.api.domain.PlanVariant;

public interface IPlanVariantRepository extends IRepository<PlanVariant,Long> {

}
