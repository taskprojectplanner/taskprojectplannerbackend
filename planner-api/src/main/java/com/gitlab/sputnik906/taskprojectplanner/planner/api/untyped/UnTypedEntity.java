package com.gitlab.sputnik906.taskprojectplanner.planner.api.untyped;

import java.io.Serializable;
import java.util.Map;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UnTypedEntity {
  private Serializable id;
  private String type;
  private Map<String,Object> props;
}
