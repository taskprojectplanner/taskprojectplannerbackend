package com.gitlab.sputnik906.taskprojectplanner.planner.api.untyped;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface IReadOnlyDataSource {

  Optional<UnTypedEntity> findById(Serializable id, String type,String[] fetchPropPaths);
  default Optional<UnTypedEntity> findById(Serializable id, String type){
    return findById(id,type,null);
  }

  List<UnTypedEntity> findAllById(Iterable<Serializable> ids, String type,String[] fetchPropPaths);
  default List<UnTypedEntity> findAllById(Iterable<Serializable> ids, String type){
    return findAllById(ids,type,null);
  }

  List<UnTypedEntity> findAll(DataSourceQuery query,String[] fetchPropPaths);
  default List<UnTypedEntity> findAll(DataSourceQuery query){
    return findAll(query,null);
  }
}
