package com.gitlab.sputnik906.taskprojectplanner.planner.api.repository;

public interface IRepository<Domain,Id> {
  Domain persist(Domain newOrder);
  Domain delete(Id id);
  Domain getById(Id id);
}
