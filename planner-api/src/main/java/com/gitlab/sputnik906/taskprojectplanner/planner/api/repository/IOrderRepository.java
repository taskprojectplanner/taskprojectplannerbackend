package com.gitlab.sputnik906.taskprojectplanner.planner.api.repository;

import com.gitlab.sputnik906.taskprojectplanner.planner.api.domain.Order;

public interface IOrderRepository extends IRepository<Order,Long> {

}
