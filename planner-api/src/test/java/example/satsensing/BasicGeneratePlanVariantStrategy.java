package example.satsensing;

import com.gitlab.sputnik906.taskprojectplanner.planner.api.GeneratePlanVariantResult;
import com.gitlab.sputnik906.taskprojectplanner.planner.api.IGeneratePlanVariantStrategy;
import com.gitlab.sputnik906.taskprojectplanner.planner.api.domain.OperationVariant;
import com.gitlab.sputnik906.taskprojectplanner.planner.api.domain.Order;
import com.gitlab.sputnik906.taskprojectplanner.planner.api.domain.PlanVariant;
import com.gitlab.sputnik906.taskprojectplanner.planner.api.domain.ResourceTimePlace;
import example.satsensing.domain.GsSatVisible;
import example.satsensing.domain.GsSatVisibleRepository;
import example.satsensing.domain.ShootVisible;
import example.satsensing.domain.IBallisticService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BasicGeneratePlanVariantStrategy implements IGeneratePlanVariantStrategy {

  @NonNull
  private final GsSatVisibleRepository gsSatVisibleRepository;

  @NonNull
  private final IBallisticService ballisticService;

  @Override
  public GeneratePlanVariantResult nextPlanVariants(
    Order order, int desiredAmount,
    Map<String, Object> prevContext) {

    List<PlanVariant> planVariants = new ArrayList<>();

    Context ctx = Context.from(prevContext);

    boolean isFinished = false;

    while (planVariants.size() < desiredAmount) {

      int maxCount = desiredAmount - planVariants.size();

      List<GsSatVisible> gsSatVisibles = gsSatVisibleRepository
        .findVisiblesOrderByStartTime(
          //TODO расмотерить ситуацию с одинаковыми значениями startTime (оно не попадет в след. выборку)
          Math.max(order.getStartTime(), ctx.maxStartTimeHandledGsSatVisibles + 1L),
          order.getEndTime(), maxCount);

      if (gsSatVisibles.size()<maxCount) isFinished=true;

      if (gsSatVisibles.size() == 0) {
        break;
      }

      ctx.maxStartTimeHandledGsSatVisibles = gsSatVisibles.stream()
        .mapToLong(GsSatVisible::getStartTime).max().getAsLong();
      ctx.maxIdHandledGsSatVisibles = gsSatVisibles.stream().mapToLong(GsSatVisible::getId).max()
        .getAsLong();
      ctx.countHandledGsSatVisibles += gsSatVisibles.size();

      for (GsSatVisible gsSatVisible : gsSatVisibles) {

        List<ShootVisible> shootVisibles = ballisticService
          .findShootVisiblesOrderByStartTime(order, gsSatVisible.getSatellite(),
            order.getStartTime(),
            gsSatVisible.getStartTime(), Integer.MAX_VALUE);

        if (shootVisibles.size() == 0) {
          continue;
        }

        OperationVariant downloadAndUploadOperationVariant = new OperationVariant(null, order,
          from(new ResourceTimePlace(
            gsSatVisible.getSatellite(), gsSatVisible.getStartTime(), gsSatVisible.getEndTime()
          ), new ResourceTimePlace(
            gsSatVisible.getGroundStation(), gsSatVisible.getStartTime(), gsSatVisible.getEndTime()
          )));

        shootVisibles.forEach(shootVisible -> {

          OperationVariant shootOperationVariant = new OperationVariant(null, order,
            from(new ResourceTimePlace(
              shootVisible.getSatellite(), shootVisible.getStartTime(),
              shootVisible.getEndTime())));

          shootOperationVariant.getParams()
            .put("resolutionMeterPerPixel", shootVisible.getResolutionMeterPerPixel());

          PlanVariant planVariant = new PlanVariant(null, order,
            Arrays.asList(shootOperationVariant, downloadAndUploadOperationVariant),
            getObjectFunValue(order, shootOperationVariant, downloadAndUploadOperationVariant)
          );

          planVariants.add(planVariant);
          ctx.countFindedPlanVariants += 1;

        });
      }

    }

    return new GeneratePlanVariantResult(planVariants, isFinished, ctx.serialize(),
      ctx.countFindedPlanVariants > 0);

  }

  private Set<ResourceTimePlace> from(ResourceTimePlace... resourceTimePlaces) {
    return new HashSet<>(Arrays.asList(resourceTimePlaces));
  }

  @AllArgsConstructor
  private static class Context {

    public long maxStartTimeHandledGsSatVisibles;
    public long maxIdHandledGsSatVisibles;
    public long countHandledGsSatVisibles;
    public long countFindedPlanVariants;

    public Map<String, Object> serialize() {
      Map<String, Object> serializedContext = new HashMap<>();
      serializedContext.put("maxStartTimeHandledGsSatVisibles", maxStartTimeHandledGsSatVisibles);
      serializedContext.put("maxIdHandledGsSatVisibles", maxIdHandledGsSatVisibles);
      serializedContext.put("countHandledGsSatVisibles", countHandledGsSatVisibles);
      serializedContext.put("countFindedPlanVariants", countFindedPlanVariants);
      return serializedContext;
    }

    public static Context from(Map<String, Object> serializedContext) {
      return new Context(
        (long) serializedContext.getOrDefault("maxStartTimeHandledGsSatVisibles", -1L),
        (long) serializedContext.getOrDefault("maxIdHandledGsSatVisibles", -1L),
        (long) serializedContext.getOrDefault("countHandledGsSatVisibles", 0),
        (long) serializedContext.getOrDefault("countFindedPlanVariants", 0)
      );
    }

  }


  private double getObjectFunValue(
    Order order,
    OperationVariant shoot,
    OperationVariant downloadAndUpload) {

    return downloadAndUpload.getResourceTimePlaceSet().stream()
      .findAny().get().getEndTime() - order.getStartTime();
  }
}
