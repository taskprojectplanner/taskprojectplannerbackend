package example.satsensing.domain;

import com.gitlab.sputnik906.taskprojectplanner.planner.api.domain.Order;
import example.satsensing.domain.Satellite;
import example.satsensing.domain.ShootVisible;
import java.util.List;


public interface IBallisticService {

  List<ShootVisible> findShootVisiblesOrderByStartTime(Order order, Satellite satellite, long startTime,
    long endTime, int maxCount);
}
