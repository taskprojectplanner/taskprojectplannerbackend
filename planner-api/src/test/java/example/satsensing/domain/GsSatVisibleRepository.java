package example.satsensing.domain;

import java.util.List;

public interface GsSatVisibleRepository {
  List<GsSatVisible> findVisiblesOrderByStartTime(long startFrom,long endTo,int maxCount);
}
