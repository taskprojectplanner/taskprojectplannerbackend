package example.satsensing.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class GsSatVisible {
  private Long id;
  private GroundStation groundStation;
  private Satellite satellite;
  private long startTime;
  private long endTime;
}
