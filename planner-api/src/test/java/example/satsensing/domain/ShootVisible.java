package example.satsensing.domain;

import lombok.Value;

@Value
public class ShootVisible {
  Satellite satellite;
  long startTime;
  long endTime;
  double resolutionMeterPerPixel;
}
