package com.gitlab.sputnik906.tpp.spring.module;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepositoryFactory;
import com.gitlab.sputnik906.entity.spring.EntityTransactionManagerAdapter;
import com.gitlab.sputnik906.entity.spring.configuration.EnableAutoEntityControllers;
import com.gitlab.sputnik906.entity.spring.configuration.EntityJacksonObjectMapperConfiguration;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.JobLauncher;
import com.gitlab.sputnik906.tpp.domain.PlanService;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.PlatformTransactionManager;


@ComponentScan
@EnableJpaRepositories
@EnableAutoEntityControllers
@EntityScan("com.gitlab.sputnik906.tpp.domain.entity")
@Import({
  EntityJacksonObjectMapperConfiguration.class,
})
public class SpringModule {

  @Autowired private EntityManager em;

  @Autowired private PlatformTransactionManager transactionManager;

  @Autowired private JobLauncher jobLauncher;

  @Bean
  @ConditionalOnMissingBean
  public EntityRepository entityRepository(){
    return EntityRepositoryFactory.withTransactionManager(em,new EntityTransactionManagerAdapter(transactionManager));
  }

  @Bean
  @Lazy(false)
  public PlanService planService(){
    PlanService planService = new PlanService(entityRepository(),jobLauncher);
    PlanService.instance = planService;
    return planService;
  }





}
