package component;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepositoryFactory;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.SimpleJobLauncher;
import com.gitlab.sputnik906.tpp.domain.PlanService;
import com.gitlab.sputnik906.tpp.domain.PlanService.PlanTaskCommand;
import com.gitlab.sputnik906.tpp.domain.PlanService.PlanTaskCommandResult;
import com.gitlab.sputnik906.tpp.domain.entity.Assignment;
import com.gitlab.sputnik906.tpp.domain.entity.Employee;
import com.gitlab.sputnik906.tpp.domain.entity.Task;
import com.gitlab.sputnik906.tpp.domain.TppDataSets;
import com.gitlab.sputnik906.tpp.spring.module.SpringModule;
import java.util.List;
import javax.persistence.EntityExistsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest
@ContextConfiguration(classes = {LoadDataSet.class,SpringModule.class})
@TestInstance(Lifecycle.PER_CLASS)
public class PersistentTests {

  @Autowired
  TestEntityManager em;

  EntityRepository repository;

  PlanService planService;

  @BeforeEach
  public void init(){
    this.repository= EntityRepositoryFactory.withoutTransaction(em.getEntityManager());
    this.planService = new PlanService(repository,new SimpleJobLauncher());
  }

  @Test
  public void planTest() {

    Task task = repository.findAll(Task.class).stream()
      .findFirst().orElseThrow(EntityExistsException::new);

    Employee employee = repository.findAll(Employee.class).stream()
      .findFirst().orElseThrow(EntityExistsException::new);

    PlanTaskCommandResult result = planService.plan(new PlanTaskCommand(
      task.getId(),employee.getId(), TppDataSets.BASE_TIME.toEpochMilli()
    ));

    Assertions.assertTrue(result.isSuccess());
    Assertions.assertEquals(1,repository.findAll(Assignment.class).size());
  }

  @Test
  public void planTest2() {

    List<Task> tasks = repository.findAll(Task.class);

    System.out.println(tasks.size());
  }

}
