package component;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.tpp.domain.TppDataSets;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoadDataSet {

  @Autowired EntityRepository repository;

  @PostConstruct
  public void init() {
    TppDataSets.applyToRepository(
      repository,
      TppDataSets.devDataSet()
    );
  }

}
