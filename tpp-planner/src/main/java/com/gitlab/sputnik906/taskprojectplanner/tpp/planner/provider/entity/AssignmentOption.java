package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity;

import lombok.Value;
import org.joda.time.Interval;

@Value
public class AssignmentOption {
  private final Task task;
  private final Employee employee;
  private final Interval allowedInterval;
}
