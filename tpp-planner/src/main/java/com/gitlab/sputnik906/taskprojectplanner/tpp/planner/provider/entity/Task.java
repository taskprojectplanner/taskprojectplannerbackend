package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity;

import java.util.Set;
import lombok.NonNull;
import lombok.Value;

@Value
public class Task {

  @NonNull
  private final Long id;

  @NonNull
  private final Project project;

  private final Long duration;

  @NonNull
  private final Set<Skill> requiredSkills;
}
