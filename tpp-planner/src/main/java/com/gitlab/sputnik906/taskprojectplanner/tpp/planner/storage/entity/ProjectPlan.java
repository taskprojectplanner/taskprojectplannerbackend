package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity;

import java.util.Collections;
import java.util.Set;
import lombok.Value;

@Value
public class ProjectPlan {
  private final Project project;

  private final Set<Assignment> assignments;

  public Set<Assignment> getAssignments() {
    return Collections.unmodifiableSet(assignments);
  }
}
