package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider;

import com.gitlab.sputnik906.entity.event.api.cdc.IChangeDataCaptureProvider;
import com.gitlab.sputnik906.entity.event.api.cdc.SnapshotEntityResponse;
import com.gitlab.sputnik906.entity.event.api.cdc.SnapshotEntityRequest;
import com.gitlab.sputnik906.entity.persist.BeanUtils;
import com.gitlab.sputnik906.entity.persist.EntityMapper;
import com.gitlab.sputnik906.entity.persist.WhereBuilder;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity.AssignmentOption;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity.AvailabilityEmployee;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity.Project;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity.ProjectPlanOption;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity.Skill;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity.Task;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.ListUtils;
import org.joda.time.Interval;

@RequiredArgsConstructor
public class ProjectPlanOptionProvider implements IOptionProvider<ProjectPlanOption>, Runnable {

  private final List<ProjectPlanOption> knownOptions = new ArrayList<>();

  private final Project project;

  private final IChangeDataCaptureProvider cdc;

  private final Map<Task,SnapshotEntityRequest> currentResourceRequestMap = new HashMap<>();

  @Override
  public ProjectPlanOption get(int number) {
    if (number>=knownOptions.size()) return null;
    return knownOptions.get(number);
  }

  @Override
  public List<ProjectPlanOption> getSubList(int fromIndex, int toIndex) {
    return knownOptions.subList(fromIndex, Math.min(toIndex,knownOptions.size()));
  }

  @Override
  public int count() {
    return knownOptions.size();
  }

  @Override
  public void run() {

    knownOptions.addAll(
      generateProjectPlanOptions(
        new ArrayList<>(project.getTasks()),
        new ArrayList<>()
      )
    );

    Collections.sort(knownOptions);

  }

  protected List<ProjectPlanOption> generateProjectPlanOptions(List<Task> leftTasks,List<AssignmentOption> assignmentOptions){

    if (leftTasks.isEmpty()) {
      return Collections.singletonList(new ProjectPlanOption(
        project,
        new HashSet<>(assignmentOptions),
        evaluation(project,assignmentOptions)
      ));
    }

    Task task = leftTasks.get(0);

    SnapshotEntityRequest snapshotEntityRequest = currentResourceRequestMap.get(task);

    if (snapshotEntityRequest==null) snapshotEntityRequest =
      createInitSnapshotEntityRequest(
        AvailabilityEmployee.class,
        3,
        new WhereBuilder()
          .allMemberOf(
            task.getRequiredSkills().stream()
              .map(Skill::getId)
              .collect(Collectors.toSet()),
            "employee.skills"
          )
          .and()
          .overlap(
            "endTime",
            project.getStartTime(),
            "startTime",
            project.getEndTime()
          )
          .and()
          .expression(" endTime - startTime > "+task.getDuration())
          .toString()
      );


    SnapshotEntityResponse<?> snapshotEntityResponse = cdc.getSnapshotEntity(snapshotEntityRequest);

    List<AvailabilityEmployee> availabilityEmployees = snapshotEntityResponse.getEntities().stream()
      .map(e-> EntityMapper.convert(e,AvailabilityEmployee.class))
      .collect(Collectors.toList());

    List<ProjectPlanOption> projectPlanOptions = new ArrayList<>();

    availabilityEmployees.forEach(availabilityEmployee -> {
      Interval allowedInterval = new Interval(availabilityEmployee.getStartTime(),availabilityEmployee.getEndTime());

      AssignmentOption assignmentOption = new AssignmentOption(task,availabilityEmployee.getEmployee(),allowedInterval);

      List<AssignmentOption> localAssignmentOption = ListUtils.union(assignmentOptions,Collections.singletonList(assignmentOption));

      List<Task> localLeftTasks = new ArrayList<>(leftTasks);
      localLeftTasks.remove(task);

      projectPlanOptions.addAll(generateProjectPlanOptions(localLeftTasks,localAssignmentOption));

    });

    return projectPlanOptions;

  }

  protected SnapshotEntityRequest createInitSnapshotEntityRequest(Class<?> entityClass,int count,String where){
    Set<String> propertyNames = BeanUtils.convertConstructorBeanToFlatMap(entityClass)
      .keySet();
    return SnapshotEntityRequest.builder()
      .entityType(entityClass.getTypeName())
      .count(count)
      .where(where)
      .propertyNames(propertyNames)
      .build();
  }


  protected double evaluation(Project project, Collection<AssignmentOption> assignmentOptions){
    double nonNormalization = assignmentOptions.stream()
      .mapToDouble(a->a.getAllowedInterval().getStartMillis()-project.getStartTime())
      .average()
      .orElseThrow(IllegalArgumentException::new);
    return nonNormalization/(project.getEndTime()-project.getStartTime());
  }
}
