package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity;

import lombok.Value;

@Value
public class Task {
  private final Long id;
  private final String label;
}
