package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity;

import lombok.Value;

@Value
public class AvailabilityEmployee {
  private final Long id;
  private final Employee employee;
  private final long startTime;
  private final long endTime;
}
