package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider;

import java.util.List;

public interface IOptionProvider<T extends Comparable<T>> {

  T get(int number);

  List<T> getSubList(int fromIndex, int toIndex);

  int count();

  default T getTheBestOption(){
    return get(0);
  }
}
