package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity;

import lombok.Value;
import org.joda.time.Interval;

@Value
public class Assignment {
  private final Task task;
  private final Employee employee;
  private final Interval interval;
}
