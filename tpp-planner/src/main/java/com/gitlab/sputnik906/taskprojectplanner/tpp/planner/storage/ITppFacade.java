package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity.Employee;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity.ProjectPlan;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.joda.time.Interval;

public interface ITppFacade {

  boolean addEmployee(Employee employee);

  boolean addEmployees(Collection<Employee> employees);

  Set<Employee> getEmployees();

  Optional<Set<ProjectPlan>> removeEmployee(Employee employee);

  ResultAddProjectPlans addProjectPlans(List<ProjectPlan> projectPlans);

  ResultAddProjectPlans testAddShootWorks(
    List<ProjectPlan> projectPlans, Set<ProjectPlan> ignoryProjectPlans);

  Optional<Set<AssignmentOperation>> getOperationsByEmployee(Employee employee);

  Optional<Set<AssignmentOperation>> getOperationsByEmployee(
    Employee employee, Interval intersectedInterval);

  boolean removeProjectPlan(ProjectPlan projectPlan);

  Set<ProjectPlan> removeProjectPlans(Collection<ProjectPlan> projectPlans);

  ResultReplaceProjectPlans replaceProjectPlans(
    List<ReplaceOperationCommand<ProjectPlan>> replaceProjectPlanCommands);

  ResultReplaceProjectPlans testReplaceProjectPlans(
    List<ReplaceOperationCommand<ProjectPlan>> replaceProjectPlanCommands);

  boolean containsProjectPlan(ProjectPlan projectPlan);

  Set<ProjectPlan> getAllProjectPlans();

  default List<IOperation<?>> operationsFrom(ProjectPlan projectPlan) {
    return projectPlan.getAssignments().stream()
      .map(a->new AssignmentOperation(projectPlan,a))
      .collect(Collectors.toList());
  }

}
