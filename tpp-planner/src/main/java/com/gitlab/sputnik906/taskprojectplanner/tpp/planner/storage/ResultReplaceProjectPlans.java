package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import java.util.Set;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ResultReplaceProjectPlans {
  @NonNull private final Set<IOperation<?>> unremovedOperations;
  @NonNull
  private final ResultAddProjectPlans resultAddProjectPlans;

  public boolean isSuccessful() {
    if (unremovedOperations.size() > 0) {
      return false;
    }
    return resultAddProjectPlans.isSuccessful();
  }
}
