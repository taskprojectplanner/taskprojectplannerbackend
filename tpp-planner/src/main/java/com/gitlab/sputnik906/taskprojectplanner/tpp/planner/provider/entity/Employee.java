package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity;

import java.util.Set;
import lombok.Value;

@Value
public class Employee {
  private final Long id;

  private final Set<Skill> skills;
}
