package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.multistorage.ResultAddOperations;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.intersect.ResultIntersectAddOperation;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class ResultAddProjectPlans {
  @NonNull
  private final ResultAddOperations resultAddOperations;

  public boolean isSuccessful() {
    return resultAddOperations.isSuccessful();
  }

  public AssignmentOperation notAppliedOperation() {
    if (resultAddOperations.getNotAppliedOperation() instanceof AssignmentOperation) {
      return (AssignmentOperation) resultAddOperations.getNotAppliedOperation();
    }
    return null;
  }

  public Set<AssignmentOperation> intersectedConflictOperations() {
    if (resultAddOperations.getReasonNotAppliedOperation() instanceof ResultIntersectAddOperation) {
      ResultIntersectAddOperation<? extends IOperation<?>> result =
        (ResultIntersectAddOperation<? extends IOperation<?>>)
          resultAddOperations.getReasonNotAppliedOperation();
      return result.getIntersectedOperations().stream()
        .map(k -> (AssignmentOperation) k)
        .collect(Collectors.toSet());
    }
    return null;
  }


  public Set<AssignmentOperation> intersectedConflictBetweenAddedOperations() {
    if (resultAddOperations.getReasonNotAppliedOperationWithAddedOperations()
      instanceof ResultIntersectAddOperation) {
      ResultIntersectAddOperation<? extends IOperation<?>> result =
        (ResultIntersectAddOperation<? extends IOperation<?>>)
          resultAddOperations.getReasonNotAppliedOperationWithAddedOperations();
      return result.getIntersectedOperations().stream()
        .map(k -> (AssignmentOperation) k)
        .collect(Collectors.toSet());
    }
    return null;
  }


}
