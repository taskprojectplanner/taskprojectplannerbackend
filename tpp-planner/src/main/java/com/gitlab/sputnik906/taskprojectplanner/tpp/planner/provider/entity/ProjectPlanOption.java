package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity;

import java.util.Collections;
import java.util.Set;
import lombok.Value;

@Value
public class ProjectPlanOption implements Comparable<ProjectPlanOption>{
  private final Project project;

  private final Set<AssignmentOption> assignmentOptions;

  private final double evaluation;

  public Set<AssignmentOption> getAssignmentOptions() {
    return Collections.unmodifiableSet(assignmentOptions);
  }

  @Override
  public int compareTo(ProjectPlanOption o) {
    return Double.compare(evaluation,o.getEvaluation());
  }
}
