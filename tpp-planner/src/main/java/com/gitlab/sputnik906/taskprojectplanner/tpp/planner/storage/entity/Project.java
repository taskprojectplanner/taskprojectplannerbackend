package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity;

import lombok.Value;

@Value
public class Project {
  private final Long id;
}
