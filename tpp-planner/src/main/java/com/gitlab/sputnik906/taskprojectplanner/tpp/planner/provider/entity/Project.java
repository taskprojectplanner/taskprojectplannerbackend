package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity;

import java.util.Set;
import lombok.Value;

@Value
public class Project {
  private final Long id;

  private final Long startTime;

  private final Long endTime;

  private final Double priority;

  private final Set<Task> tasks;
}
