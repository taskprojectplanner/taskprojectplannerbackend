package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider;

import com.gitlab.sputnik906.entity.event.api.cdc.ChangesEntities;
import com.gitlab.sputnik906.entity.event.api.cdc.IChangeDataCaptureProvider;
import com.gitlab.sputnik906.entity.event.api.cdc.SnapshotEntityResponse;
import com.gitlab.sputnik906.entity.event.api.cdc.SnapshotEntityRequest;
import com.gitlab.sputnik906.entity.persist.BeanUtils;
import com.gitlab.sputnik906.entity.persist.EntityMapper;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity.Project;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProjectProvider implements Runnable{

  @NonNull
  private final IChangeDataCaptureProvider cdc;

  private final Map<Serializable,ProjectPlanOptionProvider> projectPlanOptionProviderMap = new HashMap<>();

  private SnapshotEntityResponse<?> snapshotEntityResponse;
  private Long lastHandledId;

  @Override
  public void run() {

    if (snapshotEntityResponse!=null){
      ChangesEntities changesEntities = cdc.getChanges(snapshotEntityResponse.getChangesRequest());
      changesEntities.getChanges().forEach(e->{
        if ("d".equals(e.getEventType())){
          projectPlanOptionProviderMap.remove(e.getEntityId());
          return;
        }
        throw new UnsupportedOperationException();
      });
      lastHandledId = changesEntities.getNextChangesRequest().get(0).getAfterEventId();
    }


    SnapshotEntityRequest snapshotEntityRequest = snapshotEntityResponse==null
      ?snapshotEntityRequest = createInitSnapshotEntityRequest(
        Project.class,
        5,
        null
         )
      :snapshotEntityResponse.getNextSnapshotEntityRequest();



    SnapshotEntityResponse<?> snapshotEntityResponse =  cdc.getSnapshotEntity(snapshotEntityRequest);

    List<Project> projects = snapshotEntityResponse.getEntities().stream()
      .map(e-> EntityMapper.convert(e,Project.class))
      .collect(Collectors.toList());

    projects.forEach(project -> {
      projectPlanOptionProviderMap.put(project.getId(),new ProjectPlanOptionProvider(project,cdc));
    });

    this.snapshotEntityResponse = snapshotEntityResponse;


  }

  protected SnapshotEntityRequest createInitSnapshotEntityRequest(Class<?> entityClass,int count,String where){
    Set<String> propertyNames = BeanUtils.convertConstructorBeanToFlatMap(entityClass)
      .keySet();
    return SnapshotEntityRequest.builder()
      .entityType(entityClass.getTypeName())
      .count(count)
      .where(where)
      .propertyNames(propertyNames)
      .build();
  }


}
