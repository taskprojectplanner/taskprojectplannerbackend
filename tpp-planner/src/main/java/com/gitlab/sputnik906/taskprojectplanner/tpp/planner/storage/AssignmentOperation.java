package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage;

import com.gitlab.sputnik906.operationsstorage.BaseOperation;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity.Assignment;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity.Employee;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity.ProjectPlan;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(callSuper = true)
public class AssignmentOperation extends BaseOperation<Employee> {

  private final ProjectPlan projectPlan;

  private final Assignment assignment;

  public AssignmentOperation( ProjectPlan projectPlan,Assignment assignment) {
    super(assignment.getEmployee(), assignment.getInterval());
    this.projectPlan=projectPlan;
    this.assignment=assignment;
  }
}
