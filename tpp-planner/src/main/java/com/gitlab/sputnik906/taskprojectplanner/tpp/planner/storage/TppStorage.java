package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage;

import com.gitlab.sputnik906.operationsstorage.IOperation;
import com.gitlab.sputnik906.operationsstorage.IOperationContainer;
import com.gitlab.sputnik906.operationsstorage.ReplaceOperationCommand;
import com.gitlab.sputnik906.operationsstorage.multistorage.ResourcesOperationsStorage;
import com.gitlab.sputnik906.operationsstorage.multistorage.ResultAddOperations;
import com.gitlab.sputnik906.operationsstorage.multistorage.ResultReplaceOperations;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.intersect.OperationIntersectConfig;
import com.gitlab.sputnik906.operationsstorage.operationcontainerimpl.intersect.OperationIntersectContainerImpl;
import com.gitlab.sputnik906.operationsstorage.operationtreeimpl.intervaltree.OperationTreeOnIntervalTree;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity.Employee;
import com.gitlab.sputnik906.taskprojectplanner.tpp.planner.storage.entity.ProjectPlan;
import com.lodborg.intervaltree.IntervalTree;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.joda.time.Interval;

public class TppStorage implements ITppFacade{

  private final Function<Object, IOperationContainer> operationContainerFactory =
    (resource) -> {
      if (resource instanceof Employee)
        return new EmployeeOperationContainer((Employee) resource);
      throw new UnsupportedOperationException("Type resource " + resource + " not support");
    };

  private final ResourcesOperationsStorage operationsStorage;

  public TppStorage(Map<Object, IOperationContainer> resourceToOperationContainerMap) {
    this.operationsStorage = new ResourcesOperationsStorage(
      resourceToOperationContainerMap,operationContainerFactory);
  }


  @Override
  public boolean addEmployee(
    Employee employee) {
    return operationsStorage.addResource(employee);
  }

  @Override
  public boolean addEmployees(
    Collection<Employee> employees) {
    employees.forEach(this::addEmployee);
    return true;
  }

  @Override
  public Set<Employee> getEmployees() {
    return operationsStorage.getAllResources().stream()
      .filter(r -> r instanceof Employee)
      .map(r -> (Employee) r)
      .collect(Collectors.toSet());
  }

  @Override
  public Optional<Set<ProjectPlan>> removeEmployee(
    Employee employee) {
    EmployeeOperationContainer employeeOperationContainer =
      (EmployeeOperationContainer) operationsStorage.removeResource(employee);
    if (employeeOperationContainer == null) return Optional.empty();

    return Optional.of(
      employeeOperationContainer.getOperations().stream()
        .map(AssignmentOperation::getProjectPlan)
        .peek(
          projectPlan ->
            operationsStorage.removeOperations(operationsFrom(projectPlan)))
        .collect(Collectors.toSet()));
  }

  private ResultAddProjectPlans addProjectPlans(
    boolean test, List<ProjectPlan> projectPlans, Set<ProjectPlan> ignoryProjectPlans) {

    List<IOperation<?>> operations =
      projectPlans.stream()
        .flatMap(shootWork -> operationsFrom(shootWork).stream())
        .collect(Collectors.toList());

    Set<IOperation<?>> ignoryOperations =
      ignoryProjectPlans.stream()
        .flatMap(shootWork -> operationsFrom(shootWork).stream())
        .collect(Collectors.toSet());

    ResultAddOperations resultAddOperations =
      test
        ? operationsStorage.testCommitOperations(operations, ignoryOperations)
        : operationsStorage.commitOperations(operations);

    return new ResultAddProjectPlans(resultAddOperations);
  }

  @Override
  public ResultAddProjectPlans addProjectPlans(
    List<ProjectPlan> projectPlans) {
    return addProjectPlans(false, projectPlans, new HashSet<>());
  }

  @Override
  public ResultAddProjectPlans testAddShootWorks(
    List<ProjectPlan> projectPlans,
    Set<ProjectPlan> ignoryProjectPlans) {
    return addProjectPlans(true, projectPlans, ignoryProjectPlans);
  }

  @Override
  public Optional<Set<AssignmentOperation>> getOperationsByEmployee(
    Employee employee) {
    return operationsStorage.getOperationsByResource(employee);
  }

  @Override
  public Optional<Set<AssignmentOperation>> getOperationsByEmployee(
    Employee employee, Interval intersectedInterval) {
    return operationsStorage.getIntersectedOperationsByResource(employee,intersectedInterval);
  }

  @Override
  public boolean removeProjectPlan(
    ProjectPlan projectPlan) {
    Set<IOperation<?>> removedOperations =
      operationsStorage.removeOperations(operationsFrom(projectPlan));
    return removedOperations.size() > 0;
  }

  @Override
  public Set<ProjectPlan> removeProjectPlans(
    Collection<ProjectPlan> projectPlans) {
    return projectPlans.stream().filter(this::removeProjectPlan).collect(Collectors.toSet());
  }

  private ResultReplaceProjectPlans replaceProjectPlans(
    boolean test, List<ReplaceOperationCommand<ProjectPlan>> replaceShootWorkCommands) {
    List<ReplaceOperationCommand<? extends IOperation<?>>> replaceOperationCommands =
      new ArrayList<>();

    for (ReplaceOperationCommand<ProjectPlan> replaceShootWork : replaceShootWorkCommands) {
      if (replaceShootWork.getTryToRemoveOperation() != null) {

        replaceOperationCommands.addAll(
          operationsFrom(replaceShootWork.getTryToRemoveOperation()).stream()
            .map(ReplaceOperationCommand::remove)
            .collect(Collectors.toList()));
      }

      if (replaceShootWork.getTryToAddOperation() != null) {

        replaceOperationCommands.addAll(
          operationsFrom(replaceShootWork.getTryToAddOperation()).stream()
            .map(ReplaceOperationCommand::add)
            .collect(Collectors.toList()));
      }
    }

    ResultReplaceOperations resultReplaceOperations =
      test
        ? operationsStorage.testReplace(replaceOperationCommands)
        : operationsStorage.replace(replaceOperationCommands);

    return new ResultReplaceProjectPlans(
      resultReplaceOperations.getUnremovedOperations(),
      new ResultAddProjectPlans(resultReplaceOperations.getResultAddOperations()));
  }

  @Override
  public ResultReplaceProjectPlans replaceProjectPlans(
    List<ReplaceOperationCommand<ProjectPlan>> replaceProjectPlanCommands) {
    return replaceProjectPlans(false, replaceProjectPlanCommands);
  }

  @Override
  public ResultReplaceProjectPlans testReplaceProjectPlans(
    List<ReplaceOperationCommand<ProjectPlan>> replaceProjectPlanCommands) {
    return replaceProjectPlans(true, replaceProjectPlanCommands);
  }

  @Override
  public boolean containsProjectPlan(
    ProjectPlan projectPlan) {
   return operationsStorage.containAllOperations(operationsFrom(projectPlan));
  }

  private Optional<Set<ProjectPlan>> getProjectPlansByResource(Employee employee) {
    return getProjectPlansByResource(employee, null);
  }

  private  Optional<Set<ProjectPlan>> getProjectPlansByResource(
    Employee employee, Interval interval) {
    Optional<Set<AssignmentOperation>> operations =
      interval != null
        ? operationsStorage.getIntersectedOperationsByResource(employee, interval)
        : operationsStorage.getOperationsByResource(employee);
    return operations.map(
      operationWithShootWorks ->
        operationWithShootWorks.stream()
          .map(AssignmentOperation::getProjectPlan)
          .collect(Collectors.toSet()));
  }

  @Override
  public Set<ProjectPlan> getAllProjectPlans() {
    return getEmployees().stream()
      .flatMap(employee -> getProjectPlansByResource(employee).orElse(new HashSet<>()).stream())
      .collect(Collectors.toSet());
  }

  public static class EmployeeOperationContainer
    extends OperationIntersectContainerImpl<Employee, AssignmentOperation> {

    public EmployeeOperationContainer(Employee resource) {
      super(
        new OperationTreeOnIntervalTree<>(new IntervalTree<>()),
        resource,
        OperationIntersectConfig.cantIntercectedAllOperations());
    }
  }
}
