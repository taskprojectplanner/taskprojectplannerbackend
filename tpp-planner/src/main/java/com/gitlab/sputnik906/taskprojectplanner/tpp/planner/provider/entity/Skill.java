package com.gitlab.sputnik906.taskprojectplanner.tpp.planner.provider.entity;

import lombok.Value;

@Value
public class Skill {
  private final Long id;
}
