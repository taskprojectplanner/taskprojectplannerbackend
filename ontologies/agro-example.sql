CREATE TABLE "Person" (
"id" INT NOT NULL PRIMARY KEY,
"first_name" VARCHAR(40) NOT NULL,
"last_name" VARCHAR(40) NOT NULL
);

INSERT INTO "Person"
("id","first_name","last_name") VALUES
(1,'Петр','Иванов'),
(2,'Иван','Петров');

UPDATE "Person" SET "first_name" = 'Петр 2' WHERE "id" = 1;