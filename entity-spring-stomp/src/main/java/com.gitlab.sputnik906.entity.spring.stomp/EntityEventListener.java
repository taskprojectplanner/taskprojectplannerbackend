package com.gitlab.sputnik906.entity.spring.stomp;

import com.gitlab.sputnik906.entity.event.api.TransactionEvents;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@Lazy(false)
public class EntityEventListener {

  @Autowired
  private SimpMessageSendingOperations messagingTemplate;


  @TransactionalEventListener
  public  void handler(TransactionEvents transactionEvents){

    transactionEvents.getEvents().forEach(e->{

      String[] entityTypeSplit = e.getEntityType().split("\\.");

      String topic = String.format(
        "/topic/%s/%s/%s",
        e.getEventType(),
        entityTypeSplit[entityTypeSplit.length-1],
        e.getEntityId()
      );

      System.out.println(topic);

      messagingTemplate.convertAndSend(topic,e);


    });



  }


}
