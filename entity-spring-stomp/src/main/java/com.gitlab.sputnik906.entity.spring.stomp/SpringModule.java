package com.gitlab.sputnik906.entity.spring.stomp;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Lazy;

@ComponentScan
@Lazy(false)
public class SpringModule {


}
