var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#events").html("");
}

function connect() {
    //var socket = new SockJS('http://localhost:8080/ws');
    //stompClient = Stomp.over(socket);
    stompClient = Stomp.client('ws://localhost:8080/ws')
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function subscribe() {
   var topic = $("#topic").val()
   var headersStr = $("#headers").val()
   var headers = {}// {selector: "headers['nativeHeaders']['props'][0].contains('label')"};
   if (headersStr.trim()){
   var splittedHeaders = headersStr.split(";;").forEach(e=>{
       var splittedHeader = e.split("=")
       headers[splittedHeader[0]]=splittedHeader[1]
      })
   }


    stompClient.subscribe(topic, function (event) {
        //console.log(event)
        showEvent(event.body);
   },headers);
}

function showEvent(message) {
    $("#events").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#subscribe" ).click(function() { subscribe(); });
});
