package com.gitlab.projectgenerator.module.generator

import com.github.mustachejava.DefaultMustacheFactory
import groovy.swing.SwingBuilder
import org.gradle.api.Plugin
import org.gradle.api.Project

import java.nio.charset.StandardCharsets
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes
import java.util.stream.Collectors

class ModuleGeneratorGradlePlugin implements Plugin<Project> {

  void apply(Project project) {
    project.task("generateSubModule") {
      //конструктор
      group = "generator"
      doLast {

        String moduleName = null;

        String templateName = null;

        String moduleGroup = project.group

        List<String> knowTemplates = knowTemplates()

        new SwingBuilder().edt {
          dialog(modal: true, // Otherwise the build will continue running before you closed the dialog
            title: 'Params', // Dialog title
            alwaysOnTop: true, // pretty much what the name says
            resizable: false, // Don't allow the user to resize the dialog
            locationRelativeTo: null, // Place dialog in center of the screen
            pack: true, // We need to pack the dialog (so it will take the size of it's children
            show: true // Let's show it
          ) {
            vbox { // Put everything below each other
              label(text: "Group:")
              moduleGroupComp = textField(text:moduleGroup)
              label(text: "Choose template:")
              templateNameComp = comboBox(items: knowTemplates)
              label(text: "Module name:")
              moduleNameComp = textField()
              button(defaultButton: true, text: 'OK', actionPerformed: {
                moduleName = moduleNameComp.getText();
                moduleGroup =moduleGroupComp.getText();
                templateName = templateNameComp.getSelectedItem()
                dispose(); // Close dialog
              })
            }
          }
        }

        if (moduleName==null || templateName==null||moduleGroup==null) {
          System.out.println("Error in input params")
          return
        }

        String basePackage = moduleGroup+"."+moduleName.toLowerCase().replace("-",".")

        HashMap<String, Object> scopes = new HashMap<String, Object>();
        scopes.put("basePackage", basePackage);
        scopes.put("basePackageDir", basePackage.replace(".","\\"));
        //scopes.put("moduleName",moduleName)
        scopes.put("startModuleName",moduleName.split("-")[0])

        createModuleByTemplate(moduleName,templateName,scopes)
      }
    }
  }

  static List<String> knowTemplates() {
    FileSystem fileSystemOnJar = FileSystems.newFileSystem(
      ModuleGeneratorGradlePlugin.getResource("").toURI(),
      Collections.<String, Object> emptyMap())

    Path templatesDirPath = fileSystemOnJar.getPath("/templates")

    List<String> knowTemplates = Files.walk(templatesDirPath, 1)
      .skip(1)//пропускаем саму корневую папку
      .map { path -> path.getFileName().toString() }
      .filter { fileName -> fileName.endsWith("/") } //признак директории
      .map { dirName -> dirName.substring(0, dirName.length() - 1) }
      .collect(Collectors.toList())
    //.forEach{dirName->System.out.println("        "+dirName)}

    fileSystemOnJar.close()

    return knowTemplates;
  }

  static void createModuleByTemplate(String moduleName, String templateDirName, HashMap<String, Object> scopes) {

    DefaultMustacheFactory mf = new DefaultMustacheFactory();

    Map<String, String> env = new HashMap<>();
    //env.put("create", "true");

    URI jarUri = ModuleGeneratorGradlePlugin.getResource("").toURI()

    FileSystem jarFileSystem = FileSystems.newFileSystem(jarUri, env)
    Path fromPath = jarFileSystem.getPath("/templates", templateDirName)

    Path toRootDirPath = Paths.get(new File(moduleName).absolutePath);//целевая папка для модуля

    Files.walkFileTree(fromPath, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {

        Path targetDirPath = toRootDirPath.resolve(fromPath.relativize(dir).toString());

        StringWriter writer = new StringWriter();
        mf.compile(new StringReader(targetDirPath.toAbsolutePath().toString()), "example").execute(writer, scopes).flush();
        targetDirPath = Paths.get(writer.toString());


        if (!Files.exists(targetDirPath)) {
          Files.createDirectories(targetDirPath);
        }
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        StringWriter writer = new StringWriter();
        mf.compile(new StringReader(toRootDirPath.resolve(fromPath.relativize(file).toString()).toAbsolutePath().toString()), "example").execute(writer, scopes).flush();

        //Хак, т.к. .gitignore не копируется в jar
        String dstPathOfFile = writer.toString();
        if (dstPathOfFile.endsWith("gitignore"))
          dstPathOfFile=dstPathOfFile.replace("gitignore",".gitignore")

        compileFileTemplate(mf, file, dstPathOfFile, scopes);

        return FileVisitResult.CONTINUE;
      }
    });

    jarFileSystem.close()
  }

  static void compileFileTemplate(DefaultMustacheFactory mf,
                                  Path file, String dstPathOfFile, HashMap<String, Object> scopes) {
    File dstFile = new File(dstPathOfFile);
    if (!dstFile.exists()) {
      dstFile.createNewFile();
      Writer fw = new OutputStreamWriter(new FileOutputStream(dstPathOfFile), StandardCharsets.UTF_8);
      try {
        mf.compile(Files.newBufferedReader(file), "example").execute(fw, scopes).flush();
      } finally {
        fw.close();
      }
    }
  }


}
