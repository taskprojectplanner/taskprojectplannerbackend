package com.gitlab.sputnik906.tpp.domain.entity.planning;

import com.gitlab.sputnik906.entity.persist.entity.IdentifiableLong;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@FieldNameConstants
@Getter
@Entity
public class ProjectPlanOption extends IdentifiableLong implements Comparable<ProjectPlanOption>{

  @OneToOne(fetch = FetchType.LAZY)
  @NotNull
  @NonNull
  private ProjectTask projectTask;

  @OneToMany(
    fetch = FetchType.LAZY,
    //mappedBy = "projectPlanOption",
    cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE},
    orphanRemoval = true)
  @NonNull
  private List<AssignmentOption> assignmentOptions;

  @NotNull @NonNull @Min(0)private Double evaluation;

  public List<AssignmentOption> getAssignmentOptions() {
    return Collections.unmodifiableList(assignmentOptions);
  }

  @Override
  public int compareTo(ProjectPlanOption o) {
    return Double.compare(evaluation,o.getEvaluation());
  }
}
