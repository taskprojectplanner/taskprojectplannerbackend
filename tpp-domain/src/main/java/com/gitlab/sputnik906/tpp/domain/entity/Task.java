package com.gitlab.sputnik906.tpp.domain.entity;

import com.gitlab.sputnik906.entity.event.api.annotation.CdcEntity;
import com.gitlab.sputnik906.entity.event.api.annotation.Related;
import com.gitlab.sputnik906.tpp.domain.PlanService;
import com.gitlab.sputnik906.tpp.domain.PlanService.PlanTaskCommand;
import com.gitlab.sputnik906.tpp.domain.PlanService.PlanTaskCommandResult;
import com.gitlab.sputnik906.entity.persist.entity.IdentifiableLong;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@FieldNameConstants
@Getter
@ToString(callSuper = true)
@Entity
@CdcEntity
public class Task extends IdentifiableLong {

  @NonNull @NotNull @Setter private String label;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @NotNull
  @Setter(AccessLevel.PACKAGE)
  @Related("tasks")
  private Project project;

  @ManyToMany @NonNull @NotNull private Set<Skill> requiredSkills;

  @Min(0)
  @NonNull
  @NotNull
  @Setter
  private Long duration;

  @OneToOne(
      fetch = FetchType.LAZY,
      //mappedBy = "task",
      cascade = {CascadeType.MERGE, CascadeType.REMOVE},
      orphanRemoval = true)
  @Setter(AccessLevel.PACKAGE)
  private Assignment assignment;

  public Set<Skill> getRequiredSkills() {
    return Collections.unmodifiableSet(requiredSkills);
  }

  public boolean addRequiredSkills(Collection<Skill> skills) {
    boolean result = requiredSkills.addAll(skills);
    if (result
        && assignment != null
        && !assignment.getEmployee().getSkills().containsAll(requiredSkills)) {
      assignment = null;
    }
    return result;
  }

  public boolean removeRequiredSkills(Collection<Skill> skills) {
    return requiredSkills.removeAll(skills);
  }

  public void clearRequiredSkills() {
    requiredSkills.clear();
  }

  public boolean unPlan() {
    if (assignment == null) return false;
    assignment = null;
    return true;
  }

  public static PlanTaskCommandResult[] plan(PlanTaskCommand[] commands) {
    return  Stream.of(commands)
      .map(c -> PlanService.instance.plan(c))
      .toArray(PlanTaskCommandResult[]::new);
  }

}
