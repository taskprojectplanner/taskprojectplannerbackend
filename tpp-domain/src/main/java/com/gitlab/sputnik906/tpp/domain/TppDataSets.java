package com.gitlab.sputnik906.tpp.domain;

import com.gitlab.sputnik906.entity.persist.ExampleDataSets;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.tpp.domain.entity.Employee;
import com.gitlab.sputnik906.tpp.domain.entity.Project;
import com.gitlab.sputnik906.tpp.domain.entity.Skill;
import com.gitlab.sputnik906.tpp.domain.entity.Task;
import com.gitlab.sputnik906.tpp.domain.entity.planning.AvailabilityEmployee;
import com.gitlab.sputnik906.tpp.domain.entity.planning.ProjectTask;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.Value;
import lombok.experimental.UtilityClass;

@UtilityClass
public class TppDataSets extends ExampleDataSets {

  public static final Instant BASE_TIME = Instant.parse("2020-01-01T10:00:00.00Z");

  public static Skill exampleSkill(){
    return new Skill("Skill");
  }

  public static Employee exampleEmployee(Skill ... skills){
    return new Employee("Employee",new HashSet<>(Arrays.asList(skills)));
  }

  public static Project exampleProject(List<Task> tasks){
    return exampleProject(tasks.toArray(new Task[0]));
  }

  public static Project exampleProject(Task ... tasks){
    Project project = new Project(
      "Project",
      BASE_TIME.toEpochMilli(),
      BASE_TIME.plus(Duration.ofDays(1)).toEpochMilli(),
      1.0
    );
    if (tasks.length>0) project.addTasks(Arrays.asList(tasks));
    return project;
  }

  public static Skill canProgrammingJava(){
    return new Skill("Может программировать на java");
  }

  public static Skill canTestingJava(){
    return new Skill("Может тестировать программы на java");
  }

  public static Skill canTestingKotlin(){
    return new Skill("Может тестировать программы на kotlin");
  }

  public static List<Skill> defaultDeveloperSkills(){
    return Arrays.asList(canProgrammingJava(),canTestingJava(),canTestingKotlin());
  }

  public static List<Task> exampleProjectTasks(Skill ... skills){
    return createTasks(1,3,1,skills);
  }

  public static List<Employee> createEmployees(int startNumber, int count, Collection<Skill> skills){
    return rangeGenerator(startNumber,count,
      i -> new Employee("Employee "+i,new HashSet<>(skills)));
  }

  public static List<AvailabilityEmployee> createAvailabilityEmployee(List<Employee> employees,List<Interval> intervals){
    return employees.stream()
      .flatMap(employee -> intervals.stream()
        .map(interval -> new AvailabilityEmployee(employee,interval.getStartTime(),interval.getEndTime()))
      ).collect(Collectors.toList());
  }

  @Value
  public static class Interval{
    Instant startInstant;
    Duration duration;

    public long getStartTime() {
      return startInstant.toEpochMilli();
    }

    public long getEndTime() {
      return startInstant.plus(duration).toEpochMilli();
    }

  }

  public static List<Task> createTasks(int startNumber, int count, int defDurationInHours,
    Skill ... skills){
    return rangeGenerator(startNumber,count,
      i -> new Task(
        "Task "+i,
        new HashSet<>(Arrays.asList(skills)),
        Duration.ofHours(defDurationInHours).toMillis())
    );
  }

  public static List<Project> createProjects(int startNumber, int count){
    return rangeGenerator(startNumber,count,
      i -> new Project("Project "+i,BASE_TIME.toEpochMilli(),
        BASE_TIME.plus(Duration.ofDays(2)).toEpochMilli(),1.0));
  }

  public static Map<Class<?>,List<?>> devDataSet(){
    Map<Class<?>,List<?>> dataSet = new LinkedHashMap<>(); //важен порядок

    List<Skill> defaultDeveloperSkills = defaultDeveloperSkills();
    dataSet.put(Skill.class,defaultDeveloperSkills);

    List<Employee> employees = createEmployees(1, 6, defaultDeveloperSkills);
    dataSet.put(Employee.class,employees);

    dataSet.put(AvailabilityEmployee.class,
      createAvailabilityEmployee(employees,Arrays.asList(
        new Interval(BASE_TIME,Duration.ofHours(1)),
        new Interval(BASE_TIME.plus(Duration.ofDays(1)),Duration.ofHours(3))
      ))
    );

    List<Project> projects = createProjects(1,7);

    projects.forEach(p->p.addTasks(
      createTasks(1,3,1,defaultDeveloperSkills.get(0))
    ));

    dataSet.put(Project.class,projects);

    return dataSet;
  }

  public static void applyDevEvents(EntityRepository repository){
    repository.inTransaction(r->r.persist(new ProjectTask(r.findById(1L,Project.class).orElse(null))));
  }




}
