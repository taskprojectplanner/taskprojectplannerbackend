package com.gitlab.sputnik906.tpp.domain.entity;

import com.gitlab.sputnik906.entity.event.api.annotation.CdcEntity;
import com.gitlab.sputnik906.entity.persist.entity.IdentifiableLong;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@FieldNameConstants
@ToString(exclude = "tasks")
@Getter
@Entity
@CdcEntity
public class Project extends IdentifiableLong {

  @NonNull @NotNull @Setter
  private String label;

  @NonNull
  @NotNull
  @Min(0)
  private Long startTime;

  @NonNull
  @NotNull
  @Min(0)
  private Long endTime;

  @NonNull
  @NotNull
  private Double priority;

  @OneToMany(
      fetch = FetchType.LAZY,
      mappedBy = "project",
      cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE},
      orphanRemoval = true)
  private Set<Task> tasks = new HashSet<>();


  public boolean addTasks(Collection<Task> tasks) {
    tasks.forEach(t->t.setProject(this));
    return this.tasks.addAll(tasks);
  }

  public boolean removeTasks(Collection<Task> tasks) {
    tasks.forEach(t->t.setProject(null));
    return this.tasks.removeAll(tasks);
  }


}
