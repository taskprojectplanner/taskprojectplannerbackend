package com.gitlab.sputnik906.tpp.domain.entity.planning;

import com.gitlab.sputnik906.entity.persist.Validations;
import com.gitlab.sputnik906.entity.persist.entity.IdentifiableLong;
import com.gitlab.sputnik906.tpp.domain.entity.Task;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@FieldNameConstants
@Getter
@Entity
public class AssignmentOption extends IdentifiableLong {

//  @ManyToOne(fetch = FetchType.LAZY, optional = false)
//  @NotNull @NonNull
//  private ProjectPlanOption projectPlanOption;

  @OneToOne(fetch = FetchType.LAZY)
  private Task task;

  @OneToOne(fetch = FetchType.LAZY)
  private AvailabilityEmployee availabilityEmployee;

  private Long startAllowedTime;

  private Long endAllowedTime;

  public AssignmentOption(Task task,
    AvailabilityEmployee availabilityEmployee, Long startAllowedTime, Long endAllowedTime) {

    Objects.requireNonNull(task);
    this.task = task;

    Objects.requireNonNull(availabilityEmployee);
    this.availabilityEmployee = availabilityEmployee;

    Objects.requireNonNull(startAllowedTime);
    Validations.shouldBeTrue(
      startAllowedTime>=availabilityEmployee.getStartAvailabilityTime()
      &&startAllowedTime<availabilityEmployee.getEndAvailabilityTime(),
      Fields.startAllowedTime
    );
    this.startAllowedTime = startAllowedTime;

    Objects.requireNonNull(endAllowedTime);
    Validations.shouldBeTrue(
      endAllowedTime>availabilityEmployee.getStartAvailabilityTime()
        &&endAllowedTime<=availabilityEmployee.getEndAvailabilityTime(),
      Fields.endAllowedTime
    );
    this.endAllowedTime = endAllowedTime;
  }
}
