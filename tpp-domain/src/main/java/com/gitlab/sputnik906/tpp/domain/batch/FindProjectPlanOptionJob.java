package com.gitlab.sputnik906.tpp.domain.batch;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.Job;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.Tasklet;
import java.util.Collections;
import java.util.List;


public class FindProjectPlanOptionJob implements Job {

  private final List<Tasklet> steps;

  public FindProjectPlanOptionJob(
    EntityRepository repository, Long projectTaskId) {
    this.steps = Collections.singletonList(new FindProjectPlanOptionStep(repository,projectTaskId));
  }

  @Override
  public List<Tasklet> steps() {
    return steps;
  }
}
