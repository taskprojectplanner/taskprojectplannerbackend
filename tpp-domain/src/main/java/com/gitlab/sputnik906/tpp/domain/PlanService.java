package com.gitlab.sputnik906.tpp.domain;

import com.gitlab.sputnik906.entity.event.api.ITransactionEventsHandler;
import com.gitlab.sputnik906.entity.event.api.TransactionEvents;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.JobLauncher;
import com.gitlab.sputnik906.tpp.domain.entity.Assignment;
import com.gitlab.sputnik906.tpp.domain.entity.Employee;
import com.gitlab.sputnik906.tpp.domain.entity.Task;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.LockModeType;
import java.util.Collection;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@RequiredArgsConstructor
public class PlanService implements ITransactionEventsHandler {

  public static PlanService instance;

  @NonNull @Getter private final EntityRepository repository;
  @NonNull @Getter private final JobLauncher jobLauncher;


  public PlanTaskCommandResult plan(PlanTaskCommand command) {
    return repository.inTransactionWithResult(r->{
      long startTime = command.getStartTime();

      Employee employee = repository.findById(
        command.getEmployeeId(),
        Employee.class,
        LockModeType.PESSIMISTIC_READ
      ).orElse(null);

      if (employee==null) return command.error("Employee with id not found: "+command.getEmployeeId());

      Task task = repository.findById(
        command.getTaskId(),
        Task.class,
        LockModeType.PESSIMISTIC_READ
      ).orElse(null);

      if (task==null) return command.error("Task with id not found: "+command.getTaskId());

      List<Assignment> overlaps = Assignment.overlaps(repository,employee.getId(),startTime, startTime + task.getDuration());

      if (overlaps.size()>0) return command.overlaps(overlaps.stream()
        .map(a->new AssigmentDto(a.getTask().getId(),a.getEmployee().getId(),a.getStartTime(),a.getEndTime()))
        .collect(Collectors.toList()));

      Assignment assignment = Assignment.from(task, employee, startTime, startTime + task.getDuration());

      repository.persist(assignment);

      return command.ok();
    });


  }

  @Override
  public void handle(TransactionEvents transactionEvents) {

  }

  @Value
  public static class PlanTaskCommand {
    @NonNull private Long taskId;
    @NonNull private Long employeeId;
    @NonNull private Long startTime;

    public PlanTaskCommandResult error(String error){
      return new PlanTaskCommandResult(this, Collections.emptyList(),error);
    }

    public PlanTaskCommandResult ok(){
      return new PlanTaskCommandResult(this,Collections.emptyList(),null);
    }

    public PlanTaskCommandResult overlaps(Collection<AssigmentDto> overlaps){
      return new PlanTaskCommandResult(this,overlaps,null);
    }
  }

  @Value
  public static class PlanTaskCommandResult {
    private PlanTaskCommand command;
    private Collection<AssigmentDto> overlaps;
    private String error;

    public boolean isSuccess() {
      return error==null&&!overlaps.iterator().hasNext();
    }

    public void setSuccess(boolean success) {} // for correct json deserialisation

  }

  @Value
  public static class AssigmentDto{
    private Long taskId;
    private Long employeeId;
    private Long startTime;
    private Long endTime;
  }

}
