package com.gitlab.sputnik906.tpp.domain.entity.planning;

import com.gitlab.sputnik906.entity.persist.Maps;
import com.gitlab.sputnik906.entity.persist.entity.IdentifiableLong;
import com.gitlab.sputnik906.tpp.domain.PlanService;
import com.gitlab.sputnik906.tpp.domain.batch.FindProjectPlanOptionJob;
import com.gitlab.sputnik906.tpp.domain.entity.Project;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PostPersist;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@FieldNameConstants
@Getter
@Entity
public class ProjectTask extends IdentifiableLong {
  @OneToOne @NonNull @JoinColumn(unique=true)
  private Project project;

  private ProjectTaskState state = ProjectTaskState.Created;

//  @PostPersist
//  protected void postPersistHook() {
//    PlanService.instance.getJobLauncher()
//      .run(
//        new FindProjectPlanOptionJob(PlanService.instance.getRepository(),getId()),
//        Maps.of("projectTaskId",String.valueOf(getId()))
//       );
//
//  }

  public enum ProjectTaskState{
    Created
  }
}
