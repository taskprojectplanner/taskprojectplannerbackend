package com.gitlab.sputnik906.tpp.domain.entity.planning;

import com.gitlab.sputnik906.entity.persist.entity.IdentifiableLong;
import com.gitlab.sputnik906.tpp.domain.entity.Employee;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@FieldNameConstants
@Getter
@Entity
public class AvailabilityEmployee extends IdentifiableLong {
  @OneToOne(fetch = FetchType.LAZY)
  @NotNull
  @NonNull
  private Employee employee;

  @NotNull @NonNull @Min(0) private Long startAvailabilityTime;

  @NotNull @NonNull @Min(0) private Long endAvailabilityTime;
}
