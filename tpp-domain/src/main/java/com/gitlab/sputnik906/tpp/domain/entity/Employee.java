package com.gitlab.sputnik906.tpp.domain.entity;

import com.gitlab.sputnik906.entity.event.api.annotation.CdcEntity;
import com.gitlab.sputnik906.entity.persist.entity.IdentifiableLong;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@FieldNameConstants
@ToString(callSuper = true)
@Getter
@Entity
@CdcEntity
public class Employee extends IdentifiableLong {

  @NonNull @NotNull @Setter private String label;

  @ManyToMany @NonNull @NotNull private Set<Skill> skills;

  @OneToMany(
    fetch = FetchType.LAZY,
    mappedBy = "employee",
    cascade = CascadeType.REMOVE)
  private Set<Assignment> assignments = new HashSet<>();

  public Set<Assignment> getAssignments() {
    return Collections.unmodifiableSet(assignments);
  }

  public Set<Skill> getSkills() {
    return Collections.unmodifiableSet(skills);
  }

  public boolean addSkills(List<Skill> skills) {
    return this.skills.addAll(skills);
  }

  public boolean removeSkills(List<Skill> skills) {
    boolean isChanged = this.skills.removeAll(skills);
    if (isChanged) {
      List<Assignment> brokenAssigmnents =
          assignments.stream()
              .filter(a -> !this.skills.containsAll(a.getTask().getRequiredSkills()))
              .collect(Collectors.toList());
      removeAssignments(brokenAssigmnents);
    }
    return isChanged;
  }

  protected boolean addAssignments(List<Assignment> addAssigmnents){
    return this.assignments.addAll(addAssigmnents);
  }

  public boolean removeAssignments(List<Assignment> removedAssigmnents) {
    boolean isChanged = false;
    for (Assignment a : removedAssigmnents) {
      if (assignments.remove(a)) {
        isChanged = true;
        a.getTask().unPlan();
      }
    }
    return isChanged;
  }


}
