package com.gitlab.sputnik906.tpp.domain.entity;

import com.gitlab.sputnik906.entity.event.api.annotation.CdcEntity;
import com.gitlab.sputnik906.entity.persist.entity.IdentifiableLong;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@FieldNameConstants
@Getter
@Entity
@CdcEntity
public class Skill extends IdentifiableLong {

  @NonNull @NotNull private String label;

}
