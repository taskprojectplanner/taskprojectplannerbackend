package com.gitlab.sputnik906.tpp.domain.batch;

import com.gitlab.sputnik906.entity.persist.WhereBuilder;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.entity.persist.repository.query.EntityJpaQuery;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.ChunkContext;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.RepeatStatus;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.StepContribution;
import com.gitlab.sputnik906.taskprojectplanner.batch.api.Tasklet;
import com.gitlab.sputnik906.tpp.domain.entity.Employee;
import com.gitlab.sputnik906.tpp.domain.entity.Project;
import com.gitlab.sputnik906.tpp.domain.entity.Skill;
import com.gitlab.sputnik906.tpp.domain.entity.Task;
import com.gitlab.sputnik906.tpp.domain.entity.planning.AssignmentOption;
import com.gitlab.sputnik906.tpp.domain.entity.planning.AvailabilityEmployee;
import com.gitlab.sputnik906.tpp.domain.entity.planning.AvailabilityEmployee.Fields;
import com.gitlab.sputnik906.tpp.domain.entity.planning.ProjectPlanOption;
import com.gitlab.sputnik906.tpp.domain.entity.planning.ProjectTask;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FindProjectPlanOptionStep implements Tasklet {

  private final EntityRepository repository;
  private final Long projectTaskId;

  @Override
  public RepeatStatus execute(
    StepContribution contribution,
    ChunkContext chunkContext) throws Exception {

    ProjectTask projectTask = repository.findById(projectTaskId,ProjectTask.class)
    .orElseThrow(EntityNotFoundException::new);

    Project project = projectTask.getProject();

    List<AssignmentOption> assignmentOptions = new ArrayList<>();

    List<Task> orderedTasks = new ArrayList<>(project.getTasks());

    boolean getNext = false;

    for(int i=0; i<orderedTasks.size(); i++){

      Task task = orderedTasks.get(i);

      Object lastAvailabilityEmployeeId = chunkContext.getAttribute(generateKeyForLastHandledIdOfAvailabilityEmployeeForTask(task));
      if (lastAvailabilityEmployeeId==null) lastAvailabilityEmployeeId=-1;

      AvailabilityEmployee availabilityEmployee;

      if (lastAvailabilityEmployeeId.equals(-1)||orderedTasks.get(orderedTasks.size()-1).equals(task)||getNext){
        getNext= false;
        EntityJpaQuery<AvailabilityEmployee> query = new EntityJpaQuery<>(AvailabilityEmployee.class);
        query.setSize(1);
        query.setWhere(new WhereBuilder()
          .allMemberOf(
            task.getRequiredSkills().stream()
              .map(Skill::getId)
              .collect(Collectors.toSet()),
            Fields.employee+"."+Employee.Fields.skills
          )
          .and()
          .overlap(
            Fields.endAvailabilityTime,
            project.getStartTime(),
            Fields.startAvailabilityTime,
            project.getEndTime()
          )
          .and()
          .expression(" id > "+lastAvailabilityEmployeeId)
          .and()
          .expression(String.format(" %s - %s > %d ",Fields.endAvailabilityTime,Fields.startAvailabilityTime,task.getDuration()))
          .toString()
        );

        List<AvailabilityEmployee> availabilityEmployees =  repository.query(query);

        if (availabilityEmployees.isEmpty()){
          if (lastAvailabilityEmployeeId.equals(-1)) return RepeatStatus.FINISHED;//нет ни одного варианта для задачи
          chunkContext.setAttribute(generateKeyForLastHandledIdOfAvailabilityEmployeeForTask(task),-1);
          i--;
          i--;
          if (i<-1) return RepeatStatus.FINISHED; //перебрали все
          assignmentOptions.remove(assignmentOptions.size()-1);
          getNext = true;
          continue;
        }


        availabilityEmployee = availabilityEmployees.get(0);


      }else{
        availabilityEmployee = repository.findById((Serializable) lastAvailabilityEmployeeId,AvailabilityEmployee.class)
          .orElseThrow(EntityNotFoundException::new);
      }

      AssignmentOption assignmentOption = new AssignmentOption(task,availabilityEmployee,
        availabilityEmployee.getStartAvailabilityTime(),
        availabilityEmployee.getStartAvailabilityTime()+task.getDuration()
      );

      assignmentOptions.add(assignmentOption);

      chunkContext.setAttribute(generateKeyForLastHandledIdOfAvailabilityEmployeeForTask(task),availabilityEmployee.getId());
    }

    ProjectPlanOption projectPlanOption = new ProjectPlanOption(
      projectTask,
      assignmentOptions,
      evaluation(project,assignmentOptions)
    );

    repository.persist(projectPlanOption);


    return RepeatStatus.CONTINUABLE;
  }

  private static String generateKeyForLastHandledIdOfAvailabilityEmployeeForTask(Task task){
    return "Last Handled Availability Employee id for Task "+task.getId();
  }

  protected double evaluation(Project project, Collection<AssignmentOption> assignmentOptions){
    double nonNormalization = assignmentOptions.stream()
      .mapToDouble(a->a.getStartAllowedTime()-project.getStartTime())
      .average()
      .orElseThrow(IllegalArgumentException::new);
    return nonNormalization/(project.getEndTime()-project.getStartTime());
  }
}
