package com.gitlab.sputnik906.tpp.domain.entity;

import com.gitlab.sputnik906.entity.persist.WhereBuilder;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.entity.persist.entity.IdentifiableLong;
import java.util.Collections;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@FieldNameConstants
@Getter
@Entity
public class Assignment extends IdentifiableLong {

  @OneToOne(fetch = FetchType.LAZY)
  @NotNull @NonNull
  private Task task;

  @ManyToOne(fetch = FetchType.LAZY)
  @NotNull @NonNull
  private Employee employee;

  @NotNull @NonNull @Min(0) private Long startTime;

  @NotNull @NonNull @Min(0) private Long endTime;

  public static Assignment from(Task task,Employee employee,Long startTime, Long endTime){
    Assignment assignment = new Assignment(task,employee,startTime,endTime);
    task.setAssignment(assignment);
    employee.addAssignments(Collections.singletonList(assignment));
    return assignment;
  }

  public static List<Assignment> overlaps(EntityRepository repo,
    Long employeeId,Long startTime,Long endTime){

    return repo.query(
      Assignment.class,
      new WhereBuilder()
      .expression("employee.id = ",employeeId)
      .and()
      .overlap(
          "endTime",
          startTime,
          "startTime",
          endTime
       )
    );


  }

}
