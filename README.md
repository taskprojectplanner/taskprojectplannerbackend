# TaskProjectPlannerBackend

## iDEA
Required plugins:
1. Lombok
2. google-java-format

Set Code Style | Java from config/intellij-java-google-style.xml
Before push you should apply Idea's command 'Reformat code' on src dir
Bug IDEA: If you set 'Reformat code' in Commit Dialog - it's don't work

Set Settings-Build, Executions, Deployment-Compiler-Annotation Compiler-Enable annotation processing
Set Settings-Build, Executions, Deployment-Build tools-Gradle-Automatically import this project

