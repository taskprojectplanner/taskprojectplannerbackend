package com.gitlab.sputnik906.taskprojectplanner.actor.system.api;

import java.util.function.Consumer;

public interface IAskResult<RESPONSE> {
  IAskResult<Void> success(Consumer<RESPONSE> consumer);
  IAskResult<Void> error(Consumer<Throwable> consumer);
}
