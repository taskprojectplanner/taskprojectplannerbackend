package com.gitlab.sputnik906.taskprojectplanner.actor.system.api;

public interface IBehaviorContext {

  <B extends AbstractBehavior> IActorRef<B> createChild(B behavior);

  <B extends AbstractBehavior> IActorRef<B> self();
}
