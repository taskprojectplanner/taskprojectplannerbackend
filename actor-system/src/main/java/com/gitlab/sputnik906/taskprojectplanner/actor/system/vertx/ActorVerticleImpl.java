package com.gitlab.sputnik906.taskprojectplanner.actor.system.vertx;

import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.AbstractBehavior;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.BehaviorTerminatedException;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.CompletableFutureWithoutBlockMethod;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.IActorRef;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.IBehaviorContext;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

class ActorVerticleImpl<B extends AbstractBehavior> extends AbstractVerticle implements IActorRef<B>,
  IBehaviorContext {

  private static final Logger LOG = LoggerFactory.getLogger(ActorVerticleImpl.class);

  private final Set<Consumer<ActorVerticleImpl<B>>> onCloseActions = Collections.synchronizedSet(new HashSet<>());

  private final ActorSystemVertxImpl actorSystemVertx;

  private final B behavior;

  ActorVerticleImpl(ActorSystemVertxImpl actorSystemVertx,B behavior) {
    this.actorSystemVertx = actorSystemVertx;
    this.behavior = behavior;
    this.behavior.setBehaviorContext(this);
  }


  @Override
  public void start() throws Exception {
    LOG.info("Actor deployed");
  }

  @Override
  public void stop() throws Exception {
     LOG.info("Actor undeployed");
  }

  @Override
  public void tell(Consumer<B> action) {
    context.runOnContext((ignore)->{
      if (behavior.isTerminated()){
        LOG.warn("Behavior was terminated");
        return;
      }
      action.accept(behavior);
      if (behavior.isTerminated()) fireTerminate();
    });
  }


  @Override
  public <RESPONSE> CompletableFutureWithoutBlockMethod<RESPONSE> ask(
    Function<B, RESPONSE> action) {

    CompletableFutureWithoutBlockMethod<RESPONSE> result = new CompletableFutureWithoutBlockMethod<>();

    final Context currentVertxContext = Vertx.currentContext();//null if method was invoked outside Verticle

    Consumer<Runnable> invokeContext = r->{
      if (currentVertxContext!=null)
        currentVertxContext.runOnContext((ignory)->r.run());
      else
        r.run();
    };

    context.runOnContext((ignore)->{
      if (result.isCancelled()) return;

      Throwable exception=null;

      if (behavior.isTerminated()){
        LOG.warn("Behavior was terminated");
        exception=new BehaviorTerminatedException(behavior);
      }else{
        try{
          RESPONSE response = action.apply(behavior);

          invokeContext.accept(()->result.complete(response));

        }catch (Exception e){
          exception=e;
        }
      }

      if (exception!=null){
        Throwable finalException = exception;
        invokeContext.accept(()->result.completeExceptionally(finalException));
      }

      if (!(exception instanceof BehaviorTerminatedException) && behavior.isTerminated()) fireTerminate();
    });

    return result;
  }

  private void fireTerminate(){
    vertx.undeploy(deploymentID());
    onCloseActions.forEach(a->a.accept(this));
  }

  public boolean onClose(Consumer<ActorVerticleImpl<B>> action){
    return onCloseActions.add(action);
  }


  @Override
  public <S extends AbstractBehavior> IActorRef<S> createChild(
    S behavior) {
    return actorSystemVertx.create(vertx,behavior);
  }

  @Override
  public  IActorRef<B> self() {
    return this;
  }

}
