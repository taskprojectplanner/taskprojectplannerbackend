package com.gitlab.sputnik906.taskprojectplanner.actor.system.api;

import java.util.function.Consumer;
import java.util.function.Function;

public interface IActorRef<B extends AbstractBehavior> {

  void tell(Consumer<B> action);

  <RESPONSE> CompletableFutureWithoutBlockMethod<RESPONSE> ask(Function<B, RESPONSE> action);

}
