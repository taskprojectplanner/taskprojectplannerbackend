package com.gitlab.sputnik906.taskprojectplanner.actor.system.api;

public abstract class AbstractBehavior {

  protected IBehaviorContext context;

  private boolean terminated;

  public boolean isTerminated(){
    return terminated;
  }

  protected void terminate(){
    this.terminated=true;
  }

  public synchronized void setBehaviorContext(IBehaviorContext context){
    if (this.context!=null) throw new IllegalStateException("Context cant be changed");
    this.context=context;
  }


}
