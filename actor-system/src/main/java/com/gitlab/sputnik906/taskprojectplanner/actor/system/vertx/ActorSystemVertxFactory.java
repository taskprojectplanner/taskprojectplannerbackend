package com.gitlab.sputnik906.taskprojectplanner.actor.system.vertx;

import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.IActorSystem;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.IActorSystemFactory;
import io.vertx.core.VertxOptions;

public final class ActorSystemVertxFactory implements IActorSystemFactory {

  @Override
  public IActorSystem defaultActorSystem() {
    return new ActorSystemVertxImpl(new VertxOptions());
  }

}
