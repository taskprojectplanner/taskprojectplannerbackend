package com.gitlab.sputnik906.taskprojectplanner.actor.system.api;

public class BehaviorTerminatedException extends Throwable{
  private final AbstractBehavior behavior;

  public BehaviorTerminatedException(
    AbstractBehavior behavior) {
    this.behavior = behavior;
  }

  @Override
  public String toString() {
    return "BehaviorTerminatedException{" +
      "behavior=" + behavior +
      '}';
  }
}
