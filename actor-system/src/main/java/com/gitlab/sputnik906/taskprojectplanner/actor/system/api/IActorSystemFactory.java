package com.gitlab.sputnik906.taskprojectplanner.actor.system.api;

public interface IActorSystemFactory {
  IActorSystem defaultActorSystem();
}
