package com.gitlab.sputnik906.taskprojectplanner.actor.system.vertx;

import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.AbstractBehavior;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.IActorRef;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.IActorSystem;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


class ActorSystemVertxImpl implements IActorSystem{

  private final Vertx vertx;

  private final Map<Class<? extends AbstractBehavior>,Set<ActorVerticleImpl<? extends AbstractBehavior>>> behaviorInstances = new ConcurrentHashMap<>();

  public ActorSystemVertxImpl(VertxOptions vertxOptions) {
    this.vertx = Vertx.vertx(vertxOptions);
  }


  @Override
  public <B extends AbstractBehavior> IActorRef<B> create(B behavior) {
    return create(this.vertx,behavior);
  }

  public <B extends AbstractBehavior> IActorRef<B> create(Vertx vertx, B behavior) {
    ActorVerticleImpl<B> actorVerticle = new ActorVerticleImpl<>(this,behavior);
    actorVerticle.onClose(actor->behaviorInstances.getOrDefault(behavior.getClass(),new HashSet<>())
      .remove(actor));

    vertx.deployVerticle(actorVerticle,deployResult->{
      if (deployResult.succeeded()) {
        behaviorInstances
          .computeIfAbsent(behavior.getClass(),key->ConcurrentHashMap.newKeySet())
          .add(actorVerticle);

      }
    });

    return actorVerticle;
  }

  @Override
  public <B extends AbstractBehavior> Set<IActorRef<B>> getWhoRealizedBehavior(
    Class<B> behaviorClass) {

    return behaviorInstances.getOrDefault(behaviorClass,new HashSet<>()).stream()
      .map(e->(IActorRef<B>)e)
      .collect(Collectors.toSet());


  }

  @Override
  public void shutdown() {
    vertx.close();
  }



}
