package com.gitlab.sputnik906.taskprojectplanner.actor.system.api;

import java.util.Set;

public interface IActorSystem {
  <B extends AbstractBehavior> IActorRef<B> create(B behavior);
  <B extends AbstractBehavior> Set<IActorRef<B>> getWhoRealizedBehavior(Class<B> behaviorClass);
  void shutdown();
}
