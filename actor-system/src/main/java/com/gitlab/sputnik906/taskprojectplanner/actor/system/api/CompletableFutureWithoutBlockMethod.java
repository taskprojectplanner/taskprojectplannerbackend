package com.gitlab.sputnik906.taskprojectplanner.actor.system.api;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public class CompletableFutureWithoutBlockMethod<T> extends CompletableFuture<T> {

  @Override
  public T get() throws InterruptedException, ExecutionException {
    throw new UnsupportedOperationException();
  }

  @Override
  public T get(long timeout, TimeUnit unit)
    throws InterruptedException, ExecutionException, TimeoutException {
    throw new UnsupportedOperationException();
  }
  @Override
  public T join() {
    throw new UnsupportedOperationException();
  }

  public CompletableFuture<T> handle(Consumer<T> successHandler,Consumer<Throwable> errorHandler){
    return handle((result,error)->{
      if (error!=null) errorHandler.accept(error);
      else successHandler.accept(result);
      return result;
    });
  }
}
