package unit;

import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.AbstractBehavior;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.IActorRef;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.IActorSystem;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.vertx.ActorSystemVertxFactory;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import java.time.Instant;
import org.junit.jupiter.api.Test;

public class ActorSystemVertxTest {
  @Test
  public void createActorSystemTest() throws InterruptedException {
    IActorSystem actorSystem = new ActorSystemVertxFactory().defaultActorSystem();

    IActorRef<PrintBehavior> printActor =  actorSystem.create(new PrintBehavior());
    IActorRef<GetTimeBehavior> getTimeActor =  actorSystem.create(new GetTimeBehavior());

    printActor.tell(b->b.printMessage("Hello"));
    printActor.tell(b->b.printMessage("Hello 2"));

    printActor.tell(b-> b.printTime(getTimeActor));

    printActor.ask(PrintBehavior::getPrintedMessageCount)
      .thenAccept(System.out::println);



    printActor.ask(PrintBehavior::throwException).handle(System.out::println,System.err::println);


    printActor.tell(PrintBehavior::stopPrint);



    printActor.tell(b->b.printMessage("Hello 3"));

    Thread.sleep(3000);

    printActor.tell(b->b.printMessage("Hello 3"));
  }


  private static class GetTimeBehavior extends AbstractBehavior{
    private static final Logger LOG = LoggerFactory.getLogger(GetTimeBehavior.class);
    public Instant getTime(){
      LOG.info("invoke getTime. Tread name: "+Thread.currentThread().getName());
      return Instant.now();
    }
  }

  private static class PrintBehavior extends AbstractBehavior{

    private static final Logger LOG = LoggerFactory.getLogger(PrintBehavior.class);

    private int printedMessageCount;

    public void printTime(IActorRef<GetTimeBehavior> getTimeActor){
      getTimeActor.ask(GetTimeBehavior::getTime).thenAccept(this::printTime);
    }

    public void printTime(Instant instant){
      printMessage(instant.toString());
    }

    public void printMessage(String messageForPrint){
      LOG.info("invoke printMessage. Tread name: "+Thread.currentThread().getName());
      System.out.println(messageForPrint);
      printedMessageCount++;
    }

    public int getPrintedMessageCount(){
      LOG.info("invoke getPrintedMessageCount. Tread name: "+Thread.currentThread().getName());
      return printedMessageCount;
    }

    public int throwException(){
      throw new IllegalStateException();
    }

    public void stopPrint(){
      terminate();
    }
  }
}
