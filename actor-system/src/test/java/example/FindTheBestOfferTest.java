package example;

import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.AbstractBehavior;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.IActorRef;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.api.IActorSystem;
import com.gitlab.sputnik906.taskprojectplanner.actor.system.vertx.ActorSystemVertxFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.junit.jupiter.api.Test;

public class FindTheBestOfferTest {

  @Test
  public void checkDomainTest() throws InterruptedException {

    List<String> companyNames  = stringListGenerator("Company",4);

    Map<String,Map<String,List<Offer>>> companyProductOffersMap= new HashMap<>();
    companyNames.forEach(companyName->companyProductOffersMap.put(
      companyName,
      productOffersGenerator("Product",3,4,1000,0.9)
    ));

    IActorSystem actorSystem = new ActorSystemVertxFactory().defaultActorSystem();

    Set<IActorRef<ParticipantBehavior>> participants = companyProductOffersMap.values().stream()
      .map(offers->actorSystem.create(new ParticipantBehavior(offers)))
      .collect(Collectors.toSet());

    IActorRef<FindTheBestOfferBehavior> findActor =  actorSystem.create(new FindTheBestOfferBehavior());

    findActor.ask(b->b.findTheBestOffer("Product 1",2,participants))
      .thenAccept(r->{
        System.out.println("1-"+Thread.currentThread().getName());
        r.thenAccept(rr->{
          System.out.println(rr);
          System.out.println("2-"+Thread.currentThread().getName());
        });
      });


  }



  private Map<String,List<Offer>> productOffersGenerator(String prefixProductName,
    int productCount,int maxOffersCount,double baseOfferCost,double offerStepCoeff){
    List<String> productNames = stringListGenerator(prefixProductName,productCount);
    Map<String,List<Offer>> productOffersMap = new HashMap<>();
    productNames.forEach(productName->productOffersMap.put(productName,offersGenerator(productName,maxOffersCount,baseOfferCost,offerStepCoeff)));
    return productOffersMap;
  }

  private List<String> stringListGenerator(String prefixName, int count){
    return IntStream.range(1, count)
      .mapToObj(i->prefixName+" "+i)
      .collect(Collectors.toList());
  }

  private List<Offer> offersGenerator(String product,int maxCount,double baseCost,double offerStepCoeff){
    List<Offer> offers = new ArrayList<>();

    double prevCost = baseCost;

    for(int i=1; i<=maxCount; i++){
      offers.add(new Offer(product,i,prevCost));
      prevCost=prevCost*offerStepCoeff;
    }

    return offers;
  }


  @RequiredArgsConstructor
  public static class ParticipantBehavior extends AbstractBehavior {

    private final Map<String,List<Offer>> productOffersMap;

    public List<Offer> getOffers(String product, int needCount){
      return productOffersMap.computeIfAbsent(product,k-> Collections.emptyList())
        .stream().filter(offer->offer.count<=needCount)
        .collect(Collectors.toList());
    }
  }

  public static class FindTheBestOfferBehavior extends AbstractBehavior{

    private ParticipantOffer theBestOffer;

    public CompletableFuture<Optional<ParticipantOffer>> findTheBestOffer(String product,Integer needCount,
      Set<IActorRef<ParticipantBehavior>> participants){

      System.out.println("findTheBestOffer:"+Thread.currentThread().getName());

      CompletableFuture<Optional<ParticipantOffer>> theBestOfferFuture = new CompletableFuture<>();

      List<CompletableFuture<List<Offer>>> listOfferFutures =  participants.stream()
        .map(p->p.ask(b->b.getOffers(product,needCount))
          .handle(r->handleOffers(p,r),e->handleError(p,e))
        ).collect(Collectors.toList());

      CompletableFuture<Void> allFutures = CompletableFuture.allOf(listOfferFutures.toArray(new CompletableFuture[0]));

      //TODO узнать в каком потоке выполниться
      allFutures.thenRun(()->{
        System.out.println("findTheBestOffer(complete):"+Thread.currentThread().getName());
        theBestOfferFuture.complete(Optional.ofNullable(theBestOffer));
        terminate();
      });

      return theBestOfferFuture;
    }

    public void handleOffers(IActorRef<ParticipantBehavior> participant,List<Offer> offers){
      Offer minOffer = offers.stream().min(Comparator.comparingDouble(Offer::getCostPerOne)).orElse(null);
      if (minOffer==null) return;
      if (theBestOffer==null){
        theBestOffer = new ParticipantOffer(participant,minOffer);
        return;
      }
      if (theBestOffer.getOffer().getCostPerOne()>minOffer.getCostPerOne()){
        theBestOffer = new ParticipantOffer(participant,minOffer);
      }
    }

    public void handleError(IActorRef<ParticipantBehavior> participant,Throwable error){
      //ignory
    }
  }

  @RequiredArgsConstructor
  @Getter
  public static class Offer{
    private final String product;
    private final int count;
    private final double costPerOne;
  }

  @RequiredArgsConstructor
  @Getter
  @ToString
  public static class ParticipantOffer{
    private final IActorRef<ParticipantBehavior> participant;
    private final Offer offer;
  }



}
