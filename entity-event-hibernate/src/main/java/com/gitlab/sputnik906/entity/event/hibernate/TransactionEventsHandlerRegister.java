package com.gitlab.sputnik906.entity.event.hibernate;

import com.gitlab.sputnik906.entity.event.api.ITransactionEventsHandler;
import com.gitlab.sputnik906.entity.event.api.TransactionEvents;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class TransactionEventsHandlerRegister {

  @Autowired
  private List<ITransactionEventsHandler> transactionEventsHandlerList;

  @TransactionalEventListener
  public  void handler(TransactionEvents transactionEvents){
    CompletableFuture.runAsync(()->
      transactionEventsHandlerList.forEach(h->h.handle(transactionEvents))
    );

  }
}
