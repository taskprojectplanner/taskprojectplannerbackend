package com.gitlab.sputnik906.entity.event.hibernate;


import com.gitlab.sputnik906.entity.event.api.cdc.IChangeDataCaptureProvider;
import com.gitlab.sputnik906.entity.event.api.cdc.SnapshotEntityRequest;
import com.gitlab.sputnik906.entity.event.api.repo.EntityEventQuery;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Lazy(false)
@RequestMapping(value = "/cdc")
public class ChangeDataCaptureController {
  @Autowired
  private IChangeDataCaptureProvider cdcProvider;

  @RequestMapping(value = "/{entityType}",method = RequestMethod.GET)
  public Object getSnapshotEntityList(
    @PathVariable(value = "entityType") String entityType,
    @RequestParam(value = "count", required = false) Integer count,
    @RequestParam(value = "fromEntityId", required = false) Serializable fromEntityId
  ){
    return cdcProvider.getSnapshotEntity(
      SnapshotEntityRequest.builder()
      .entityType(entityType)
      .count(count)
      .fromEntityId(fromEntityId)
      .build()
    );
  }

  @RequestMapping(value = "/{entityType}/changes",method = RequestMethod.GET)
  public Object getChangesAfterEventId(
    @PathVariable(value = "entityType") String entityType,
    @RequestParam(value = "afterEventId", required = false) Long afterEventId,
    @RequestParam(value = "propNames", required = false) String propNames){

    return cdcProvider.getChanges(
      new EntityEventQuery(afterEventId)
        .setEntityType(entityType)
        .setPropertyNames(toSetOrNull(propNames))
    );

  }

  @RequestMapping(value = "/changes",method = RequestMethod.GET)
  public Object getChangesAfterEventId(
    @RequestParam(value = "afterEventId", required = false) Long afterEventId,
    @RequestParam(value = "propNames", required = false) String propNames){

    return cdcProvider.getChanges(
      new EntityEventQuery(afterEventId)
         .setPropertyNames(toSetOrNull(propNames))
    );

  }

  private Set<String> toSetOrNull(String propNames){
    if (propNames==null) return null;
    return new HashSet<>(Arrays.asList(propNames.split(",")));
  }
}
