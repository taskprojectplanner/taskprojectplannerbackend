package com.gitlab.sputnik906.entity.event.hibernate;

import com.gitlab.sputnik906.entity.event.api.cdc.ChangesEntities;
import com.gitlab.sputnik906.entity.event.api.cdc.IChangeDataCaptureProvider;
import com.gitlab.sputnik906.entity.event.api.cdc.SnapshotEntityRequest;
import com.gitlab.sputnik906.entity.event.api.cdc.SnapshotEntityResponse;
import com.gitlab.sputnik906.entity.event.api.repo.EntityEventQuery;
import com.gitlab.sputnik906.entity.event.api.repo.EntityEventRepository;
import com.gitlab.sputnik906.entity.event.api.type.AbstractEntityEvent;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.entity.persist.repository.query.EntityJpaQuery;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SimpleChangeDataCaptureProvider implements IChangeDataCaptureProvider {

  private final EntityRepository entityRepository;

  private final EntityEventRepository entityEventRepository;

  @Override
  public List<SnapshotEntityResponse<?>> getSnapshotEntityList(
    List<SnapshotEntityRequest> snapshotEntityRequests) {
    return snapshotEntityRequests.stream()
      .map(this::getResult).collect(Collectors.toList());
  }

  @Override
  public ChangesEntities getChanges(
    List<EntityEventQuery> entityEventQueries) {

    List<AbstractEntityEvent<?>> events = entityEventRepository.executeQuery(entityEventQueries);

    if (events.isEmpty()) return new ChangesEntities(events,entityEventQueries);

    Long theLatestAfterId = events.get(events.size()-1).getEventId();

    List<EntityEventQuery> newEntityEventQueries = entityEventQueries.stream()
      .map(c->c.with(theLatestAfterId))
      .collect(Collectors.toList());

    return new ChangesEntities(events, newEntityEventQueries);
  }


  private SnapshotEntityResponse<?> getResult(SnapshotEntityRequest snapshotEntityRequest){

    Class<?> entityClass = entityRepository.getEntityClassByName(snapshotEntityRequest.getEntityType());
    String idFieldName = entityRepository.getIdFieldName(entityClass);

    EntityJpaQuery<?> jpaQuery = new EntityJpaQuery<>(entityClass);
    jpaQuery.setWhere(snapshotEntityRequest.getWhere());
    jpaQuery.setSize(snapshotEntityRequest.getCount());

    if (snapshotEntityRequest.getFromEntityId()!=null){
      String where = jpaQuery.getWhere()!=null?jpaQuery.getWhere()+" and ":"";
      jpaQuery.setWhere(where+" "+idFieldName+" >"+snapshotEntityRequest.getFromEntityId());
    }

    for(;;){

      Long lastEventId = entityEventRepository.lastEventId();

      List<?> result = snapshotEntityRequest.getPropertyNames()!=null
        ?entityRepository.query(
          jpaQuery,
          snapshotEntityRequest.getPropertyNames().toArray(new String[0])
        )
        :entityRepository.query(jpaQuery);

     if (lastEventId.equals(entityEventRepository.lastEventId())){
       EntityEventQuery entityEventQuery = new EntityEventQuery(lastEventId)
         .setEntityType(snapshotEntityRequest.getEntityType())
         .setPropertyNames(snapshotEntityRequest.getPropertyNames())
         .setEventTypes(snapshotEntityRequest.getEventTypes());


       SnapshotEntityRequest nextSnapshotEntityRequest = null;

       if (!result.isEmpty() && snapshotEntityRequest.getCount()!=null){
         Serializable lastEntityId = (Serializable)entityRepository.getId(result.get(result.size()-1),idFieldName);
         entityEventQuery.setLessOrEqualEntityId(lastEntityId);
         nextSnapshotEntityRequest = snapshotEntityRequest.withFromEntityId(lastEntityId);
       }

       return new SnapshotEntityResponse<>(
         result,
         entityEventQuery,
         nextSnapshotEntityRequest
       );
     }

    }


  }
}
