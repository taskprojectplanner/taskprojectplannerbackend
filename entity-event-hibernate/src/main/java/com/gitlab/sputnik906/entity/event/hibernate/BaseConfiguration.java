package com.gitlab.sputnik906.entity.event.hibernate;

import com.gitlab.sputnik906.entity.event.api.AuthorProvider;
import com.gitlab.sputnik906.entity.event.api.cdc.IChangeDataCaptureProvider;
import com.gitlab.sputnik906.entity.event.api.repo.EntityEventRepository;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepositoryFactory;
import com.gitlab.sputnik906.entity.spring.EntityTransactionManagerAdapter;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class BaseConfiguration {

  @Autowired
  private EntityManager em;

  @Autowired private PlatformTransactionManager transactionManager;

  @Bean
  @ConditionalOnMissingBean
  public AuthorProvider authorProvider(){
    return new MockAuthorProvider();
  }

  @Bean
  @ConditionalOnMissingBean
  public EntityEventRepository entityEventRepository(){
    return new InMemorySimpleEntityEventRepository();
  }

  @Bean
  @ConditionalOnMissingBean
  public EntityRepository entityRepository(){
    return EntityRepositoryFactory.withTransactionManager(em,new EntityTransactionManagerAdapter(transactionManager));
  }

  @Bean
  @ConditionalOnMissingBean
  public IChangeDataCaptureProvider changeDataCaptureProvider(){
    return new SimpleChangeDataCaptureProvider(entityRepository(),entityEventRepository());
  }



}
