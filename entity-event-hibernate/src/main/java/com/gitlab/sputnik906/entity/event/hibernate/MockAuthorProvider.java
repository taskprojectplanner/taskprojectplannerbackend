package com.gitlab.sputnik906.entity.event.hibernate;

import com.gitlab.sputnik906.entity.event.api.AuthorProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.SearchStrategy;
import org.springframework.stereotype.Component;

//@ConditionalOnMissingBean(type = "AuthorProvider")
//@Component
public class MockAuthorProvider implements AuthorProvider {
    @Override
    public String provide() {
        return "unknown";
    }
}
