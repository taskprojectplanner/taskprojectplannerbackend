package component.domain.valueobject;

import lombok.Value;

@Value
public class Position {
  private final double longitude;
  private final double latitude;
}
