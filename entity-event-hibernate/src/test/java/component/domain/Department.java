package component.domain;

import com.gitlab.sputnik906.entity.event.api.annotation.CdcEntity;
import com.sun.istack.NotNull;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Entity
@CdcEntity
public class Department {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected String id;

  @Version
  protected Long version;

  @NotNull
  @NonNull
  private String label;

  @OneToMany(fetch = FetchType.LAZY)
  private Set<Employee> employees;

}
