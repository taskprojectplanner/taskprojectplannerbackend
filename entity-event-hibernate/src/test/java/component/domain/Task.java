package component.domain;

import com.gitlab.sputnik906.entity.event.api.annotation.CdcEntity;
import com.sun.istack.NotNull;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Entity
@CdcEntity
public class Task {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected String id;

  @Version
  protected Long version;

  @NotNull
  @NonNull
  private String label;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @NotNull
  @Setter(AccessLevel.PACKAGE)
  private Project project;

  @ManyToMany
  @NonNull @NotNull private Set<Skill> requiredSkills;
}
