package component.domain;

import com.gitlab.sputnik906.entity.event.api.annotation.CdcEntity;
import com.sun.istack.NotNull;
import component.domain.valueobject.Address;
import component.domain.valueobject.Position;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Entity
@CdcEntity
public class Employee {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected String id;

  @Version
  protected Long version;

  @NotNull
  @NonNull
  private String label;

  @ManyToMany
  @NonNull @NotNull private Set<Skill> skills;

  @ElementCollection
  private List<String> phones = new ArrayList<>();

  @Embedded @NotNull @NonNull
  private Address address;

  @ElementCollection
  private Map<String,String> attributes = new HashMap<>();

  @ManyToOne(fetch = FetchType.LAZY)
  private Department department;

  private int age;

  @Embedded @Setter
  private Position position;
}
