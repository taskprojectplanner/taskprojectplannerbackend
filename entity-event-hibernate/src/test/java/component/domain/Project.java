package component.domain;

import com.gitlab.sputnik906.entity.event.api.annotation.CdcEntity;
import com.sun.istack.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Entity
@CdcEntity
public class Project {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected String id;

  @Version
  protected Long version;

  @NotNull
  @NonNull
  private String label;

  @OneToMany(
    fetch = FetchType.LAZY,
    mappedBy = "project",
    cascade = {CascadeType.REMOVE, CascadeType.PERSIST, CascadeType.MERGE},
    orphanRemoval = true)
  private Set<Task> tasks = new HashSet<>();

  public Project addTasks(Set<Task> tasks) {
    this.tasks.addAll(tasks);
    tasks.forEach(t->t.setProject(this));
    return this;
  }
}
