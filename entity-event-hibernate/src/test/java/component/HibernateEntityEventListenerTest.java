package component;

import com.gitlab.sputnik906.entity.event.api.repo.EntityEventRepository;
import com.gitlab.sputnik906.entity.event.api.type.AbstractEntityEvent;
import com.gitlab.sputnik906.entity.event.api.type.AddToCollectionEvent;
import com.gitlab.sputnik906.entity.event.api.type.AddToMapEvent;
import com.gitlab.sputnik906.entity.event.api.type.EntityDeleteEvent;
import com.gitlab.sputnik906.entity.event.api.type.EntityPersistEvent;
import com.gitlab.sputnik906.entity.event.api.type.EntityUpdateEvent;
import com.gitlab.sputnik906.entity.event.api.type.RemoveFromCollectionEvent;
import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;
import com.gitlab.sputnik906.entity.persist.repository.query.EntityJpaQuery;
import component.domain.Employee;
import component.domain.Project;
import component.domain.Skill;
import component.domain.Task;
import component.domain.valueobject.Address;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


//@DataJpaTest - не подходит т.к. октатывает транзакции, поэтому не вызываются события postUpdate postDelete
//@ContextConfiguration(classes = {SpringModule.class, HibernateEntityEventHandler.class,TransactionService.class})
//@AutoConfigurationPackage

@SpringBootTest(classes = Application.class)
public class HibernateEntityEventListenerTest {

  @Autowired
  private EntityRepository entityRepository;

  @Autowired
  private EntityEventRepository entityEventRepository;

  @Test
  public void queryTest(){

    Skill skill1 = new Skill("Skill 1");
    entityRepository.persist(skill1);

    Skill skill2 = new Skill("Skill 2");
    entityRepository.persist(skill2);

    Employee employee1 = new Employee("Employee 1",new HashSet<>(Collections.singletonList(skill1)),new Address("Street 1",443029));
    employee1.getAttributes().put("attribute1","value1");
    employee1.getPhones().add("111-11-11");

    entityRepository.persist(employee1);
    entityRepository.persist(new Employee("Employee 2",new HashSet<>(Collections.singletonList(skill2)),new Address("Street 2",443029)));
    entityRepository.persist(new Employee("Employee 3",new HashSet<>(Arrays.asList(skill1,skill2)),new Address("Street 3",443029)));

    //String[] selects = new String[]{"id as id","label as label","address","skills"};
    String[] selects = new String[]{"id","label"};

//    ScalarJpaQuery<Employee> q = new ScalarJpaQuery<>(Employee.class,selects);
//    q.setWhere("label = 'Employee 3'");

    EntityJpaQuery<Employee> q = new EntityJpaQuery<>(Employee.class);

    //List<Map<String,Object>> result = ts.queryScalar(q);

    //List<TestClass> result = ts.query(q,TestClass.class);

    //List<Map<String,Object>> result = entityRepository.query(q,new String[]{"id","label","skills.id"});

    System.out.println();
  }



  @Test
  public void skillCreateAndDeleteTest() {

    Skill skill = new Skill("Skill 1");

    entityRepository.persist(skill);

    EntityPersistEvent<?> entityPersistEvent =
      (EntityPersistEvent<?>)entityEventRepository.lastEvent();

    Assertions.assertEquals(skill,entityPersistEvent.entity());

    skill.setLabel("Skill Changed");

    Skill newSkill = entityRepository.merge(skill);

    EntityUpdateEvent<?> entityUpdateEvent =
      (EntityUpdateEvent<?>)entityEventRepository.lastEvent();

    Assertions.assertEquals(newSkill,entityUpdateEvent.entity());

    entityRepository.delete(newSkill);

    EntityDeleteEvent<?> entityDeleteEvent =
      (EntityDeleteEvent<?>)entityEventRepository.lastEvent();

    Assertions.assertNotNull(entityDeleteEvent.entity());

  }

  @Test
  public void cascadePersistTest() {

    Task task1 =  new Task("Task 1",new HashSet<>());
    Task task2 =  new Task("Task 2",new HashSet<>());

    Project project = new Project("Project 1").addTasks(new HashSet<>(Arrays.asList(task1,task2)));

    Long lastId = entityEventRepository.lastEventId();

    entityRepository.persist(project);

    List<AbstractEntityEvent<?>> lastEvents = entityEventRepository.after(lastId);

    EntityPersistEvent<Project> event1 = cast(lastEvents.get(0));
    EntityPersistEvent<Task> event2 = cast(lastEvents.get(1));
    EntityPersistEvent<Task> event3 =  cast(lastEvents.get(2));

    Task task3 =  new Task("Task 3",new HashSet<>());

    project.addTasks(new HashSet<>(Collections.singletonList(task3)));

    lastId = entityEventRepository.lastEventId();

    project = entityRepository.merge(project);

    lastEvents = entityEventRepository.after(lastId);

    EntityPersistEvent<Task> event7 = cast(lastEvents.get(0));
    EntityUpdateEvent<Task> event8 = cast(lastEvents.get(1)); //TODO почему возникаес событие обновления requiredSkills?
    AddToCollectionEvent<Project> event9 = cast(lastEvents.get(2));

    Assertions.assertEquals("tasks",event9.getPropertyName());

    project.getTasks().removeIf(t->t.getLabel().equals("Task 1"));

    lastId = entityEventRepository.lastEventId();

    project = entityRepository.merge(project);

    lastEvents = entityEventRepository.after(lastId);

    RemoveFromCollectionEvent<Project> event11 = cast(lastEvents.get(0));

    Assertions.assertEquals("tasks",event11.getPropertyName());

    EntityDeleteEvent<Task> event12 = cast(lastEvents.get(1));
    Assertions.assertEquals("Task 1",event12.entity().getLabel());

    lastId = entityEventRepository.lastEventId();

    entityRepository.delete(project);

    lastEvents = entityEventRepository.after(lastId);

    EntityDeleteEvent<Task> event13 = cast(lastEvents.get(0));
    EntityDeleteEvent<Task> event14 = cast(lastEvents.get(1));
    EntityDeleteEvent<Project> event15 = cast(lastEvents.get(2));

  }



  @Test
  public void elementCollectionTest() {

    Employee employee = new Employee("Employee 1",new HashSet<>(),new Address("Street 1",443029));
    employee.getPhones().add("111-11-11");

    Long lastId = entityEventRepository.lastEventId();

    entityRepository.persist(employee);

    List<AbstractEntityEvent<?>> lastEvents = entityEventRepository.after(lastId);

    EntityPersistEvent<Employee> event1 = cast(lastEvents.get(0));

    employee.getPhones().add("222-22-22");

    lastId = entityEventRepository.lastEventId();

    employee = entityRepository.merge(employee);

    lastEvents = entityEventRepository.after(lastId);

    EntityUpdateEvent<Employee> event4 = cast(lastEvents.get(0));;
    Assertions.assertEquals("version",event4.getNewStates().keySet().stream().findFirst().orElse(""));

    AddToCollectionEvent<Project> event5 = cast(lastEvents.get(1));;
    Assertions.assertEquals("phones",event5.getPropertyName());

    employee.getAddress().setStreet("New Street");
    employee.getAddress().setPostcode(56788);

    lastId = entityEventRepository.lastEventId();

    employee = entityRepository.merge(employee);

    lastEvents = entityEventRepository.after(lastId);

    EntityUpdateEvent<Employee> event51 = cast(lastEvents.get(0));;

    Assertions.assertNotNull(event51.getNewStates().get("address.street"));
    Assertions.assertNotNull(event51.getNewStates().get("version"));
    Assertions.assertNotNull(event51.getNewStates().get("address.postcode"));

    employee.getAttributes().put("attribute1","value1");

    lastId = entityEventRepository.lastEventId();

    employee = entityRepository.merge(employee);

    lastEvents = entityEventRepository.after(lastId);

    EntityUpdateEvent<Employee> event6 = cast(lastEvents.get(0));;
    Assertions.assertEquals("version",event6.getNewStates().keySet().stream().findFirst().orElse(""));

    AddToMapEvent<Employee> event7 = cast(lastEvents.get(1));
    Assertions.assertEquals("attributes",event7.getPropertyName());
    Assertions.assertEquals("value1",event7.getAddedEntries().get("attribute1"));

    employee.getAttributes().put("attribute1","changed value");

    lastId = entityEventRepository.lastEventId();

    employee = entityRepository.merge(employee);

    lastEvents = entityEventRepository.after(lastId);

    EntityUpdateEvent<Employee> event8 = cast(lastEvents.get(0));
    Assertions.assertEquals("version",event8.getNewStates().keySet().stream().findFirst().orElse(""));

    EntityUpdateEvent<Employee> event9 = cast(lastEvents.get(1));
    Assertions.assertEquals("attributes.attribute1", event9.getNewStates().keySet().stream().findFirst().orElse(""));


  }

  private <T> T cast(AbstractEntityEvent<?> event){
    return (T) event;
  }



}
