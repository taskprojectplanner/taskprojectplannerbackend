package unit;

import com.gitlab.sputnik906.entity.persist.EntityMapper;
import component.domain.Employee;
import component.domain.Skill;
import component.domain.valueobject.Address;
import component.domain.valueobject.Position;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Value;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EntityMapperTest {

  @Test
  public void convertTest(){

    Employee employee = buildEmployee();

    EmployeeDto employeeDto = EntityMapper.convert(employee,Employee.class,EmployeeDto.class);

    Assertions.assertEquals(employee.getId(),employeeDto.getId());
  }

  @Test
  public void convertMapTest(){

    Employee employee = buildEmployee();

    Map<String,Object> employeeMap = EntityMapper.convert(
      employee,
      Employee.class,
      new String[]{"id","latDeg","lonDeg","address","addr.code","addr.st","attr.attribute1"}
    );

    Assertions.assertEquals(employee.getId(),employeeMap.get("id"));
  }

  private Employee buildEmployee(){
    Skill skill1 = new Skill("Skill 1");
    Skill skill2 = new Skill("Skill 2");

    Employee employee =
      new Employee(
        "Employee 1",
        new HashSet<>(Arrays.asList(skill1, skill2)),
        new Address("Street 1", 443029));

    employee.getAttributes().put("attribute1","value1");
    employee.getPhones().add("111-11-11");
    employee.setPosition(new Position(Math.toRadians(10),Math.toRadians(10)));

    return employee;
  }


  @Value
  public static class EmployeeDto{
    private final String id;
    private final String label;
    private final Address address;
    private final AddressDto addr;
    private final String street;
    private final Set<SkillDto> skills;
    private final List<String> phones;
    private final double latDeg;
    private final double lonDeg;
  }

  @Value
  public static class AddressDto{
    private final String st;
    private final int code;
  }

  @Value
  public static class SkillDto{
    private final String id;
    private final String label;
  }

}
