package com.gitlab.sputnik906.entity.spring.controller;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UrlEntity {
  public static String root(String entityName){
    return "/api/"+entityName.toLowerCase()+"s";
  }
}
