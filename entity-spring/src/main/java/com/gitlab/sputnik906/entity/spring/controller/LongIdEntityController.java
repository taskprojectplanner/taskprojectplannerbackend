package com.gitlab.sputnik906.entity.spring.controller;


import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;

public class LongIdEntityController<T> extends EntityController<T,Long>{

  public LongIdEntityController(Class<T> domainClass,
    EntityRepository repository) {
    super(domainClass, Long.class, repository);
  }
}
