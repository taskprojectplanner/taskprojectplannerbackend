package com.gitlab.sputnik906.entity.spring.controller;

import com.gitlab.sputnik906.entity.persist.repository.EntityRepository;

public class StringIdEntityController<T> extends EntityController<T,String>{

  public StringIdEntityController(Class<T> domainClass,
    EntityRepository repository) {
    super(domainClass, String.class, repository);
  }
}
